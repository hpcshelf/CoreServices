﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using org.hpcshelf.DGAC;
using org.hpcshelf.database;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.ports;
using org.hpcshelf.provenance;
using gov.cca;
using org.hpcshelf;

namespace org.hpcshelf
{
	public class ProvenanceManager : ProvenancePort
	{
		private int provenance_level;

		public int ProvenanceLevel { get { return this.provenance_level; } }

		private IProvenanceTracer provenance_tracer;
		private CoreServices core_services;

		public IProvenanceTracer ProvenanceTracer { set { provenance_tracer = value; } }

		public string SystemRef => core_services.SystemRef;

		public ProvenanceManager(CoreServices core_services)
		{
			this.core_services = core_services;
		}

		public void createSystemComponent(string label_ref, string new_label_ref, string system_ref, string new_system_ref, string pbk_label)
		{
			// Retrieve label ref
			string c_ref_label = label_ref.Substring(label_ref.LastIndexOf('.') + 1);
			string c_ref_label_new = new_label_ref.Substring(new_label_ref.LastIndexOf('.') + 1); ;

			// Retrieve system ref
			string c_ref_system = system_ref.Substring(system_ref.LastIndexOf('.') + 1);
			string c_ref_system_new = new_system_ref.Substring(new_system_ref.LastIndexOf('.') + 1); ;

			string path_system = Path.Combine(Constants.PATH_CATALOG_FOLDER, system_ref);
			string path_new_system = Path.Combine(Constants.PATH_CATALOG_FOLDER, new_system_ref);

			DirectoryInfo dir_source = new DirectoryInfo(path_system);
			DirectoryInfo dir_target = new DirectoryInfo(path_new_system);

			CopyFilesRecursively(dir_source, dir_target);

			string path_hpe_file = Path.Combine(path_system, c_ref_system + ".hpe");
			string path_hpe_file_old = Path.Combine(path_new_system, c_ref_system + ".hpe");
			string path_hpe_file_snk = Path.Combine(path_new_system, c_ref_system + ".snk");
			string path_hpe_file_pub = Path.Combine(path_new_system, c_ref_system + ".pub");
			string path_hpe_file_new = Path.Combine(path_new_system, c_ref_system_new + ".hpe");

			// TODO: CREATE SNK AND PUB FILES ...
			string path_snk_file_new = Path.Combine(path_new_system, c_ref_system_new + ".snk");
			string path_pub_file_new = Path.Combine(path_new_system, c_ref_system_new + ".pub");
			string pbk_system = FileUtil.create_snk_file(path_snk_file_new, path_pub_file_new);

			string hpe_file_contents = File.ReadAllText(path_hpe_file);
			ComponentType c = LoaderApp.deserialize<ComponentType>(hpe_file_contents);
			performUpdatesInSystemConfig(c, c_ref_label, c_ref_label_new, c_ref_system, c_ref_system_new, pbk_system, pbk_label);
			File.WriteAllText(path_hpe_file_new, LoaderApp.serialize<ComponentType>(c));
			File.Delete(path_hpe_file_old);
			File.Delete(path_hpe_file_snk);
			File.Delete(path_hpe_file_pub);

			string src_path = Path.Combine(path_new_system, "src", "1.0.0.0");
			foreach (string cs_file_name in Directory.EnumerateFiles(src_path, "*.cs"))
			{
				string cs_file_contents = File.ReadAllText(cs_file_name);
				cs_file_contents = cs_file_contents.Replace("using " + label_ref, "using " + new_label_ref);
				cs_file_contents = cs_file_contents.Replace("namespace " + system_ref, "namespace " + new_system_ref);
				File.Delete(cs_file_name);
				File.WriteAllText(cs_file_name, cs_file_contents);
			}
		}

		private void performUpdatesInSystemConfig(ComponentType c, string c_ref_label, string c_ref_label_new, string c_ref_system, string c_ref_system_new, string pbk_label, string pbk_system)
		{
			Debug.Assert(c.header.name == c_ref_system);
			c.header.name = c_ref_system_new;
			c.header.hash_component_UID = pbk_system;

			foreach (object inner_c_obj in c.componentInfo)
			{
				if (inner_c_obj is InnerComponentType)
				{
					InnerComponentType inner_c = (InnerComponentType)inner_c_obj;
					if (inner_c.localRef.Equals("provenance_label"))
					{
						inner_c.hash_component_UID = pbk_label;
						inner_c.name = c_ref_label_new;
						inner_c.location = inner_c.location.Replace(c_ref_label, c_ref_label_new);
					}
				}
			}
		}

		public void createProvenanceLabelComponent(string label_ref, string new_label_ref, out string pbk_label)
		{
			// Retrieve label ref
			string c_ref_label = label_ref.Substring(label_ref.LastIndexOf('.') + 1);
			string c_ref_label_new = new_label_ref.Substring(new_label_ref.LastIndexOf('.') + 1); ;

			// Create a new label component
			string path_label = Path.Combine(Constants.PATH_CATALOG_FOLDER, label_ref);
			string path_new_label = Path.Combine(Constants.PATH_CATALOG_FOLDER, new_label_ref);

			DirectoryInfo dir_source = new DirectoryInfo(path_label);
			DirectoryInfo dir_target = new DirectoryInfo(path_new_label);

			CopyFilesRecursively(dir_source, dir_target);

			string path_hpe_file = Path.Combine(path_label, c_ref_label + ".hpe");
			string path_hpe_file_old = Path.Combine(path_new_label, c_ref_label + ".hpe");
			string path_hpe_file_snk = Path.Combine(path_new_label, c_ref_label + ".snk");
			string path_hpe_file_pub = Path.Combine(path_new_label, c_ref_label + ".pub");
			string path_hpe_file_new = Path.Combine(path_new_label, c_ref_label_new + ".hpe");

			// TODO: CREATE SNK AND PUB FILES ...
			string path_snk_file_new = Path.Combine(path_new_label, c_ref_label_new + ".snk");
			string path_pub_file_new = Path.Combine(path_new_label, c_ref_label_new + ".pub");
			pbk_label = FileUtil.create_snk_file(path_snk_file_new, path_pub_file_new);

			string hpe_file_contents = File.ReadAllText(path_hpe_file);
			ComponentType c = LoaderApp.deserialize<ComponentType>(hpe_file_contents);
			performUpdatesInLabelConfig(c, label_ref, c_ref_label, c_ref_label_new, pbk_label);
			File.WriteAllText(path_hpe_file_new, LoaderApp.serialize<ComponentType>(c));
			File.Delete(path_hpe_file_old);
			File.Delete(path_hpe_file_snk);
			File.Delete(path_hpe_file_pub);

			string path_src_file_main = Path.Combine(path_label, "src", "1.0.0.0", "IProvenanceLabel.cs");
			string path_src_file_main_new = Path.Combine(path_new_label, "src", "1.0.0.0", "IProvenanceLabel.cs");
			//string path_src_file_main_contents = File.ReadAllText(path_src_file_main);
			string path_src_file_main_contents = "using org.hpcshelf.kinds;\n\nnamespace org.hpcshelf.provenance.ProvenanceLabel_TEMPLATE\n{\n\tpublic interface IProvenanceLabel : BaseIProvenanceLabel, org.hpcshelf.provenance.ProvenanceLabel_BASE.IProvenanceLabel\n\t{\n\t}\n}";
			string path_src_file_main_contents_new = path_src_file_main_contents.Replace("ProvenanceLabel_BASE", c_ref_label);
			path_src_file_main_contents_new = path_src_file_main_contents_new.Replace("ProvenanceLabel_TEMPLATE", c_ref_label_new);
			File.Delete(path_src_file_main_new);
			File.WriteAllText(path_src_file_main_new, path_src_file_main_contents_new);

			string path_src_file_base = Path.Combine(path_label, "src", "1.0.0.0", "BaseIProvenanceLabel.cs");
			string path_src_file_base_new = Path.Combine(path_new_label, "src", "1.0.0.0", "BaseIProvenanceLabel.cs");
			string path_src_file_base_contents = "using org.hpcshelf.kinds;\n\nnamespace org.hpcshelf.provenance.ProvenanceLabel_TEMPLATE\n{\n\tpublic interface BaseIProvenanceLabel : org.hpcshelf.provenance.ProvenanceLabel_BASE.BaseIProvenanceLabel, IQualifierKind \n\t{\n\t}\n}";
			string path_src_file_base_contents_new = path_src_file_base_contents.Replace("ProvenanceLabel_BASE", c_ref_label);
			path_src_file_base_contents_new = path_src_file_base_contents_new.Replace("ProvenanceLabel_TEMPLATE", c_ref_label_new);
			File.Delete(path_src_file_base_new);
			File.WriteAllText(path_src_file_base_new, path_src_file_base_contents_new);
		}

		private void performUpdatesInLabelConfig(ComponentType c, string label_ref, string c_ref_label, string c_ref_label_new, string pbk_label)
		{
			Debug.Assert(c.header.name == c_ref_label);
			c.header.name = c_ref_label_new;
			c.header.hash_component_UID = pbk_label;

			if (c.header.baseType != null)
			{
				string c_ref_super = c.header.baseType.component.name;
				c.header.baseType.component.name = c_ref_label;
				c.header.baseType.component.location = c.header.baseType.component.location.Replace(c_ref_super, c_ref_label);
			}
			else
			{
				c.header.baseType = new BaseTypeType();
				c.header.baseType.extensionType = new ExtensionTypeType();
				c.header.baseType.extensionType.ItemElementName = ItemChoiceType.extends;
				c.header.baseType.extensionType.Item = true;
				c.header.baseType.component = new ComponentInUseType();
				c.header.baseType.component.localRef = "base";
				c.header.baseType.component.location = Path.Combine(label_ref, c_ref_label + ".hpe");
				c.header.baseType.component.name = c_ref_label;
				c.header.baseType.component.package = label_ref.Replace("." + c_ref_label, "");
				c.header.baseType.component.version = "1.0.0.0";
				c.header.baseType.component.visualDescription = new VisualElementAttributes();
				c.header.baseType.component.visualDescription.x = 0;
				c.header.baseType.component.visualDescription.y = 0;
				c.header.baseType.component.visualDescription.w = 100;
				c.header.baseType.component.visualDescription.h = 100;

			}

			foreach (object unit_obj in c.componentInfo)
			{
				if (unit_obj is UnitType)
				{
					UnitType u = (UnitType)unit_obj;
					if (u.uRef.Equals("provenance_label"))
					{
						u.super = new UnitRefType[1];
						u.super[0] = new UnitRefType();
						u.super[0].cRef = "base";
						u.super[0].slice_replica = 0;
						u.super[0].uRef = "provenance_label";
					}
				}
			}
		}



		public static void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target)
		{
			foreach (DirectoryInfo dir in source.GetDirectories())
				CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
			foreach (FileInfo file in source.GetFiles())
				file.CopyTo(Path.Combine(target.FullName, file.Name));
		}

		private void findReferences(string system_ref, out string label_ref, IDbConnection dbcon)
		{
            //	DBConnector.openConnection();

            org.hpcshelf.database.Component c = Backend.cdao.retrieve_libraryPath(system_ref, dbcon);
			int id_functor_app = c.Id_functor_app;

			SupplyParameter sp = Backend.spdao.retrieve("provenance_label", id_functor_app, dbcon);
			if (sp != null)
			{
				SupplyParameterComponent spc = (SupplyParameterComponent)sp;
				AbstractComponentFunctorApplication acfa = Backend.acfadao.retrieve(spc.Id_functor_app_actual, dbcon);
				AbstractComponentFunctor acf = Backend.acfdao.retrieve(acfa.Id_abstract, dbcon);
				label_ref = acf.Library_path;
			}
			else
				label_ref = "org.hpcshelf.provenance.ProvenanceLabel";

		//	DBConnector.closeConnection();
		}


		public void start(int level)
		{
			provenance_level = level;
		}

		public string confirm()
		{
            string result;

            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();
            try
            {

                // PROCEDURE TO SAVE THE PROCEDURE DATA

                // LEVEL 0: The componente selected using the "experiment label".
                // LEVEL 1: NOT SUPPORTED YET (dynamic creation of architectures).
                // LEVEL 2: It is saved in workflow_resolution dictionary.
                // LEVEL 3: TODO ...

                int new_label_ref_suffix = DateTime.Now.Millisecond;

                string label_ref, system_ref = core_services.SystemRef; // system_component.ComponentChoice[0]; //"mapreduce.system.linear.three_stages.non_iterative.impl.System";
                findReferences(system_ref, out label_ref, dbcon);

                // 1. CREATE AND REGISTER THE LABEL, INHERITING FROM THE CURRENT LABEL.

                string new_label_ref = result = label_ref + new_label_ref_suffix;
                string new_system_ref = system_ref + new_label_ref_suffix;

                string pbk_label;
                createProvenanceLabelComponent(label_ref, new_label_ref, out pbk_label);

                // Register the new label component

                string path_catalog_folder = Constants.PATH_CATALOG_FOLDER;
                string file_name_label = Path.Combine(path_catalog_folder, new_label_ref);
                file_name_label = Path.Combine(file_name_label, new_label_ref.Substring(new_label_ref.LastIndexOf('.') + 1) + ".hpe");
                core_services.register(File.ReadAllText(file_name_label), new byte[0] /* TODO */);

                // 2. CREATE AND REGISTER THE SYSTEM COMPONENT.

                createSystemComponent(label_ref, new_label_ref, system_ref, new_system_ref, pbk_label);

                // Register the new system component

                string file_name_system = Path.Combine(path_catalog_folder, new_system_ref);
                string file_name_system_cs = Path.Combine(file_name_system, new_system_ref.Substring(new_system_ref.LastIndexOf('.') + 1) + ".hpe");
                core_services.register(File.ReadAllText(file_name_system_cs), new byte[0] /* TODO */);

                if (provenance_level >= 2)
                {
                    foreach (KeyValuePair<string, Backend.ResolveComponentHierarchy> ctree in core_services.WorkflowResolution)
                    {
                        ConverterComponentHierachyToXML xml_converter = new ConverterComponentHierachyToXML();
                        XMLComponentHierarchy.ComponentHierarchyType ctree_xml = (XMLComponentHierarchy.ComponentHierarchyType)xml_converter.visit(ctree.Value, dbcon);
                        string ctree_xml_str = LoaderApp.serialize(ctree_xml);
                        File.WriteAllText(Path.Combine(file_name_system, ctree.Key + ".resolution"), ctree_xml_str);
                    }
                }

                if (provenance_level >= 3)
                {
                    Debug.Assert(provenance_tracer is ProvenanceTracer);
                    ProvenanceTracer provenance_tracer_ = (ProvenanceTracer)provenance_tracer;
                    provenance_tracer_.record(Path.Combine(file_name_system, "orchestration.history"));
                }
            }
            finally
            {
                dbcon.Close();
            }

            return result;
		}

		public void cancel()
		{
			throw new NotImplementedException();
		}
	}

}
