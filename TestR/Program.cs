﻿using System;
using System.IO;
using System.Linq;
using RDotNet;

namespace Sample1
{
	class Program
	{
		static void Main(string[] args)
		{
            
/*			REngine engine = REngine.GetInstance();
			// A somewhat contrived but customary Hello World:
			CharacterVector charVec = engine.CreateCharacterVector(new[] { "Hello, R world!, .NET speaking" });
			engine.SetSymbol("greetings", charVec);
			engine.Evaluate("str(greetings)"); // print out in the console
			string[] a = engine.Evaluate("'Hi there .NET, from the R engine'").AsCharacter().ToArray();
			Console.WriteLine("R answered: '{0}'", a[0]);
			Console.WriteLine("Press any key to exit the program");
			Console.ReadKey();
			engine.Dispose();
*/
            			
            REngine engine = REngine.GetInstance();
			engine.Initialize();

            engine.Evaluate("require('MCDM')");
			NumericMatrix d = engine.CreateNumericMatrix(new double[,] { { 1, 4, 5 }, { 3, 5, 5 }, { 2, 3, 1 } });
            NumericVector w = engine.CreateNumericVector(new double[] { 0.5, 0.2, 0.3 });
            CharacterVector cb = engine.CreateCharacterVector(new string[] { "max", "max", "min" });

            //engine.SetSymbol("d", d);
			//engine.SetSymbol("w", w);
			//engine.SetSymbol("cb", cb);

			var f_str = engine.GetSymbol("str").AsFunction();
			
            var f_mcmd = engine.GetSymbol("TOPSISLinear").AsFunction();

            DataFrame output = f_mcmd.Invoke(new SymbolicExpression[] { d, w, cb }).AsDataFrame();

            Console.WriteLine(output.ColumnCount);
            foreach (string s in output.ColumnNames)
                Console.Write(s + ",");
            Console.WriteLine();


			//  Console.WriteLine(output[0].GetType());

			int[] u1 = output[0].AsInteger().ToArray();

            foreach (int y in u1)
            {
                Console.WriteLine("tttttt {0}", y);
            }

           // Console.WriteLine(u1.ElementAt(0));

            f_str.Invoke(new[] { output[0] });
			f_str.Invoke(new[] { output[1] });
			f_str.Invoke(new[] { output[2] });

			//	GenericVector testResult = engine.Evaluate("TOPSYSLINEAR(d,w,cb)").AsList();

			engine.Dispose();

		}
	}
}