#!/bin/sh

MAKE=make_dgac.rsp
OLD="MONO_HOME"
NEW=$MONO_HOME

cp $MAKE temp.rsp
find ./ -iname temp.rsp | xargs sed -i --expression "s@$OLD@$NEW@"

if [ ! -d ./CoreWebServices/bin ]
then
    mkdir ./CoreWebServices/bin
fi

if [ ! -f RDotNet/RDotNet.snk ]
then
    sn -k RDotNet/RDotNet.snk
fi

if [ ! -f DGAC/DGAC.snk ]
then
    sn -k DGAC/DGAC.snk
fi


if [ ! -f DynamicInterop/DynamicInterop.snk ]
then
    sn -k DynamicInterop/DynamicInterop.snk
fi

cd RDotNet
#$MONO_HOME/bin/xbuild
msbuild
cd ../DynamicInterop
#$MONO_HOME/bin/xbuild
msbuild
cd ..

$MONO_HOME/bin/gacutil -i RDotNet/bin/Debug/RDotNet.dll -package RDotNet
$MONO_HOME/bin/gacutil -i DynamicInterop/bin/Debug/DynamicInterop.dll -package DynamicInterop

mcs @temp.rsp
rm temp.rsp

./install_DGAC.sh

