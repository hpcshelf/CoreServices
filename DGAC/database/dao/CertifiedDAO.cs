﻿using System;
using System.Data;
using System.Collections.Generic;


namespace org.hpcshelf.database
{

    [Serializable()]
    public class CertifiedDAO
    {
        public void insert(Certified ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO certified (id_concrete, id_functor_app)" +
                    " VALUES (" + ac.Id_concrete + "," + ac.Id_functor_app + ")";

            Console.WriteLine("CertifiedDAO.cs: TRY INSERT: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);
        }

        public Certified retrieve(int id_concrete, int id_functor_app, IDbConnection dbcon)
        {
            Certified c = null;

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = "SELECT id_concrete, id_functor_app " +
                         "FROM certified " +
                         "WHERE id_concrete=" + id_concrete + " AND id_functor_app=" + id_functor_app;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                c = new Certified();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_functor_app = (int)reader["id_functor_app"];
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return c;
        }

        public IList<Certified> list(int id_concrete, IDbConnection dbcon)
        {
            IList<Certified> cList = new List<Certified>();
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_functor_app " +
                 "FROM certified " + 
                 "WHERE id_concrete=" + id_concrete;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Certified c = new Certified();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                cList.Add(c);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return cList;
        }

    }//class

}//namespace
