﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class SupplyParameterComponentDAO
    {


        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insert(SupplyParameterComponent acc, IDbConnection dbcon)
        {
            string sql =
            "INSERT INTO supplyparametercomponent (id_parameter, id_functor_app, id_functor_app_actual)" +
            " VALUES ('" + acc.Id_parameter + "'," + acc.Id_functor_app + "," + acc.Id_functor_app_actual + ")";

            Console.WriteLine("SupplyParameterComponent.cs: TRY INSERT: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);

            invalidate_cache_1[acc.Id_functor_app] = true;
        }

        public SupplyParameterComponent retrieve(string id_parameter, int id_functor_app, IDbConnection dbcon)

        {

            SupplyParameterComponent spc = null;
         //   IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_parameter, id_functor_app, id_functor_app_actual " +
                "FROM supplyparametercomponent " +
                "WHERE id_parameter like '" + id_parameter + "' AND id_functor_app=" + id_functor_app;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                spc = new SupplyParameterComponent();
                spc.Id_parameter = (string)reader["id_parameter"];
                spc.Id_functor_app = (int)reader["id_functor_app"];
                spc.Id_functor_app_actual = (int)reader["id_functor_app_actual"];

            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            if (spc == null)
            {
                Console.WriteLine("SupplyParameterComponentDAO.cs: Parameter NOT FOUND " + id_parameter + "," + id_functor_app);
            }

            return spc;
        }//list

        private IDictionary<int, IList<SupplyParameterComponent>> cache_1 = new Dictionary<int, IList<SupplyParameterComponent>>();
        private IDictionary<int, bool> invalidate_cache_1 = new Dictionary<int, bool>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<SupplyParameterComponent> list(int id_functor_app, IDbConnection dbcon)
        {
            IList<SupplyParameterComponent> list = null;

            if (cache_1.TryGetValue(id_functor_app, out list) && !invalidate_cache_1.ContainsKey(id_functor_app)) 
                return list;
            list = new List<SupplyParameterComponent>();

            if (invalidate_cache_1.ContainsKey(id_functor_app))
            {
                invalidate_cache_1.Remove(id_functor_app);
                cache_1.Remove(id_functor_app);
            }
            cache_1[id_functor_app] = list;

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_parameter, id_functor_app, id_functor_app_actual " +
                "FROM supplyparametercomponent " +
                "WHERE id_functor_app=" + id_functor_app;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                SupplyParameterComponent sp = new SupplyParameterComponent();
                sp.Id_parameter = (string)reader["id_parameter"];
                sp.Id_functor_app = (int)reader["id_functor_app"];
                sp.Id_functor_app_actual = (int)reader["id_functor_app_actual"];
                list.Add(sp);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;

        }//list

    }//class

}//namespace