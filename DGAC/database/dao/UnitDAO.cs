﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class UnitDAO
    {
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insert(Unit ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO unit (id_unit, id_abstract, id_interface, id_concrete, assembly_string, id_unit_super, class_name, class_nargs, uri_source, `order`)" +
                " VALUES ('" + ac.Id_unit + "'," + ac.Id_abstract + ",'" + ac.Id_interface + "'," + ac.Id_concrete + ",'" + ac.Assembly_string + "','" + ac.Id_unit_super + "','" + ac.Class_name + "'," + ac.Class_nargs + ",'" + ac.URI_Source + "'," + ac.Order + ")";

            //Console.WriteLine("Unit.cs: TRY INSERT 1: " + sql);

            invalidate_cache_1[new Tuple<int, string>(ac.Id_concrete, ac.Id_unit)] = true;

            DBConnector.performSQLUpdate(sql, dbcon);
        }

        public IList<Unit> list(int id_concrete, IDbConnection dbcon)
        {

            IList<Unit> list = new List<Unit>();
          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_unit, id_concrete, id_interface,id_abstract, assembly_string, `order`, id_unit_super, class_name, class_nargs, uri_source " +
                "FROM unit " +
                "WHERE id_concrete=" + id_concrete + " " +
                "ORDER BY id_unit";
            dbcmd.CommandText = sql;
           // Console.WriteLine(sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Unit u = new Unit();
                u.Id_unit = (string)reader["id_unit"];
                u.Id_concrete = (int)reader["id_concrete"];
                u.Id_interface = (string)reader["id_interface"];
                u.Id_abstract = (int)reader["id_abstract"];
                u.Assembly_string = (string)reader["assembly_string"];
                u.Id_unit_super = (string)reader["id_unit_super"];
                u.Class_name = (string)reader["class_name"];
                u.Class_nargs = (int)reader["class_nargs"];
                u.URI_Source = (string)reader["uri_source"];
                u.Order = (int)reader["order"];
                list.Add(u);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;

        }//list

        private IDictionary<Tuple<int, string>, Unit> cache_1 = new Dictionary<Tuple<int, string>, Unit>();
        private IDictionary<Tuple<int, string>, bool> invalidate_cache_1 = new Dictionary<Tuple<int, string>, bool>();


        //(); ADDED BY HERON
        [MethodImpl(MethodImplOptions.Synchronized)]
        public Unit retrieve(int id_concrete, string id_unit, IDbConnection dbcon)
        {
            Tuple<int, string> key = new Tuple<int, string>(id_concrete, id_unit);
            if (cache_1.ContainsKey(key) && !invalidate_cache_1.ContainsKey(key))
                return cache_1[key];

            Unit u = null;
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_unit, id_concrete, id_interface, id_abstract, `order`, assembly_string, id_unit_super, class_name, class_nargs, uri_source " +
                "FROM unit " +
                "WHERE id_concrete=" + id_concrete + " AND " +
                "id_unit like '" + id_unit + "'";

            dbcmd.CommandText = sql;
          //  Console.WriteLine(sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                u = new Unit();
                u.Id_unit = (string)reader["id_unit"];
                u.Id_concrete = (int)reader["id_concrete"];
                u.Id_interface = (string)reader["id_interface"];
                u.Id_abstract = (int)reader["id_abstract"];
                u.Assembly_string = (string)reader["assembly_string"];
                u.Id_unit_super = (string)reader["id_unit_super"];
                u.Class_name = (string)reader["class_name"];
                u.Class_nargs = (int)reader["class_nargs"];
                u.URI_Source = (string)reader["uri_source"];
                u.Order = (int)reader["order"];
                if (invalidate_cache_1.ContainsKey(key))
                {
                    invalidate_cache_1.Remove(key);
                    cache_1.Remove(key);
                }
                cache_1[key] = u;
            }
            else
            {
                // UNIT NOT FOUND
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return u;

        }//list


    }//class

}//namespace
