﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Threading;
using System.Runtime.CompilerServices;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class InterfaceDAO
    {
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insert(Interface ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO interface (id_interface, " +
                                        "facet, " +
                                       "id_abstract, " +
                                       "assembly_string, " +
                                       "id_interface_super, " +
                                        "class_name, " +
                                       "class_nargs, " +
                                       "uri_source, " +
                                       "is_parallel, " +
                                       "`order`)" +
                " VALUES ('" + ac.Id_interface + "',"
                             + ac.Facet + ", "
                             + ac.Id_abstract + ",'"
                             + ac.Assembly_string + "','"
                             + ac.Id_interface_super + "','"
                             + ac.Class_name + "',"
                             + ac.Class_nargs + ",'"
                             + ac.URI_Source + "',"
                             + (ac.Is_parallel ? "1" : "0") + ","
                             + ac.Order + ")";

            Console.WriteLine("Interface.cs: TRY INSERT: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);

			invalidate_cache_1[new Tuple<int, string>(ac.Id_abstract, ac.Id_interface)] = true;
			invalidate_cache_2[new Tuple<int, string>(ac.Id_abstract, ac.Id_interface)] = true;
			invalidate_cache_3[ac.Id_abstract] = true;
		}


        private IDictionary<Tuple<int, string>, Interface> cache_1 = new Dictionary<Tuple<int, string>, Interface>();
        private IDictionary<Tuple<int, string>, bool> invalidate_cache_1 = new Dictionary<Tuple<int, string>, bool>();


        public IList<Interface> retrieve(int id_abstract_1, int id_abstract_2, string id_interface, IDbConnection dbcon)
        {
            //            if (id_abstract_1 == 148 && id_abstract_2 == 140 && id_interface.Equals("peer"))
            //                Console.Write(true);

            AbstractComponentFunctor acf = Backend.acfdao.retrieve(id_abstract_1, dbcon);

            IDictionary<Interface, ISet<Interface>> m = new Dictionary<Interface, ISet<Interface>>();
            IList<Interface> i_list = list(id_abstract_1, dbcon);
            foreach (Interface i in i_list)
            {
                m[i] = new HashSet<Interface>();
                m[i].Add(i);
            }


            while (acf.Id_abstract != id_abstract_2)
            {
                AbstractComponentFunctorApplication acfa = Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
                acf = Backend.acfdao.retrieve(acfa.Id_abstract, dbcon);

                IDictionary<Interface, ISet<Interface>> m_ = new Dictionary<Interface, ISet<Interface>>();

                foreach (KeyValuePair<Interface, ISet<Interface>> m_item in m)
                {
                    m_[m_item.Key] = new HashSet<Interface>();

                    foreach (Interface ii in m_item.Value)
                    {
                        string[] id_interface_super_list = ii.Id_interface_super.Split(new char[1] { '+' });
                        foreach (string id_interface_super in id_interface_super_list)
                        {
                            Interface ii_super = Backend.idao.retrieve(acf.Id_abstract, id_interface_super, dbcon);
                            m_[m_item.Key].Add(ii_super);
                        }
                    }

                    m = m_;
                }
            }

            IList<Interface> i_result = new List<Interface>();

            foreach (KeyValuePair<Interface, ISet<Interface>> m_item in m)
                foreach (Interface i in m_item.Value)
                    if (i.Id_interface.Equals(id_interface))
                        i_result.Add(m_item.Key);

            return i_result;
        }

        // UPDATED BY HERON (id_abstract is now a key)
        [MethodImpl(MethodImplOptions.Synchronized)]
        public Interface retrieve(int id_abstract, string id_interface, IDbConnection dbcon)
        {
            Tuple<int, string> key = new Tuple<int, string>(id_abstract, id_interface);
            if (cache_1.ContainsKey(key) && !invalidate_cache_1.ContainsKey(key))
                return cache_1[key];

            Interface i = null;
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_interface, facet, id_abstract, assembly_string, id_interface_super, id_interface_super_top, class_name, class_nargs, uri_source, `order`, is_parallel " +
                "FROM interface " +
                "WHERE id_interface like '" + id_interface + "' AND " +
                     "id_abstract=" + id_abstract;

            dbcmd.CommandText = sql;
            // Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }

            if (reader.Read())
            {
                i = new Interface();
                i.Id_interface = (string)reader["id_interface"];
                i.Facet = (int)reader["facet"];
                i.Id_abstract = (int)reader["id_abstract"];
                i.Assembly_string = (string)reader["assembly_string"];
                i.Id_interface_super = (string)reader["id_interface_super"];
                i.Is_parallel = ((int)reader["is_parallel"]) != 0;
                if (reader["id_interface_super_top"].ToString().Equals(""))
                    i.SetId_interface_super_top(null);
                else
                    i.SetId_interface_super_top((string)reader["id_interface_super_top"]);
                i.Class_name = (string)reader["class_name"];
                i.Class_nargs = (int)reader["class_nargs"];
                i.URI_Source = (string)reader["uri_source"];
                i.Order = (int)reader["order"];

                if (invalidate_cache_1.ContainsKey(key))
                {
                    invalidate_cache_1.Remove(key);
                    cache_1.Remove(key);
                }

                cache_1[key] = i;
            }
            else
            {
                // UNIT NOT FOUND
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            if (i == null)
                Console.Error.WriteLine("InterfaceDAO.cs: Interface not found ! id_abstract=" + id_abstract + ", id_interface=" + id_interface);

            return i;
        }


        private IDictionary<Tuple<int, string>, Interface> cache_2 = new Dictionary<Tuple<int, string>, Interface>();
        private IDictionary<Tuple<int, string>, bool> invalidate_cache_2 = new Dictionary<Tuple<int, string>, bool>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Interface retrieveTop(int id_abstract, string id_interface, IDbConnection dbcon)
        {
            Tuple<int, string> key = new Tuple<int, string>(id_abstract, id_interface);
            if (cache_2.ContainsKey(key) & !invalidate_cache_2.ContainsKey(key))
                return cache_2[key];

            IList<Interface> iList = Backend.idao.list(id_abstract, dbcon);
            foreach (Interface i in iList)
            {
                string[] interface_ids_tops = Interface.splitIDs(i.GetId_interface_super_top(dbcon));

                foreach (string interface_id_top in interface_ids_tops)
                {
                    if (interface_id_top.Equals(id_interface))
                    {
                        if (cache_2.ContainsKey(key))
                        {
                            cache_2.Remove(key);
                            invalidate_cache_2.Remove(key);
                        }

                        cache_2[key] = i;
                        return i;
                    }
                }
            }

            return null;
        }

        private IDictionary<int, IList<Interface>> cache_3 = new Dictionary<int, IList<Interface>>();
        private IDictionary<int, bool> invalidate_cache_3 = new Dictionary<int, bool>();

        // UPDATED BY HERON (id_abstract is now a key)
        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<Interface> list(int id_abstract, IDbConnection dbcon)
        {
            if (cache_3.ContainsKey(id_abstract) && !invalidate_cache_3.ContainsKey(id_abstract))
                return cache_3[id_abstract];

            IList<Interface> iList = new List<Interface>();

            if (invalidate_cache_3.ContainsKey(id_abstract))
            {
                invalidate_cache_3.Remove(id_abstract);
                cache_3.Remove(id_abstract);
            }
            cache_3[id_abstract] = iList;

     //       IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_interface, facet, id_abstract, assembly_string, id_interface_super, id_interface_super_top, class_name, class_nargs, uri_source, `order`, is_parallel " +
                "FROM interface " +
                "WHERE id_abstract=" + id_abstract + " ORDER BY `order`";
            //Console.WriteLine ("InterfaceDAO - list :" + sql);
            dbcmd.CommandText = sql;
            //Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }


            while (reader.Read())
            {
                Interface i = new Interface();
                i.Id_interface = (string)reader["id_interface"];
                i.Facet = (int)reader["facet"];
                i.Id_abstract = (int)reader["id_abstract"];
                i.Assembly_string = (string)reader["assembly_string"];
                i.Is_parallel = ((int)reader["is_parallel"]) != 0;
                if (reader["id_interface_super_top"].ToString().Equals(""))
                    i.SetId_interface_super_top(null);
                else
                    i.SetId_interface_super_top((string)reader["id_interface_super_top"]);

                i.Id_interface_super = (string)reader["id_interface_super"];

                i.Class_name = (string)reader["class_name"];
                i.Class_nargs = (int)reader["class_nargs"];
                i.URI_Source = (string)reader["uri_source"];
                i.Order = (int)reader["order"];
                iList.Add(i);
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return iList;
        }

        private IDictionary<Tuple<int, string>, IList<Interface>> cache_6 = new Dictionary<Tuple<int, string>, IList<Interface>>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<Interface> listByInterface(int id_abstract, string id_interface, IDbConnection dbcon)
        {
            Tuple<int, string> key = new Tuple<int, string>(id_abstract, id_interface);
            if (cache_6.ContainsKey(key))
                return cache_6[key];

            IList<Interface> iList = new List<Interface>();
            cache_6[key] = iList;

          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_interface, facet, id_abstract, assembly_string, id_interface_super, id_interface_super_top, class_name, class_nargs, uri_source, `order`, is_parallel " +
                "FROM interface " +
                "WHERE id_abstract=" + id_abstract + " and id_interface ='" + id_interface + "' ORDER BY `order`";
            dbcmd.CommandText = sql;
            //Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Interface i = new Interface();
                i.Id_interface = (string)reader["id_interface"];
                i.Facet = (int)reader["facet"];
                i.Id_abstract = (int)reader["id_abstract"];
                i.Assembly_string = (string)reader["assembly_string"];
                i.Is_parallel = ((int)reader["is_parallel"]) != 0;
                if (reader["id_interface_super_top"].ToString().Equals(""))
                    i.SetId_interface_super_top(null);
                else
                    i.SetId_interface_super_top((string)reader["id_interface_super_top"]);

                i.Id_interface_super = (string)reader["id_interface_super"];
                //i.Unit_replica_super = (int)reader["unit_replica_super"];

                i.Class_name = (string)reader["class_name"];
                i.Class_nargs = (int)reader["class_nargs"];
                i.URI_Source = (string)reader["uri_source"];
                i.Order = (int)reader["order"];
                iList.Add(i);
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return iList;
        }


        private IDictionary<Tuple<int, int>, IList<Interface>> cache_7 = new Dictionary<Tuple<int, int>, IList<Interface>>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<Interface> listByFacet(int id_abstract, int facet, IDbConnection dbcon)
        {
            Tuple<int, int> key = new Tuple<int, int>(id_abstract, facet);
            if (cache_7.ContainsKey(key))
                return cache_7[key];

            IList<Interface> iList = new List<Interface>();
            cache_7[key] = iList;

          // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_interface, facet, id_abstract, assembly_string, id_interface_super, id_interface_super_top, class_name, class_nargs, uri_source, `order`, is_parallel " +
                "FROM interface " +
                "WHERE id_abstract=" + id_abstract + " and facet ='" + facet + "' ORDER BY `order`";
            dbcmd.CommandText = sql;
            //Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Interface i = new Interface();
                i.Id_interface = (string)reader["id_interface"];
                i.Facet = (int)reader["facet"];
                i.Id_abstract = (int)reader["id_abstract"];
                i.Assembly_string = (string)reader["assembly_string"];
                i.Is_parallel = ((int)reader["is_parallel"]) != 0;
                if (reader["id_interface_super_top"].ToString().Equals(""))
                {
                    i.SetId_interface_super_top(null);
                }
                else
                {
                    i.SetId_interface_super_top((string)reader["id_interface_super_top"]);
                }

                i.Id_interface_super = (string)reader["id_interface_super"];

                i.Class_name = (string)reader["class_name"];
                i.Class_nargs = (int)reader["class_nargs"];
                i.URI_Source = (string)reader["uri_source"];
                i.Order = (int)reader["order"];
                iList.Add(i);
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return iList;
        }


        internal void setInterfaceSuperTop(int id_abstract, string id_interface, string id_interface_super_top, IDbConnection dbcon)
        {
            String sql =
                "UPDATE interface SET id_interface_super_top = '" + id_interface_super_top + "' " +
                " WHERE id_abstract=" + id_abstract + " AND id_interface like '" + id_interface + "'";

            //	Console.WriteLine ("setInterfaceSuperTop: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);
        }

        private IDictionary<Tuple<int, int, string>, IList<Interface>> cache_8 = new Dictionary<Tuple<int, int, string>, IList<Interface>>();

        //private readonly object lock_cache_8 = new object();

        private IDictionary<Tuple<int, int, string>, ManualResetEvent> a = new Dictionary<Tuple<int, int, string>, ManualResetEvent>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal IList<Interface> retrieveByMatching(int id_abstract_1, int id_abstract_2, string id_interface, IDbConnection dbcon)
        {
            Tuple<int, int, string> key = new Tuple<int, int, string>(id_abstract_1, id_abstract_2, id_interface);

            if (cache_8.ContainsKey(key))
                return cache_8[key];

            /* IList<Interface> result_list = null;
            bool found_key;
            lock (lock_cache_8)
            {
                found_key = cache_8.ContainsKey(key);
                if (!found_key)
                {
                    cache_8[key] = result_list = new List<Interface>();
                    a[key] = new ManualResetEvent(false);
                }
            }
            if (found_key)
            {
                a[key].WaitOne();
                return cache_8[key];
            }
            */

            IList<Interface> result_list = new List<Interface>();

            Interface i = retrieve(id_abstract_2, id_interface, dbcon);
            string[] id_interface_super_top_2 = Interface.splitIDs(i.GetId_interface_super_top(dbcon));

            IList<Interface> iList = list(id_abstract_1, dbcon);
            foreach (Interface iSuperCandidate in iList)
            {
                string[] id_interface_super_top_1 = Interface.splitIDs(iSuperCandidate.GetId_interface_super_top(dbcon));

                // CHECK WHETHER this candidate to unit 1 is transitively inherited from a subset of the units from which the unit 2 was derived.
                bool found = false;
                foreach (string x in id_interface_super_top_1)
                {
                    found = false;
                    foreach (string y in id_interface_super_top_2)
                    {
                        if (x.Equals(y))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        break;
                }
                if (found)
                    result_list.Add(iSuperCandidate);
            }


            return result_list;
        }

        internal void setPublicKey(int id_abstract, string id_interface, string publicKey, IDbConnection dbcon)
        {
            Interface i = this.retrieve(id_abstract, id_interface, dbcon);

            String s = ", PublicKey=";

            if (i.Assembly_string.IndexOf(s) < 0)
            {
                i.Assembly_string += s + publicKey;
            }
            else
            {
                int index = i.Assembly_string.IndexOf(s);
                i.Assembly_string = i.Assembly_string.Substring(0, index) + s + publicKey;
            }



            this.updatePublicKey(i, dbcon);

        }

        private void updatePublicKey(Interface i, IDbConnection dbcon)
        {
            String sql = "UPDATE interface SET assembly_string = '" + i.Assembly_string + "' " +
                         " WHERE id_abstract=" + i.Id_abstract + " AND " +
                         "id_interface like '" + i.Id_interface + "'";

            DBConnector.performSQLUpdate(sql, dbcon);
        }

        private IDictionary<string, Interface> cache_5 = new Dictionary<string, Interface>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal Interface retrieve_libraryPath(string library_path, IDbConnection dbcon)
        {
            if (cache_5.ContainsKey(library_path))
                return cache_5[library_path];

            Interface i = null;
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_interface, facet, id_abstract, assembly_string, id_interface_super, id_interface_super_top, class_name, class_nargs, uri_source, `order`, is_parallel " +
                "FROM interface " +
                "WHERE class_name like '" + library_path + "'";
            dbcmd.CommandText = sql;
            //	Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                i = new Interface();
                i.Id_interface = (string)reader["id_interface"];
                i.Facet = (int)reader["facet"];
                i.Id_abstract = (int)reader["id_abstract"];
                i.Assembly_string = (string)reader["assembly_string"];
                i.Id_interface_super = (string)reader["id_interface_super"];
                i.Is_parallel = ((int)reader["is_parallel"]) != 0;
                if (reader["id_interface_super_top"].ToString().Equals(""))
                    i.SetId_interface_super_top(null);
                else
                    i.SetId_interface_super_top((string)reader["id_interface_super_top"]);
                i.Class_name = (string)reader["class_name"];
                i.Class_nargs = (int)reader["class_nargs"];
                i.URI_Source = (string)reader["uri_source"];
                i.Order = (int)reader["order"];
                cache_5[library_path] = i;
            }
            else
            {
                // UNIT NOT FOUND
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return i;
        }


        private IDictionary<Tuple<int, string>, Interface> cache_4 = new Dictionary<Tuple<int, string>, Interface>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Interface retrieveSuper(int id_abstract, string id_interface, IDbConnection dbcon)
        {
            Tuple<int, string> key = new Tuple<int, string>(id_abstract, id_interface);
            if (cache_4.ContainsKey(key))
                return cache_4[key];

            //	Console.WriteLine ("retrieveSuper - BEGIN 1 " + id_abstract + "/" + id_interface);
            bool loop;
            AbstractComponentFunctor acf = DGAC.Backend.acfdao.retrieve(id_abstract, dbcon);

            IList<Interface> iList = Backend.idao.list(id_abstract, dbcon);
            //	Console.WriteLine ("retrieveSuper - BEGIN 2 " + iList.Count);
            foreach (Interface i in iList)
            {
                IList<Tuple<AbstractComponentFunctor, Interface>> queue = new List<Tuple<AbstractComponentFunctor, Interface>>();
                queue.Add(new Tuple<AbstractComponentFunctor, Interface>(acf, i));
                while (queue.Count > 0)
                {
                    AbstractComponentFunctor acfCurr = queue[0].Item1;
                    Interface iCurr = queue[0].Item2;
                    queue.RemoveAt(0);
                    //		Console.WriteLine ("retrieveSuper - DEQUEUE - id_abstract=" + acfCurr.Id_abstract + " / id_interface=" + iCurr.Id_interface);
                    if (iCurr.Id_interface.Equals(id_interface))
                    {
                        //		Console.WriteLine ("retrieveSuper: found id_abstract=" + acfCurr.Id_abstract + " / id_interface=" + iCurr.Id_interface);
                        cache_4[key] = i;
                        return i;
                    }
                    else if (loop = acfCurr.Id_functor_app_supertype > 0)
                    {
                        AbstractComponentFunctorApplication acfaSuper = DGAC.Backend.acfadao.retrieve(acfCurr.Id_functor_app_supertype, dbcon);
                        acfCurr = DGAC.Backend.acfdao.retrieve(acfaSuper.Id_abstract, dbcon);
                        string[] id_interface_super_list = Interface.splitIDs(iCurr.Id_interface_super);
                        foreach (string id_interface_super in id_interface_super_list)
                        {
                            //	Console.WriteLine ("retrieveSuper: id_abstract=" + acfCurr.Id_abstract + " / id_interface=" +iCurr.Id_interface);
                            iCurr = DGAC.Backend.idao.retrieve(acfaSuper.Id_abstract, id_interface_super, dbcon);
                            queue.Add(new Tuple<AbstractComponentFunctor, Interface>(acfCurr, iCurr));
                        }
                    }
                }
            }

            //	Console.WriteLine ("retrieveSuper - END NULL");
            return null;
        }

        public int count_facets(int id_abstract, IDbConnection dbcon)
        {
            Interface i = null;
          // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = "SELECT DISTINCT facet FROM interface where id_abstract=" + id_abstract + " and facet>=0 group by facet";
            dbcmd.CommandText = sql;

            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            int count = 0;
            while (reader.Read()) count++;

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return count;
        }


    }//class

}//namespace
