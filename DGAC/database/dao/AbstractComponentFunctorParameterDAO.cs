﻿using System;
using System.Data;
using System.Collections.Generic;
using org.hpcshelf.DGAC.utils;
using System.Runtime.CompilerServices;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class AbstractComponentFunctorParameterDAO
    {

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insert(AbstractComponentFunctorParameter ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO abstractcomponentfunctorparameter (id_parameter, id_abstract, bounds_of, variance, `order`, class)" +
                " VALUES ('" + ac.Id_parameter + "'," + ac.Id_abstract + "," + ac.Bounds_of + ",'" + ac.Variance + "'," + ac.Order + ",'" + ac.ParameterClass + "')";

            Console.WriteLine("AbstractComponentFunctorParameter.cs: TRY INSERT: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);

            invalidate_cache_1[ac.Id_abstract] = true;
            invalidate_cache_1_static[ac.Id_abstract] = true;
            invalidate_cache_2[new Tuple<int, string>(ac.Id_abstract, ac.Id_parameter)] = true;

        }


        IDictionary<int, IList<AbstractComponentFunctorParameter>> cache_1 = new Dictionary<int, IList<AbstractComponentFunctorParameter>>();
        IDictionary<int, bool> invalidate_cache_1 = new Dictionary<int, bool>();

        IDictionary<int, IList<AbstractComponentFunctorParameter>> cache_1_static = new Dictionary<int, IList<AbstractComponentFunctorParameter>>();
        IDictionary<int, bool> invalidate_cache_1_static = new Dictionary<int, bool>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<AbstractComponentFunctorParameter> list(int id_abstract, IDbConnection dbcon)
        {
            IList<AbstractComponentFunctorParameter> list = null;

            if (!invalidate_cache_1.ContainsKey(id_abstract) && cache_1.TryGetValue(id_abstract, out list))
                return list;

            list = new List<AbstractComponentFunctorParameter>();

            if (invalidate_cache_1.ContainsKey(id_abstract))
            {
                invalidate_cache_1.Remove(id_abstract);
                cache_1.Remove(id_abstract);
            }
			cache_1[id_abstract] = list;

            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_parameter, bounds_of, variance, `order`, class " +
                "FROM abstractcomponentfunctorparameter " +
                "WHERE id_abstract=" + id_abstract + " ORDER BY `order`";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                AbstractComponentFunctorParameter acfp = new AbstractComponentFunctorParameter();
                acfp.Bounds_of = (int)reader["bounds_of"];
                acfp.Id_abstract = (int)reader["id_abstract"];
                acfp.Id_parameter = (string)reader["id_parameter"];
                acfp.Variance = Constants.varianceValueInv[(string)reader["variance"]];
                acfp.Order = (int)reader["order"];
                acfp.ParameterClass = AbstractComponentFunctorParameter.parameterClass[(string)reader["class"]];
				list.Add(acfp);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;

        }//lis

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<AbstractComponentFunctorParameter> listStaticParameters(int id_abstract, IDbConnection dbcon)
		{
			IList<AbstractComponentFunctorParameter> list = null;

			if (!invalidate_cache_1_static.ContainsKey(id_abstract) && cache_1_static.TryGetValue(id_abstract, out list))
				return list;

			list = new List<AbstractComponentFunctorParameter>();

			if (invalidate_cache_1_static.ContainsKey(id_abstract))
			{
				invalidate_cache_1_static.Remove(id_abstract);
				cache_1_static.Remove(id_abstract);
			}
			cache_1_static[id_abstract] = list;

		//  IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT id_abstract, id_parameter, bounds_of, variance, `order`, class " +
				"FROM abstractcomponentfunctorparameter " +
				"WHERE id_abstract=" + id_abstract + " ORDER BY `order`";
            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader(); 
			while (reader.Read())
			{
                string parameter_class = (string)reader["class"];

                if (new List<string>() { ParameterClass.application.ToString(), ParameterClass.platform.ToString() }.Contains(parameter_class))
                {
                    AbstractComponentFunctorParameter acfp = new AbstractComponentFunctorParameter();
                    acfp.Bounds_of = (int)reader["bounds_of"];
                    acfp.Id_abstract = (int)reader["id_abstract"];
                    acfp.Id_parameter = (string)reader["id_parameter"];
                    acfp.Variance = Constants.varianceValueInv[(string)reader["variance"]];
                    acfp.Order = (int)reader["order"];
                    acfp.ParameterClass = AbstractComponentFunctorParameter.parameterClass[parameter_class];
                    list.Add(acfp);
                }
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//lis
		

        IDictionary<Tuple<int, string>, AbstractComponentFunctorParameter> cache_2 = new Dictionary<Tuple<int, string>, AbstractComponentFunctorParameter>();
        IDictionary<Tuple<int, string>, bool> invalidate_cache_2 = new Dictionary<Tuple<int, string>, bool>();

        private Tuple<int, string> makeKey(int id_abstract, string id_parameter)
        {
            return new Tuple<int, string>(id_abstract, id_parameter);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AbstractComponentFunctorParameter retrieve(int id_abstract, string id_parameter, IDbConnection dbcon)
        {
            AbstractComponentFunctorParameter acfp = null;

            Tuple<int, string> key = makeKey(id_abstract, id_parameter);
            if (cache_2.TryGetValue(key, out acfp) && !invalidate_cache_2.ContainsKey(key))
                return acfp;

            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_parameter, bounds_of, variance, `order`, class " +
                "FROM abstractcomponentfunctorparameter " +
                "WHERE id_abstract=" + id_abstract + " AND id_parameter like '" + id_parameter + "'";
          //  Console.WriteLine("AbstractComponentFunctorParameterDAO - retrieve: " + sql);
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                acfp = new AbstractComponentFunctorParameter();
                acfp.Bounds_of = (int)reader["bounds_of"];
                acfp.Id_abstract = (int)reader["id_abstract"];
                acfp.Id_parameter = (string)reader["id_parameter"];
                acfp.Variance = Constants.varianceValueInv[(string)reader["variance"]];
				acfp.Order = (int)reader["order"];
                acfp.ParameterClass = AbstractComponentFunctorParameter.parameterClass[(string)reader["class"]];
				if (invalidate_cache_2.ContainsKey(key))
                {
                    invalidate_cache_2.Remove(key);
                    cache_2.Remove(key);
                }
                cache_2[key] = acfp;
            }//if

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            //  if (acfp==null) 
            //  {
            // 	  Console.WriteLine("AbstractComponentFunctorParameterDAO.cs: PARAMETER NOT FOUND "+ id_abstract + "," + id_parameter);
            // }

            return acfp;
        }
    }//class

}//namespace