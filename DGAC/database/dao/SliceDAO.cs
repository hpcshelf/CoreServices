﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class SliceDAO
    {

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insert(Slice ac, IDbConnection dbcon)
        {
            String sql =
				"INSERT INTO slice (id_abstract, id_interface, id_interface_slice, id_inner, transitive, property_name)" +
				" VALUES (" + ac.Id_abstract + ",'" + ac.Id_interface + "','" + ac.Id_interface_slice + "','" + ac.Id_inner + "'," + (ac.Transitive ? -1 : 0) + ",'" + ac.PortName + "')";

			//	Console.WriteLine("SliceDAO.cs: TRY INSERT SLICE : " + sql);

			invalidate_cache_1[new Tuple<int, string>(ac.Id_abstract, ac.Id_inner)] = true;
			invalidate_cache_2[new Tuple<int, string>(ac.Id_abstract, ac.Id_interface)] = true;

			DBConnector.performSQLUpdate(sql, dbcon);
		
         }


        // ADDED BY HERON
		public Slice retrieve(int id_abstract, string id_inner, string id_interface_slice, string id_interface, IDbConnection dbcon)
        {
            Slice s = null;
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
				"SELECT id_abstract, " +
				       "id_interface, " +
				       "id_interface_slice, " +
				       "id_inner, " +
				       "transitive, " +
				       "property_name " +
                "FROM slice " +
                "WHERE id_abstract=" + id_abstract + " AND " +
                "id_inner like '" + id_inner + "' AND " + 
                "id_interface_slice like '" + id_interface_slice + "' AND " +
				"id_interface like '" + id_interface + "'";
            dbcmd.CommandText = sql;
	//		Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                s = new Slice();
                s.Id_abstract = (int)reader["id_abstract"];
                s.Id_interface = (string)reader["id_interface"];
		//		s.Unit_replica_host = (int)reader["unit_replica_host"];
                s.Id_interface_slice = (string)reader["id_interface_slice"];
                s.Id_inner = (string)reader["id_inner"];
      //          s.Slice_replica = (int)reader["slice_replica"];
	//			s.Inner_replica = (int)reader["inner_replica"];
	//			s.Unit_replica = (int)reader["unit_replica"];
                s.Transitive = ((int)reader["transitive"]) == 0 ? false : true;
                s.PortName = (string)reader["property_name"];
            }
            else
            {
                // UNIT NOT FOUND
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
			
	//	    if (s==null) 
	//	    {
	//	  	   Console.WriteLine("SliceDAO.cs: Slice NOT FOUND " + "id_abstract=" + id_abstract + ", id_inner=" + id_inner + ", id_interface_slice=" + id_interface_slice);
	//	    }
				
            return s;

        }//list

        // ADDED BY HERON
        public Slice retrieve2(int id_abstract, string id_inner, int inner_replica, string id_interface_slice, string id_interface, IDbConnection dbcon)
        {
            Slice s = null;
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
				"SELECT id_abstract, " +
				       "id_interface, " +
				       "id_interface_slice, " +
				       "id_inner, " +
				       "transitive, " +
				       "property_name " +
                "FROM slice " +
                "WHERE id_abstract=" + id_abstract + " AND " +
                "id_inner like '" + id_inner + "' AND " + 
                "id_interface_slice like '" + id_interface_slice + "' AND " +
                "id_interface like '" + id_interface + "'";
            dbcmd.CommandText = sql;
		//	Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                s = new Slice();
                s.Id_abstract = (int)reader["id_abstract"];
                s.Id_interface = (string)reader["id_interface"];
                s.Id_interface_slice = (string)reader["id_interface_slice"];
                s.Id_inner = (string)reader["id_inner"];
         //       s.Slice_replica = (int)reader["slice_replica"];
		//		s.Inner_replica = (int)reader["inner_replica"];
		//		s.Unit_replica = (int)reader["unit_replica"];
		//		s.Unit_replica_host = (int)reader["unit_replica_host"];
                s.Transitive = ((int)reader["transitive"]) == 0 ? false : true;
                s.PortName = (string)reader["property_name"];
            }
            else
            {
                // UNIT NOT FOUND
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return s;

        }//list


        IDictionary<Tuple<int, string>, IList<Slice>> cache_1 = new Dictionary<Tuple<int, string>, IList<Slice>>();
        IDictionary<Tuple<int, string>, bool> invalidate_cache_1 = new Dictionary<Tuple<int, string>, bool>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<Slice> listByInner(int id_abstract_start, string id_inner, IDbConnection dbcon)
        {
            Tuple<int, string> key = new Tuple<int, string>(id_abstract_start, id_inner);
            if (cache_1.ContainsKey(key) && !invalidate_cache_1.ContainsKey(key))
                return cache_1[key];

            IList<Slice> list = new List<Slice>();

            if (invalidate_cache_1.ContainsKey(key))
            {
                invalidate_cache_1.Remove(key);
                cache_1.Remove(key);
            }
			cache_1[key] = list;

            //IDbConnection dbcon = DBConnector.DBcon;
            //dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();            

			int id_abstract = id_abstract_start; 
			while (id_abstract > 0) 
			{
				string sql =
					"SELECT id_abstract, " +
					"id_inner, " +
					"id_interface_slice, " +
					"id_interface, " +
					"transitive, " +
					"property_name " +
					"FROM slice " +
					"WHERE id_abstract=" + id_abstract + " and id_inner like '" + id_inner + "'";
				dbcmd.CommandText = sql;
				//Console.WriteLine (sql);
				IDataReader reader = dbcmd.ExecuteReader ();
				if (reader.Read ()) 
				{
					do {
						Slice s = new Slice ();
						s.Id_abstract = (int)reader ["id_abstract"];
						s.Id_inner = (string)reader ["id_inner"];
						s.Id_interface_slice = (string)reader ["id_interface_slice"];
						s.Id_interface = (string)reader ["id_interface"];
						s.Transitive = ((int)reader ["transitive"]) == 0 ? false : true;
						s.PortName = (string)reader ["property_name"];
						list.Add (s);
					} while (reader.Read ());
					id_abstract = 0;
				}
				else 
				{
					AbstractComponentFunctor acf = Backend.acfdao.retrieve (id_abstract, dbcon);
					id_abstract = acf.Id_functor_app_supertype > 0 ? Backend.acfadao.retrieve (acf.Id_functor_app_supertype, dbcon).Id_abstract : 0;
				}
				// clean up
				reader.Close ();
				reader = null;
			}
			dbcmd.Dispose ();
			dbcmd = null;
           // dbcon.Close();
            dbcon = null;
            return list;

        }//list2

		IDictionary<Tuple<int, string>, IList<Slice>> cache_2 = new Dictionary<Tuple<int, string>, IList<Slice>>();
        IDictionary<Tuple<int, string>, bool> invalidate_cache_2 = new Dictionary<Tuple<int, string>, bool>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<Slice> listByInterface(int id_abstract_start, string id_interface_start, IDbConnection dbcon)
        {
			Tuple<int, string> key = new Tuple<int, string>(id_abstract_start, id_interface_start);
            if (cache_2.ContainsKey(key) && !invalidate_cache_2.ContainsKey(key))
				return cache_2[key];

			IList<Slice> list = new List<Slice>();

            if (invalidate_cache_2.ContainsKey(key))
            {
                invalidate_cache_2.Remove(key);
                cache_2.Remove(key);
            }
			cache_2[key] = list;

            IList<Tuple<int,string>> queue = new List<Tuple<int, string>> ();
			queue.Add (new Tuple<int, string> (id_abstract_start, id_interface_start));

            IDictionary<Tuple<string, string>, string> mem = new Dictionary<Tuple<string, string>, string>();

			while (queue.Count > 0) 
			{
				int id_abstract = queue[0].Item1;
				string id_interface = queue[0].Item2;
				queue.RemoveAt (0);

	          //  IDbConnection dbcon = DBConnector.DBcon;
	            IDbCommand dbcmd = dbcon.CreateCommand();
	            string sql =
					"SELECT id_abstract, " +
					       "id_interface, " +
					       "id_interface_slice, " +
					       "id_inner, " +
					       "transitive, " +
					       "property_name " +
	                "FROM slice " +
					"WHERE id_abstract=" + id_abstract + " and id_interface like '" + id_interface + "'";
	            dbcmd.CommandText = sql;
			//	Console.WriteLine (sql);
	            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
	            while (reader.Read())
	            {
	                Slice s = new Slice();
	                s.Id_abstract = (int)reader["id_abstract"];
	                s.Id_interface = (string)reader["id_interface"];
	               s.Id_interface_slice = (string)reader["id_interface_slice"];
	                s.Id_inner = (string)reader["id_inner"];
	                s.Transitive = ((int)reader["transitive"]) == 0 ? false : true;
	                s.PortName = (string)reader["property_name"];

                    Tuple<string, string> key_mem = new Tuple<string, string>(s.Id_inner, s.Id_interface_slice);
                    if (!mem.ContainsKey(key_mem))
                    {
                        mem[key_mem] = s.Id_inner;
						list.Add(s);
					}
	            }//while
				
				// clean up
	            reader.Close();
	            reader = null;
	            dbcmd.Dispose();
	            dbcmd = null;
	            
                AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve(id_abstract, dbcon);
				if (acf.Id_functor_app_supertype > 0)
				{
				   AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
				   Interface i = org.hpcshelf.DGAC.Backend.idao.retrieve(id_abstract, id_interface, dbcon);					
				   id_abstract = acfa.Id_abstract;
				   string[] id_interface_super_list = Interface.splitIDs(i.Id_interface_super);
				   foreach (string id_interface_super in id_interface_super_list) 
						queue.Add (new Tuple<int, string> (id_abstract, id_interface_super));
				}
				else 
					id_abstract = -1;
				   
			}
			
            return list;

        }//list

        public IList<Slice> listByInnerInInterface(int id_abstract, string id_inner, string id_interface, IDbConnection dbcon)
        {

            IList<Slice> list = new List<Slice>();
            //IDbConnection dbcon = DBConnector.DBcon;
            //dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
				"SELECT id_abstract, " +
				       "id_inner, " +
				       "id_interface_slice, " +
				       "id_interface, " +
				       "transitive, " +
				       "property_name " +
                "FROM slice " +
                "WHERE id_abstract=" + id_abstract + 
                 " and id_inner like '" + id_inner + "'" +
                 " and id_interface like '" + id_interface + "'";
            dbcmd.CommandText = sql;
		//	Console.WriteLine (sql);
			           IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Slice s = new Slice();
                s.Id_abstract = (int)reader["id_abstract"];
                s.Id_inner = (string)reader["id_inner"];
                s.Id_interface_slice = (string)reader["id_interface_slice"];
                s.Id_interface = (string)reader["id_interface"];
			//s.Unit_replica_host = (int)reader["unit_replica_host"];
              //  s.Slice_replica = (int)reader["slice_replica"];
			//	s.Inner_replica = (int)reader["inner_replica"];
			//	s.Unit_replica = (int)reader["unit_replica"];
                s.Transitive = ((int)reader["transitive"]) == 0 ? false : true;
                s.PortName = (string)reader["property_name"];
                list.Add(s);
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
           // dbcon.Close();
            dbcon = null;
            return list;

        }//list3

		public IList<Slice> listByInterfaceSlice(int id_abstract, string id_inner, string id_interface_slice, IDbConnection dbcon)
		{

			IList<Slice> list = new List<Slice>();
			// IDbConnection dbcon = DBConnector.DBcon;
			//dbcon.Open();
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT id_abstract, " +
					   "id_inner, " +
					   "id_interface_slice, " +
					   "id_interface, " +
					   "transitive, " +
					   "property_name " +
				"FROM slice " +
				"WHERE id_abstract=" + id_abstract +
				 " and id_inner like '" + id_inner + "'" +
				 " and id_interface_slice like '" + id_interface_slice + "'";
			dbcmd.CommandText = sql;
			//  Console.WriteLine (sql);
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				Slice s = new Slice();
				s.Id_abstract = (int)reader["id_abstract"];
				s.Id_inner = (string)reader["id_inner"];
				s.Id_interface_slice = (string)reader["id_interface_slice"];
				s.Id_interface = (string)reader["id_interface"];
				//s.Unit_replica_host = (int)reader["unit_replica_host"];
				//  s.Slice_replica = (int)reader["slice_replica"];
				//  s.Inner_replica = (int)reader["inner_replica"];
				//  s.Unit_replica = (int)reader["unit_replica"];
				s.Transitive = ((int)reader["transitive"]) == 0 ? false : true;
				s.PortName = (string)reader["property_name"];
				list.Add(s);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			// dbcon.Close();
			dbcon = null;
			return list;

		}//list3

		
        internal IList<Slice> listSlicesOfInner(int id_abstract, string id_inner, string id_interface_slice, IDbConnection dbcon)
        {
            IList<Slice> list = new List<Slice>();
          //  IDbConnection dbcon = DBConnector.DBcon;
            //dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
				"SELECT id_abstract, " +
				       "id_inner, " +
				       "id_interface_slice, " +
				       "id_interface, " +
				       "transitive, " +
				       "property_name " +
                "FROM slice " +
                "WHERE id_abstract=" + id_abstract + " and id_inner like '" + id_inner + "'" + " and id_interface_slice like '" + id_interface_slice + "'";
            dbcmd.CommandText = sql;
			//Console.WriteLine (sql);
			          IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Slice s = new Slice();
                s.Id_abstract = (int)reader["id_abstract"];
                s.Id_inner = (string)reader["id_inner"];
                s.Id_interface_slice = (string)reader["id_interface_slice"];
                s.Id_interface = (string)reader["id_interface"];
			//	s.Unit_replica_host = (int)reader["unit_replica_host"];
            //   s.Slice_replica = (int)reader["slice_replica"];
			//	s.Inner_replica = (int)reader["inner_replica"];
			//	s.Unit_replica = (int)reader["unit_replica"];
                s.Transitive = ((int)reader["transitive"]) == 0 ? false : true;
                s.PortName = (string)reader["property_name"];
                list.Add(s);
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
           // dbcon.Close();
            dbcon = null;
            return list;
        }

        internal IList<int> findFacets(int id_abstract, string id_inner, IDbConnection dbcon)
        {
			IList<int> list = new List<int>();
           // IDbConnection dbcon = DBConnector.DBcon;
            //dbcon.Open();
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT u.facet as facet FROM slice as s, interface as u " +
				"WHERE s.id_abstract=" + id_abstract + " and s.id_inner like '" + id_inner + "' and " +
                      "s.id_abstract = u.id_abstract and s.id_interface like u.id_interface";
			dbcmd.CommandText = sql;
			//Console.WriteLine (sql);
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				int s = (int)reader["facet"];
				list.Add(s);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			//dbcon.Close();
			dbcon = null;
			return list;
		}

        public void delete(int id_abstract, string id_inner_rename, IDbConnection dbcon)
        {
			string sql = "DELETE FROM slice " +
						 "WHERE id_abstract=" + id_abstract +
						  " AND id_inner like '" + id_inner_rename + "'";

			DBConnector.performSQLUpdate(sql, dbcon);
		}
    }//class

}//namespace
