﻿using System;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Diagnostics;


namespace org.hpcshelf.database
{

    [Serializable()]
    public class ComponentKeyDAO
    {


        public void insert(ComponentKey ac, IDbConnection dbcon)
        {
            String sql = "INSERT INTO component_key (id_type, id, snk_contents, pub_contents)" +
                         " VALUES (?id_type_value, ?id_value, ?snk_contents_value, ?pub_contents_value)";

            MySqlParameter idTypeParameter = new MySqlParameter("?id_type_value", MySqlDbType.VarChar, 256);
			MySqlParameter idParameter = new MySqlParameter("?id_value", MySqlDbType.Int32, 11);
            MySqlParameter snkContentsParameter = new MySqlParameter("?snk_contents_value", MySqlDbType.Blob, ac.SNKContents.Length);
            MySqlParameter pubContentsParameter = new MySqlParameter("?pub_contents_value", MySqlDbType.Blob, ac.PUBContents.Length);

            idTypeParameter.Value = ac.IDType;
            idParameter.Value = ac.ID;
            snkContentsParameter.Value = ac.SNKContents;
            pubContentsParameter.Value = ac.PUBContents;

		//	Console.WriteLine("ComponentKey.cs: TRY INSERT 1: " + sql);

            DBConnector.performSQLUpdate(sql, new MySqlParameter[] {idTypeParameter, idParameter, snkContentsParameter, pubContentsParameter}, dbcon);
        }



        //(); ADDED BY HERON
        public ComponentKey retrieve(char id_type, int id, IDbConnection dbcon)
        {
            ComponentKey k = null;
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_type, id, snk_contents, pub_contents " +
                "FROM component_key " +
                "WHERE id_type='" + id_type + "' AND id=" + id;

            dbcmd.CommandText = sql;
           // Console.WriteLine(sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                k = new ComponentKey();
                k.IDType = ((string)reader["id_type"])[0];
                k.ID = (int)reader["id"];
                k.SNKContents = (byte[])reader["snk_contents"];
                k.PUBContents = (byte[])reader["pub_contents"];
            }
            else
            {
                // COMPONENT NOT FOUND
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return k;

        }//list


    }//class

}//namespace
