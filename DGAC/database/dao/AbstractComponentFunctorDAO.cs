﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace org.hpcshelf.database
{

    [Serializable()]
public class AbstractComponentFunctorDAO{

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int insert(AbstractComponentFunctor ac, IDbConnection dbcon)
        {

            int nextKey = ac.Id_abstract;
            if (nextKey <= 0)
                nextKey = DBConnector.nextKey("id_abstract", "abstractcomponentfunctor", dbcon);

            string sql =
                "INSERT INTO abstractcomponentfunctor (id_abstract, id_functor_app_supertype, library_path, hash_component_UID, kind)" +
                " VALUES (" + nextKey + ","
                + ac.Id_functor_app_supertype + ","
                + (ac.Library_path == null ? "null" : ("'" + ac.Library_path + "'")) + ","
                + (ac.Hash_component_UID == null ? "null" : ("'" + ac.Hash_component_UID + "'")) + ",'" + ac.Kind + "')";

            Console.WriteLine("AbstractComponentFunctor.cs: TRY INSERT: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);

            if (cache_acf.ContainsKey(ac.Id_abstract))
            {
                cache_acf.Remove(ac.Id_abstract);
				cache_library_path.Remove(ac.Library_path);
				cache_acf[ac.Id_abstract] = ac;
				cache_library_path[ac.Library_path] = ac;
			}

            return nextKey;
        }

    IDictionary<int, AbstractComponentFunctor> cache_acf = new Dictionary<int,AbstractComponentFunctor>();
		IDictionary<string, AbstractComponentFunctor> cache_library_path = new Dictionary<string, AbstractComponentFunctor>();

        // public AbstractComponentFunctor retrieve(int id_abstract)
        // {
        //     return retrieve(id_abstract, DBConnector.DBcon);
        // }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AbstractComponentFunctor retrieve(int id_abstract, IDbConnection dbcon)
        {

            AbstractComponentFunctor acf = null;
            if (cache_acf.TryGetValue(id_abstract, out acf)) return acf;

            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_abstract, id_functor_app_supertype, library_path, hash_component_UID, kind " +
                 "FROM abstractcomponentfunctor " +
                 "WHERE id_abstract=" + id_abstract;
            //     Console.WriteLine("AbstractComponentFunctorDAO - retrieve: " + sql);
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                acf = new AbstractComponentFunctor();
                acf.Hash_component_UID = (string)reader["hash_component_UID"]; /* LINE ADDED BY HERON (new field in abstractcomponentfunctor) */
                acf.Id_abstract = (int)reader["id_abstract"];
                acf.Id_functor_app_supertype = (int)reader["id_functor_app_supertype"];
                acf.Library_path = (string)reader["library_path"];
                acf.Kind = (string)reader["kind"];
                cache_acf[id_abstract] = acf;
                cache_library_path[acf.Library_path] = acf;
       }//if
            else
            {
                // Abstract Component Functor NOT FOUND !
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

        //    if (acf == null)
          //      Console.Error.WriteLine("AbstractComponentFunctorDAO.cs: abstract component functor not found ! id_abstract=" + id_abstract);

            return acf;
        }

        public bool isSubtype(int id_abstract, int id_abstract_super, IDbConnection dbcon)
        {
            int id_abstract_current = id_abstract;
            while (id_abstract_current > 0 && id_abstract_current != id_abstract_super)
            {
                int id_functor_app = this.retrieve(id_abstract_current, dbcon).Id_functor_app_supertype;
                id_abstract_current = id_functor_app > 0 ? DGAC.Backend.acfadao.retrieve(id_functor_app, dbcon).Id_abstract : -1;
            }
            return id_abstract_current == id_abstract_super;
        }


        /*ADDED BY HERON */
        //     public AbstractComponentFunctor retrieveByUID(string hash_component_UID)
        //    {
        //        return retrieveByUID(hash_component_UID, DBConnector.DBcon);
        //    }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AbstractComponentFunctor retrieveByUID(string hash_component_UID, IDbConnection dbcon)
        {

            AbstractComponentFunctor acf = null;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT hash_component_UID, id_abstract, id_functor_app_supertype, library_path, kind " +
                 "FROM abstractcomponentfunctor " +
                 "WHERE hash_component_UID like '" + hash_component_UID + "'";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            //	Console.WriteLine ("RETRIEVE BY UID 3" + hash_component_UID);
            if (reader.Read())
            {
                acf = new AbstractComponentFunctor();
                acf.Hash_component_UID = (string)reader["hash_component_UID"];
                acf.Id_abstract = (int)reader["id_abstract"];
                acf.Id_functor_app_supertype = (int)reader["id_functor_app_supertype"];
                acf.Library_path = (string)reader["library_path"];
                acf.Kind = (string)reader["kind"];
                //cache_acf[acf.Id_abstract] = acf;
                //cache_library_path[acf.Library_path] = acf;
            }
            else
            {
                // NOT FOUND !
            }
            //	Console.WriteLine ("RETRIEVE BY UID 4" + hash_component_UID);
            if (reader.Read())
            {
                throw new Exception("More than one line FOUND for field hash_component_UID (AbastractComponentFunctorDAO)");
            }


            //while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return acf;
        }


    internal IList<string> getIdUnitsOrdered(int id_abstract, IDbConnection dbcon)
    {
        List<string> list = new List<string>();
        //IDbConnection dbcon = DBConnector.DBcon;
        IDbCommand dbcmd = dbcon.CreateCommand();
        string sql =
            "SELECT DISTINCT id_interface " +
            "FROM interface " +
            "WHERE id_abstract=" + id_abstract + " "+ 
            "ORDER BY `order`";
        dbcmd.CommandText = sql;
        IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
        while (reader.Read())
        {
            list.Add((string)reader["id_interface"]);
        }//while
        // clean up
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        return list;
     
    }


        //   public AbstractComponentFunctor retrieve_libraryPath(string library_path)
        //   {
        //      return retrieve_libraryPath(library_path, DBConnector.DBcon);
        //   }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AbstractComponentFunctor retrieve_libraryPath(string library_path, IDbConnection dbcon)
        {
            if (cache_library_path.ContainsKey(library_path))
                return cache_library_path[library_path];

            AbstractComponentFunctor acf = null;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT hash_component_UID, id_abstract, id_functor_app_supertype, library_path, kind " +
                    "FROM abstractcomponentfunctor " +
                 "WHERE library_path like '" + library_path + "'";
            // Console.WriteLine(sql);
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                acf = new AbstractComponentFunctor();
                acf.Hash_component_UID = (string)reader["hash_component_UID"];
                acf.Id_abstract = (int)reader["id_abstract"];
                acf.Id_functor_app_supertype = (int)reader["id_functor_app_supertype"];
                acf.Library_path = (string)reader["library_path"];
                acf.Kind = (string)reader["kind"];

                cache_acf[acf.Id_abstract] = acf;
                cache_library_path[acf.Library_path] = acf;
            }


            //while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return acf;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal IList<AbstractComponentFunctor> list(IDbConnection dbcon)
        {
            IList<AbstractComponentFunctor> acfaList = new List<AbstractComponentFunctor>();
          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_abstract, id_functor_app_supertype, library_path, hash_component_UID, kind " +
                 "FROM abstractcomponentfunctor";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                AbstractComponentFunctor acf = new AbstractComponentFunctor();
                acf.Hash_component_UID = (string)reader["hash_component_UID"]; /* LINE ADDED BY HERON (new field in abstractcomponentfunctor) */
                acf.Id_abstract = (int)reader["id_abstract"];
                acf.Id_functor_app_supertype = (int)reader["id_functor_app_supertype"];
                acf.Library_path = (string)reader["library_path"];
                acf.Kind = (string)reader["kind"];
                acfaList.Add(acf);
				if (!cache_acf.ContainsKey(acf.Id_abstract))
				{
					cache_acf[acf.Id_abstract] = acf;
					cache_library_path[acf.Library_path] = acf;
				}
			}//if

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return acfaList;
        }

        internal IList<int> retrieve_supertypes(int id_abstract, IDbConnection dbcon)
        {
            IList<int> r = new List<int>();
            int id_abstract_current = id_abstract;
            while (true)
            {
                r.Add(id_abstract_current);
                AbstractComponentFunctor acf = retrieve(id_abstract_current, dbcon);
                if (acf.Id_functor_app_supertype == 0) break;
                AbstractComponentFunctorApplication acfa = DGAC.Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
                id_abstract_current = acfa.Id_abstract;
            }

            return r;
        }

        internal IList<int> retrieve_subtypes(int id_abstract, IDbConnection dbcon)
        {
            IList<int> r = new List<int>();
            Queue<int> current = new Queue<int>();
            current.Enqueue(id_abstract);
            while (current.Count > 0)
            {
                int id_abstract_current = current.Dequeue();
                r.Add(id_abstract_current);
                foreach (int i in DGAC.Backend.acfdao.retrieve_direct_subtypes(id_abstract_current, dbcon))
                    current.Enqueue(i);
            }
            return r;
        }

        private IList<int> retrieve_direct_subtypes(int id_abstract, IDbConnection dbcon)
        {
			IList<int> acfList = new List<int>();
			//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = "SELECT a.id_abstract as x FROM abstractcomponentfunctor as a, abstractcomponentfunctorapplication as b, abstractcomponentfunctor as c " +
                         "WHERE c.id_abstract = " + id_abstract + " AND " +
	                         "a.id_functor_app_supertype = b.id_functor_app AND " +
	                         "b.id_abstract = c.id_abstract";

            dbcmd.CommandText = sql;

			//  Console.WriteLine ("listByKind ****** : " + sql);

			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read()) acfList.Add((int)reader["x"]);

			// clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return acfList;

		}

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<AbstractComponentFunctor> listByKind(string kind, IDbConnection dbcon)
        {
            IList<AbstractComponentFunctor> acfList = new List<AbstractComponentFunctor>();
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_functor_app_supertype, library_path, hash_component_UID, kind " +
                "FROM abstractcomponentfunctor " +
                "WHERE kind like '" + kind + "'";
            dbcmd.CommandText = sql;

            //	Console.WriteLine ("listByKind ****** : " + sql);

            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                AbstractComponentFunctor acf = new AbstractComponentFunctor();
                acf.Hash_component_UID = (string)reader["hash_component_UID"]; /* LINE ADDED BY HERON (new field in abstractcomponentfunctor) */
                acf.Id_abstract = (int)reader["id_abstract"];
                acf.Id_functor_app_supertype = (int)reader["id_functor_app_supertype"];
                acf.Library_path = (string)reader["library_path"];
                acf.Kind = (string)reader["kind"];
                acfList.Add(acf);
                if (!cache_acf.ContainsKey(acf.Id_abstract))
                {
                    cache_acf[acf.Id_abstract] = acf;
                    cache_library_path[acf.Library_path] = acf;
                }
            }//if

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return acfList;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal void updateSuperType(int id_abstract, int id_functor_app, IDbConnection dbcon)
        {
            String sql = "UPDATE abstractcomponentfunctor SET id_functor_app_supertype = " + id_functor_app + " WHERE id_abstract = " + id_abstract;

			Console.WriteLine("AbstractComponentFunctor.cs: TRY DELETE: " + sql);

			DBConnector.performSQLUpdate(sql, dbcon);

            if (cache_acf.ContainsKey(id_abstract))
            {
                cache_acf[id_abstract].Id_functor_app_supertype = id_functor_app;
            }
		}
    }//class

}//namespace