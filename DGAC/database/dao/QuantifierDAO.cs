﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class QuantifierDAO
    {


        public void insert(Quantifier ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO quantifier (id_abstract, value, id_abstract_quantifier)" +
                " VALUES (" + ac.Id_abstract + "," + ac.QuantifierValue.ToString(CultureInfo.InvariantCulture) + "," + ac.Id_abstract_quantifier + ")";

            Console.WriteLine("Unit.cs: TRY INSERT 1: " + sql);

            invalidate_cache_1[new Tuple<int, decimal>(ac.Id_abstract, ac.QuantifierValue)] = true;

            DBConnector.performSQLUpdate(sql, dbcon);
        }

		public IList<Quantifier> list(int id_abstract, IDbConnection dbcon)
        {
            return list(id_abstract, "ASC", dbcon);
        }
		
        public IList<Quantifier> list(int id_abstract, string order, IDbConnection dbcon)
        {
            IList<Quantifier> q_list = new List<Quantifier>();
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, value, id_abstract_quantifier FROM quantifier " +
                "WHERE id_abstract=" + id_abstract + " " +
                "ORDER BY value " + order;
            dbcmd.CommandText = sql;
            Console.WriteLine(sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Quantifier q = new Quantifier();
                q.Id_abstract = (int)reader["id_abstract"];
                q.QuantifierValue = (decimal)reader["value"];
				q.Id_abstract_quantifier = (int)reader["id_abstract_quantifier"];
				q_list.Add(q);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return q_list;

        }//list

        private IDictionary<Tuple<int, decimal>, Quantifier> cache_1 = new Dictionary<Tuple<int, decimal>, Quantifier>();
        private IDictionary<Tuple<int, decimal>, bool> invalidate_cache_1 = new Dictionary<Tuple<int, decimal>, bool>();


        //(); ADDED BY HERON
        public Quantifier retrieve(int id_abstract, decimal quantifier_value, IDbConnection dbcon)
        {
            Tuple<int, decimal> key = new Tuple<int, decimal>(id_abstract, quantifier_value);
            if (cache_1.ContainsKey(key) && !invalidate_cache_1.ContainsKey(key))
                return cache_1[key];

            Quantifier q = null;
          // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, value, id_abstract_quantifier " +
                "FROM quantifier " +
				"WHERE id_abstract=" + id_abstract + " AND value = " + quantifier_value.ToString(CultureInfo.InvariantCulture);

            dbcmd.CommandText = sql;
            Console.WriteLine(sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                q = new Quantifier();
				q.Id_abstract = (int)reader["id_abstract"];
                q.QuantifierValue = (decimal)reader["value"];
                q.Id_abstract_quantifier = (int)reader["id_abstract_quantifier"];
				if (invalidate_cache_1.ContainsKey(key))
                {
                    invalidate_cache_1.Remove(key);
                    cache_1.Remove(key);
                }
                cache_1[key] = q;
            }
            else
            {
                // UNIT NOT FOUND
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return q;

        }//list


    }//class

}//namespace
