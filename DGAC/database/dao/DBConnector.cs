﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using org.hpcshelf.DGAC.utils;


namespace org.hpcshelf.database
{

    [Serializable]
    public class DBConnector
    {


        public static IDbConnection createConnection()
        {
            string connectionString = Constants.connectionString;
            // Console.WriteLine(connectionString); 
            IDbConnection dbcon;
            dbcon = new MySqlConnection(connectionString);
            return dbcon;
        }

      //  private static IDbConnection dbcon = null;
       // private static IDbTransaction dbtrans = null;

      //  public static IDbConnection DBcon
      //  {
      //      get { return dbcon; }
      //  }

       //public static void beginTransaction(IDbConnection dbcon)
       // {
            //Console.Error.WriteLine("begin transaction");
       //     dbtrans = dbcon.BeginTransaction();
      //  }

      //  public static void commitTransaction()
     //  {
            //Console.Error.WriteLine("commit transaction");
     //       dbtrans.Commit();
    //   }

    //    public static void rollBackTransaction()
    //    {
    //        //Console.Error.WriteLine("rollback transaction");
    //       dbtrans.Rollback();
    //    }

   //     public static void endTransaction()
    //    {
    //        //  Console.Error.WriteLine("end transaction");
    //        dbtrans = null;
     //   }

        private static int r = 0;
        internal static readonly object dblock = new object();


       /* public static void openConnection()
        {
            lock (dblock)
            {
                // Console.WriteLine("ENTER openConnection --- r={0} {1}", r, dbcon == null);
                if (r == 0)
                {
                    dbcon = DBConnector.getConnection();
                    dbcon.Open();
                }
                r++;
                // Console.WriteLine("EXIT openConnection --- r={0} {1}", r, dbcon == null);
            }
        }

        public static void closeConnection()
        {
            lock (dblock)
            {
                // Console.WriteLine("ENTER closeConnection --- r={0} {1}", r, dbcon == null, dbcon.State != ConnectionState.Closed);
                r--;
                if (r == 0 && dbcon != null && dbcon.State != ConnectionState.Closed)
                {
                    dbcon.Close();
                    dbcon = null;
                }
                // Console.WriteLine("EXIT closeConnection --- r={0} {1}", r, dbcon == null);
            }
        }

        */
        public static int nextKey(String keyField, String tableName, IDbConnection dbcon)
        {
            int lastKey = 0;

            // IDbConnection dbcon = Connector.getConnection();
            // dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();

            string sql =
                 "SELECT MAX(" + keyField + ") as lastKey " +
                 "FROM " + tableName;

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read() && !(reader["lastKey"] is System.DBNull))
            {
                lastKey = (int)reader["lastKey"];
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            // dbcon.Close();
            // dbcon = null;

            return lastKey + 1;
        }

        public static void performSQLUpdate(String sql, MySqlParameter[] parameters, IDbConnection dbcon)
        {
            IDbCommand dbcmd = dbcon.CreateCommand();

            foreach (MySqlParameter parameter in parameters)
                dbcmd.Parameters.Add(parameter);

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();

            dbcmd.Dispose();
            dbcmd = null;

            /*   using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = (MySqlConnection)dbcon;
                    command.CommandText = "INSERT INTO file (file_name, file_size, file) VALUES (?fileName, ?fileSize, ?rawData);";

                    foreach (MySqlParameter parameter in parameters)
                        command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                }*/
        }

        /* HERON */
        public static void performSQLUpdate(String sql, IDbConnection dbcon)
        {

            //IDbConnection dbcon = Connector.getConnection();
            //dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();

            dbcmd.Dispose();
            dbcmd = null;
            //dbcon.Close();
            //dbcon = null;

        }

    }//class

}//namespace
