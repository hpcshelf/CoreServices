﻿using System;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class SupplyParameterDAO
    {

        public void insert(SupplyParameter ac, IDbConnection dbcon)
        {
            //	Console.WriteLine("SupplyParameter.cs: TRY INSERT 1: " + (ac==null));

            String sql1 =
                "INSERT INTO supplyparameter (id_parameter, id_functor_app, id_abstract)" +
                "VALUES ('" + ac.Id_parameter + "'," + ac.Id_functor_app + "," + ac.Id_abstract + ")";

            String sql2 = null;


            if (ac is SupplyParameterComponent)
            {
                //	Console.WriteLine("SupplyParameter.cs: TRY INSERT 2: ");
                SupplyParameterComponent acc = (SupplyParameterComponent)ac;
                sql2 =
                "INSERT INTO supplyparametercomponent (id_parameter, id_functor_app, id_functor_app_actual)" +
                " VALUES ('" + acc.Id_parameter + "'," + acc.Id_functor_app + "," + acc.Id_functor_app_actual + ")";
            }
            else if (ac is SupplyParameterParameter)
            {
//	Console.WriteLine("SupplyParameter.cs: TRY INSERT 3: ");
                SupplyParameterParameter acp = (SupplyParameterParameter)ac;
                sql2 =
                "INSERT INTO supplyparameterparameter (id_parameter, id_functor_app, id_parameter_actual, freeVariable)" +
                " VALUES ('" + acp.Id_parameter + "'," + acp.Id_functor_app + ",'" + acp.Id_argument + "'," + (acp.FreeVariable ? 1 : 0) + ")";
            }

            //	Console.WriteLine("SupplyParameter.cs: TRY INSERT 4: " + sql1);
            //	Console.WriteLine("SupplyParameter.cs: TRY INSERT 5: " + sql2);

            DBConnector.performSQLUpdate(sql1, dbcon);
            DBConnector.performSQLUpdate(sql2, dbcon);
        }

        IDictionary<Tuple<int,string>, SupplyParameter> cache_2 = new Dictionary<Tuple<int, string>, SupplyParameter>();

		private Tuple<int, string> makeKey(int id_functor_app, string id_parameter)
        {
            return new Tuple<int, string>(id_functor_app, id_parameter);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SupplyParameter retrieve(string id_parameter, int id_functor_app, IDbConnection dbcon)
        {

            SupplyParameter spc = null;

            Tuple<int, string> key = makeKey(id_functor_app, id_parameter);

            if (cache_2.TryGetValue(key, out spc)) 
                return spc;

            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            bool achou = false;
            string sql =
                "SELECT a.id_parameter as id_parameter, a.id_functor_app as id_functor_app, b.id_functor_app_actual as id_functor_app_actual, a.id_abstract as id_abstract" +
                " FROM supplyparameter as a, supplyparametercomponent as b" +
                " WHERE a.id_functor_app = b.id_functor_app AND a.id_parameter = b.id_parameter AND a.id_parameter like '" + id_parameter + "' AND a.id_functor_app=" + id_functor_app;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                achou = true;
                SupplyParameterComponent spc_ = new SupplyParameterComponent();
                spc_.Id_parameter = (string)reader["id_parameter"];
                spc_.Id_functor_app = (int)reader["id_functor_app"];
                spc_.Id_functor_app_actual = (int)reader["id_functor_app_actual"];
                spc_.Id_abstract = (int)reader["id_abstract"];
                spc = spc_;

                if (cache_2.ContainsKey(key))
                {
                    cache_2.Remove(key);
                }

                cache_2[key] =spc_;
            }


            if (!achou)
            {

                reader.Close();
                dbcmd.Dispose();

                sql =
                    "SELECT a.id_parameter as id_parameter, a.id_functor_app as id_functor_app, b.id_parameter_actual as id_parameter_actual, b.freeVariable as freeVariable, a.id_abstract as id_abstract" +
                    " FROM supplyparameter as a, supplyparameterparameter as b" +
                    " WHERE a.id_functor_app = b.id_functor_app AND a.id_parameter = b.id_parameter AND a.id_parameter like '" + id_parameter + "' AND a.id_functor_app=" + id_functor_app;
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                if (reader.Read())
                {
                    achou = true;
                    SupplyParameterParameter spc_ = new SupplyParameterParameter();
                    spc_.Id_parameter = (string)reader["id_parameter"];
                    spc_.Id_functor_app = (int)reader["id_functor_app"];
                    spc_.Id_argument = (string)reader["id_parameter_actual"];
                    spc_.FreeVariable = ((int)reader["freeVariable"]) == 0 ? false : true;
                    spc_.Id_abstract = (int)reader["id_abstract"];
					spc = spc_;

					if (cache_2.ContainsKey(key))
					{
						cache_2.Remove(key);
					}

					cache_2[key] = spc_;
				}

            }

            reader.Close();
            dbcmd.Dispose();

           /* sql =
                "SELECT id_parameter, id_functor_app, id_abstract" +
                " FROM supplyparameter" +
                " WHERE id_parameter like '" + id_parameter + "' AND id_functor_app=" + id_functor_app;
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            if (reader.Read())
            {
                spc.Id_abstract = (int)reader["id_abstract"];
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;*/


            //  if (spc==null) 
            //  {
            // 	  Console.WriteLine("SupplyParameterDAO.cs: PARAMETER NOT FOUND "+ id_parameter + "," + id_functor_app);
            // }

            return spc;

        }//list

        IDictionary<int, IDictionary<string,SupplyParameter>> cache_1 = new Dictionary<int, IDictionary<string,SupplyParameter>>();


        public IList<SupplyParameter> list_slow(int id_functor_app, IDbConnection dbcon)
        {

            //	Console.WriteLine ("SupplyParameterDAO: list ENTER " + id_functor_app);

            IList<SupplyParameter> list = null;
            list = new List<SupplyParameter>();

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            IList<string> parameters = new List<string>();

            string sql =
                "SELECT id_parameter, id_functor_app, id_abstract" +
                " FROM supplyparameter" +
                " WHERE id_functor_app=" + id_functor_app;
            dbcmd.CommandText = sql;
            //	Console.WriteLine ("SupplyParameterDAO: SQL = " + sql);

            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                string id_parameter = (string)reader["id_parameter"]; ;
                // Console.WriteLine ("SupplyParameterDAO: reading parameter " + id_parameter);
                parameters.Add(id_parameter);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            foreach (string id_parameter in parameters)
            {
                //   Console.WriteLine ("SupplyParameterDAO: fetching parameter " + id_parameter + " / " + id_functor_app);
                SupplyParameter sp = retrieve(id_parameter, id_functor_app, dbcon);
                list.Add(sp);
            }

            //	Console.WriteLine ("SupplyParameterDAO: list EXIT count=" + list.Count);

            return list;

        }//list

      //  public ICollection<SupplyParameter> list(int id_functor_app)
      // {
     //       return list(id_functor_app, DBConnector.DBcon);
      //  }

        public ICollection<SupplyParameter> list(int id_functor_app, IDbConnection dbcon)
        {
            return listBy(id_functor_app, dbcon).Values;
        }

        //   public IDictionary<string,SupplyParameter> listBy(int id_functor_app)
        //   {
        //       return listBy(id_functor_app, DBConnector.DBcon);
        //    }

        private readonly object lock_listBy = new object();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IDictionary<string,SupplyParameter> listBy(int id_functor_app, IDbConnection dbcon)
		{
            IDictionary<string, SupplyParameter> list = null;

			if (cache_1.TryGetValue(id_functor_app, out list))
				return list;
            
			list = new Dictionary<string, SupplyParameter>();

			cache_1[id_functor_app] = list;

			IDataReader reader;
			IDbCommand dbcmd = dbcon.CreateCommand();
			//IList<string> parameters = new List<string>();

            {
                string sql =
                    "SELECT a.id_parameter as id_parameter, a.id_functor_app as id_functor_app, b.id_functor_app_actual as id_functor_app_actual, a.id_abstract as id_abstract" +
                    " FROM supplyparameter as a, supplyparametercomponent as b" +
                    " WHERE a.id_functor_app = b.id_functor_app AND a.id_parameter = b.id_parameter AND a.id_functor_app=" + id_functor_app;
                dbcmd.CommandText = sql;
                //  Console.WriteLine ("SupplyParameterDAO: SQL = " + sql);

                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
					SupplyParameterComponent spc = new SupplyParameterComponent();
					spc.Id_parameter = (string)reader["id_parameter"];
					spc.Id_functor_app = (int)reader["id_functor_app"];
					spc.Id_functor_app_actual = (int)reader["id_functor_app_actual"];
					spc.Id_abstract = (int)reader["id_abstract"];
					list[spc.Id_parameter] = spc;

					Tuple<int, string> key = makeKey(id_functor_app, spc.Id_parameter);

					cache_2[key] = spc;
				}//while
            }

			reader.Close();

			{
				string sql =
					"SELECT a.id_parameter as id_parameter, a.id_functor_app as id_functor_app, b.id_parameter_actual as id_parameter_actual, b.freeVariable as freeVariable, a.id_abstract as id_abstract" +
                    " FROM supplyparameter as a, supplyparameterparameter as b" +
					" WHERE a.id_functor_app=b.id_functor_app AND a.id_parameter=b.id_parameter AND a.id_functor_app=" + id_functor_app;
				dbcmd.CommandText = sql;

				reader = dbcmd.ExecuteReader();
				while (reader.Read())
				{
					SupplyParameterParameter spc = new SupplyParameterParameter();
					spc.Id_parameter = (string)reader["id_parameter"];
					spc.Id_functor_app = (int)reader["id_functor_app"];
					spc.Id_argument = (string)reader["id_parameter_actual"];
                    spc.Id_abstract = (int)reader["id_abstract"];
					spc.FreeVariable = ((int)reader["freeVariable"]) == 0 ? false : true;

					list[spc.Id_parameter] = spc;

					Tuple<int, string> key = makeKey(id_functor_app, spc.Id_parameter);
                    cache_2[key] = spc;
				}//while

			}
			
            // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;

			return list;

		}//list
		



    }//class

}//namespace