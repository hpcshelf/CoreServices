﻿using System;
using System.IO;
using System.Runtime.Serialization;


namespace org.hpcshelf.database
{

    [Serializable()]
    public class SupplyParameterParameter : SupplyParameter
    {
        private string id_argument;

        private bool freeVariable;

        public bool FreeVariable
        {
            get { return freeVariable; }
            set { freeVariable = value; }
        }


        public string Id_argument
        {
            get { return id_argument; }
            set { id_argument = value; }
        }

        public override SupplyParameter getCopy(int id_functor_app, int id_abstract)
        {
            SupplyParameterParameter sp = new SupplyParameterParameter();
            sp.Id_functor_app = id_functor_app;
            sp.Id_parameter = this.Id_parameter;
            sp.Id_abstract = id_abstract;
            sp.FreeVariable = this.FreeVariable;
            sp.Id_argument = this.Id_argument;
            return sp;
        }

    }//class

}//namespace