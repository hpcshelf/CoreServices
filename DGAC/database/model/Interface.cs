﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Collections.Concurrent;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class Interface
    {

        private string id_interface;
        private string id_interface_super;
        private string id_interface_super_top;

        private int facet;

        public int Facet
        {
            get
            {
                return facet;
            }
            set
            {
                this.facet = value;
            }
        }

        private bool is_parallel;


        public bool Is_parallel
        {
            get
            {
                return is_parallel;
            }
            set
            {
                this.is_parallel = value;
            }
        }

        private int order;

        public int Order
        {
            get { return order; }
            set { order = value; }
        }

        private string[] actions;

        public string[] Actions
        {
            get { return Actions; }
            set { actions = value; }
        }

        private string[] conditions;

        public string[] Conditions
        {
            get { return Conditions; }
            set { conditions = value; }
        }

        private string protocol;

        public string Protocol
        {
            get { return Protocol; }
            set { protocol = value; }
        }

        public string GetId_interface_super_top(IDbConnection dbcon)
        {
            //	Console.WriteLine("Id_interface_super_top ... BEGIN");
            if (id_interface_super_top == null)
            {
                if (id_interface_super.Equals(""))
                {
                    //		Console.WriteLine ("Id_interface_super_top 1: " + id_interface);
                    id_interface_super_top = id_interface;
                }
                else
                {
                    //	Console.WriteLine ("Id_interface_super_top 2.1: " + id_interface_super + " / " + id_abstract);
                    string[] id_interface_super_list_ = Interface.splitIDs(Id_interface_super);
                    AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve(id_abstract, dbcon);
                    //	Console.WriteLine ("Id_interface_super_top 2.2: " + acf.Id_functor_app_supertype);
                    if (acf.Id_functor_app_supertype != 0)
                    {
                        AbstractComponentFunctorApplication acfaSuper = org.hpcshelf.DGAC.Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
                        if (id_interface_super_list_.Length == 0)
                            id_interface_super_top = "";
                        else
                        {
                            string id_interface_super_1 = id_interface_super_list_[0];
                            Interface iSuper1 = org.hpcshelf.DGAC.Backend.idao.retrieve(acfaSuper.Id_abstract, id_interface_super_1, dbcon);
                            id_interface_super_top = iSuper1.GetId_interface_super_top(dbcon);
                            for (int j = 1; j < id_interface_super_list_.Length; j++)
                            {
                                string id_interface_super_2 = id_interface_super_list_[j];
                                //		Console.WriteLine ("Id_interface_super_top 3.1: " + id_interface_super_2);
                                Interface iSuper2 = org.hpcshelf.DGAC.Backend.idao.retrieve(acfaSuper.Id_abstract, id_interface_super_2, dbcon);
                                id_interface_super_top += "+" + iSuper2.GetId_interface_super_top(dbcon);
                                //		Console.WriteLine ("Id_interface_super_top 3.2: " + id_interface_super_top);
                            }
                        }
                    }
                    else
                    {
                        id_interface_super_top = id_interface;
                    }
                }
                org.hpcshelf.DGAC.Backend.idao.setInterfaceSuperTop(id_abstract, id_interface, id_interface_super_top, dbcon);
                //	Console.WriteLine("Id_interface_super_top ... END ");
                return id_interface_super_top;
            }
            else
            {
                return id_interface_super_top;
            }
        }

        public void SetId_interface_super_top(string value)
        {
            id_interface_super_top = value;
        }

        private int id_abstract;
        private string assembly_string;

        private string class_name; // Nome da interface no Front-End.

        public string Class_name
        {
            get { return class_name; }
            set { class_name = value; }
        }

        private int class_nargs;

        public int Class_nargs
        {
            get { return class_nargs; }
            set { class_nargs = value; }
        }

        string uri_source;
        public string URI_Source
        {
            get { return uri_source; }
            set { uri_source = value; }
        }


        public string Id_interface
        {
            get { return id_interface; }
            set { id_interface = value; }
        }

        public string Id_interface_super
        {
            get { return id_interface_super; }
            set { id_interface_super = value; }
        }

        public int Id_abstract
        {
            get { return id_abstract; }
            set { id_abstract = value; }
        }


        public string Assembly_string
        {
            get { return assembly_string; }
            set { assembly_string = value; }
        }



        private IList<string> references = null;

        public IList<string> GetReferences(IDbConnection dbcon)
        {
            if (this.references == null)
            {
                AbstractComponentFunctor acf = Backend.acfdao.retrieve(Id_abstract, dbcon);
                this.references = fetchReferences(acf.getParameterBounds(dbcon), dbcon);
            }
            return this.references;
        }


        private IList<string> id_interface_list;

        public IList<string> GetId_interface_list(IDbConnection dbcon)
        {
            if (id_interface_list != null)
                return id_interface_list;

            IList<string> l = new List<string>();

            Interface i = this;
            l.Add(i.Id_interface);
            AbstractComponentFunctor acf = Backend.acfdao.retrieve(Id_abstract, dbcon);
            while (acf.Id_functor_app_supertype > 0)
            {
                AbstractComponentFunctorApplication acfa = Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
                string[] ids = i.Id_interface_super.Split('+');
                foreach (string id in ids)
                {
                    i = Backend.idao.retrieve(acfa.Id_abstract, id, dbcon);
                    l.Add(i.Id_interface);
                }
                acf = Backend.acfdao.retrieve(acfa.Id_abstract, dbcon);
            }

            this.id_interface_list = l;
            return l;
        }


        public IList<string> fetchReferences(IDictionary<string, AbstractComponentFunctorApplication> pars, IDbConnection dbcon)
        {
          //  Console.WriteLine("ENTER fetchReferences - id_abstract=" + this.Id_abstract + ", id_interface=" + this.Id_interface);

          //  foreach (KeyValuePair<string, AbstractComponentFunctorApplication> y in pars)
          //  {
          //      if (y.Key != null && y.Value != null)
          //          Console.WriteLine("key=" + y.Key + ", value=" + y.Value.Id_abstract);
          //      else
          //      {
          //          Console.WriteLine("somthing strange : " + (y.Key == null ? "null" : y.Key.ToString()) + " , " + (y.Value == null ? "null" : y.Value.ToString()));
          //      }
          //  }

            IList<string> refs = new List<string>();

            AbstractComponentFunctor cThis = org.hpcshelf.DGAC.Backend.acfdao.retrieve(this.Id_abstract, dbcon);
            AbstractComponentFunctorApplication cSuperApp = org.hpcshelf.DGAC.Backend.acfadao.retrieve(cThis.Id_functor_app_supertype, dbcon);
            if (cSuperApp != null)
            {
                AbstractComponentFunctor acfsuper = org.hpcshelf.DGAC.Backend.acfdao.retrieve(cSuperApp.Id_abstract, dbcon);
                IDictionary<string, AbstractComponentFunctorApplication> parsSuper = null;
                collectParameters(pars, cSuperApp, out parsSuper, dbcon);

                string[] id_interface_super_list = Interface.splitIDs(this.Id_interface_super);
           //     Console.WriteLine("fetchReferences: id_interface_super_list.Length = " + id_interface_super_list.Length + ", this.Id_Interface_super=" + this.Id_interface_super);
           //     foreach (string sss in id_interface_super_list)
            //    {
            //        Console.WriteLine("fetchReference - SUPER: " + sss);
            //    }

                foreach (string id_interface_super in id_interface_super_list)
                {
                    Interface iSuper = org.hpcshelf.DGAC.Backend.idao.retrieve(cSuperApp.Id_abstract, id_interface_super, dbcon);
                    foreach (string iref in iSuper.fetchReferences(parsSuper, dbcon))
                        refs.Add(iref);
                    string refname = LoaderApp.buildDllName(acfsuper.Library_path, iSuper.Assembly_string);
                    if (!refs.Contains(refname))
                        refs.Add(refname);
                }

            }

            // Traverse slices.
            IList<Slice> slices = org.hpcshelf.DGAC.Backend.sdao.listByInterface(Id_abstract, Id_interface, dbcon);

            foreach (Slice s in slices)
            {
            //    Console.WriteLine("SLICE (fetchReference): " + Id_abstract + ":" + Id_interface + ":" + s.Id_inner + " - " + s.PortName);

                InnerComponent ic = org.hpcshelf.DGAC.Backend.icdao.retrieve(Id_abstract, s.Id_inner, dbcon);
                if (ic != null)
                {
                    InnerComponent ic2 = org.hpcshelf.DGAC.Backend.icdao.retrieve(s.Id_abstract, s.Id_inner, dbcon);
                    AbstractComponentFunctorApplication acfa = null;
                    IDictionary<string, AbstractComponentFunctorApplication> parsSlice = null;

                    // ---------------------------------------------------------------------------------------
                    if (!ic.Parameter_top.Equals(""))
                    {
                        if (!pars.TryGetValue(ic.Parameter_top, out acfa))
                        {
                            acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(ic.Id_functor_app, dbcon);
                   //         Console.WriteLine("fetchReferences - TRACE 1.1 - ic.Parameter_top=" + ic.Parameter_top + ", acfa.Id_abstract=" + acfa.Id_abstract);
                        }
                     //   else
                     //   {
                     //       Console.WriteLine("fetchReferences - TRACE 1.2 - ic.Parameter_top=" + ic.Parameter_top + ", acfa.Id_abstract=" + acfa.Id_abstract + ", ic.id_inner=" + ic.Id_inner);
                     //   }
                    }
                    else
                    {
                        acfa = Backend.acfadao.retrieve(ic.Id_functor_app, dbcon);
                     //   Console.WriteLine("fetchReferences - TRACE 2 - acfa.Id_abstract=" + acfa.Id_abstract);
                    }


                    // if (ic.Parameter_top.Equals("node"))
                    // {
                    //     Console.Write(true);
                    // }

                    collectParameters(pars, acfa, out parsSlice, dbcon);

                  //  Console.WriteLine("RETRIEVE BY MATCHING: " + acfa.Id_abstract + "," + ic.Id_abstract_inner + "," + s.Id_interface_slice);
                    string id_interface_slice = s.Id_interface_slice; // br.ufc.pargo.hpe.backend.DGAC.BackEnd.idao.fetchIdInterfaceSlice(ic.Id_abstract_inner, ic2.Id_abstract_inner, s.Id_interface_slice);

                    IList<Interface> i_list = org.hpcshelf.DGAC.Backend.idao.retrieveByMatching(acfa.Id_abstract, ic2.Id_abstract_inner, id_interface_slice, dbcon);

                    // if (i_list == null || i_list.Length == 0)
                    // {
                    //     Console.WriteLine("i is null : " + acfa.Id_abstract + "," + ic.Id_abstract_inner + "," + s.Id_interface_slice);
                    // }
                    AbstractComponentFunctor acfSlice = org.hpcshelf.DGAC.Backend.acfdao.retrieve(acfa.Id_abstract, dbcon);

                    foreach (Interface i in i_list)
                    {
                        IList<string> refsSlice = i.fetchReferences(parsSlice, dbcon);

                        foreach (string r in refsSlice)
                            if (!refs.Contains(r))
                                refs.Add(r);

                        string refname = LoaderApp.buildDllName(acfSlice.Library_path, i.Assembly_string);
                        if (!refs.Contains(refname))
                            refs.Add(refname);
                    }
                }
            }

          //  Console.WriteLine("EXIT fetchReferences - id_abstract=" + this.Id_abstract + ", id_interface=" + this.Id_interface);
            return refs;
        }


        private void collectParameters(IDictionary<string, AbstractComponentFunctorApplication> pars,
                                       AbstractComponentFunctorApplication acfa,
                                       out IDictionary<string, AbstractComponentFunctorApplication> parsSlice, IDbConnection dbcon)
        {
            ICollection<SupplyParameter> spList = Backend.spdao.list(acfa.Id_functor_app, dbcon);

            parsSlice = new ConcurrentDictionary<string, AbstractComponentFunctorApplication>();

            foreach (SupplyParameter sp in spList)
            {
                AbstractComponentFunctorApplication acfaPar = null;
                if (sp is SupplyParameterComponent)
                {
                    SupplyParameterComponent sp_ = (SupplyParameterComponent)sp;
                    acfaPar = org.hpcshelf.DGAC.Backend.acfadao.retrieve(sp_.Id_functor_app_actual, dbcon);
                    parsSlice.Add(sp_.Id_parameter, acfaPar);
                //    Console.WriteLine("collectParameter -TRACE 1 " + sp_.Id_parameter + ":" + acfaPar.Id_abstract);
                }
                else if (sp is SupplyParameterParameter)
                {
                    SupplyParameterParameter sp_ = (SupplyParameterParameter)sp;
                    acfaPar = null;
                 //   Console.WriteLine("collectParameter -TRACE 2 " + "#" + sp_.Id_functor_app + sp_.Id_parameter);

                    pars.TryGetValue("#" + sp_.Id_functor_app + sp_.Id_parameter, out acfaPar);
                    if (acfaPar == null)
                    {
                   //     Console.WriteLine("collectParameter -TRACE 3 " + sp_.Id_argument);

                        //if (sp_.Id_argument.Equals("node-accelerator-architecture"))
                        //    Console.WriteLine();

                        if (pars.TryGetValue(sp_.Id_argument, out acfaPar))
                        {
                            parsSlice.Add(sp_.Id_parameter, acfaPar);
                     //       Console.WriteLine("collectParameter -TRACE 4 " + sp_.Id_parameter + " : " + acfaPar.Id_abstract);
                        }
                        else
                        {
                            AbstractComponentFunctorParameter acfp = org.hpcshelf.DGAC.Backend.acfpdao.retrieve(Id_abstract, sp_.Id_argument, dbcon);
                            if (acfp == null)
                                acfp = org.hpcshelf.DGAC.Backend.acfpdao.retrieve(acfa.Id_abstract, sp_.Id_parameter, dbcon);
                            acfaPar = org.hpcshelf.DGAC.Backend.acfadao.retrieve(acfp.Bounds_of, dbcon);
                            parsSlice.Add(sp_.Id_parameter, acfaPar);
                       //     Console.WriteLine("collectParameter -TRACE 5 " + sp_.Id_parameter + " : " + acfaPar.Id_abstract);
                        }
                    }
                    else
                    {
                    //    Console.WriteLine("collectParameter -TRACE 6 " + "#" + sp_.Id_functor_app + sp_.Id_parameter + " : " + acfaPar.Id_abstract);
                        parsSlice.Add(sp_.Id_parameter, acfaPar);
                    }


                    /* else
                    {
                        throw new Exception("ERROR !!!! collectParameters (Interface.cs)");
                    }*/
                }

                collectFV(acfaPar, pars, parsSlice, dbcon);
            }



        }

        private void collectFV(AbstractComponentFunctorApplication acfa,
                               IDictionary<string, AbstractComponentFunctorApplication> pars,
                               IDictionary<string, AbstractComponentFunctorApplication> apars, IDbConnection dbcon)
        {

            //SupplyParameterDAO spdao = new SupplyParameterDAO();
            ICollection<SupplyParameter> sps = org.hpcshelf.DGAC.Backend.spdao.list(acfa.Id_functor_app, dbcon);
            foreach (SupplyParameter sp in sps)
            {
                if (sp is SupplyParameterComponent)
                {
                    SupplyParameterComponent sp_ = (SupplyParameterComponent)sp;
                    // AbstractComponentFunctorApplicationDAO acfadao = new AbstractComponentFunctorApplicationDAO();
                    AbstractComponentFunctorApplication acfaPar = org.hpcshelf.DGAC.Backend.acfadao.retrieve(sp_.Id_functor_app_actual, dbcon);
                    collectFV(acfaPar, pars, apars, dbcon);
                }
                else if (sp is SupplyParameterParameter)
                {
                    SupplyParameterParameter sp_ = (SupplyParameterParameter)sp;
                    AbstractComponentFunctorApplication acfaPar = null;
                    pars.TryGetValue("#" + sp_.Id_functor_app + sp_.Id_parameter, out acfaPar);
                    if (acfaPar == null)
                    {
                        pars.TryGetValue(sp_.Id_argument, out acfaPar);
                    }
                    //Console.WriteLine("collectFV : id_functor_app=" + acfa.Id_functor_app + ", id_abstract=" + acfa.Id_abstract + ", " + "#" + sp_.Id_functor_app + sp_.Id_parameter + ", " + ", " + sp_.Id_argument + (acfaPar == null));

                    string key = "#" + sp_.Id_functor_app + sp_.Id_parameter;
                    if (!apars.ContainsKey(key))
                        apars[key] = acfaPar;
                    //else
                    //    Console.WriteLine();
                }
            }
        }

        public static string[] splitIDs(string id_interface_composed)
        {
            return id_interface_composed.Split(new char[1] { '+' });
        }


    }//class

}//namespace
