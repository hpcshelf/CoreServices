﻿using System;
using System.IO;
using System.Runtime.Serialization;


namespace org.hpcshelf.database
{

    [Serializable()]
    public class SupplyParameterComponent : SupplyParameter
    {

        private int id_functor_app_actual;

        public int Id_functor_app_actual
        {
            get { return id_functor_app_actual; }
            set { id_functor_app_actual = value; }
        }

        public override SupplyParameter getCopy(int id_functor_app, int id_abstract)
        { 
           SupplyParameterComponent sp = new SupplyParameterComponent();
            sp.Id_functor_app = id_functor_app;
            sp.Id_parameter = this.Id_parameter;
            sp.Id_abstract = id_abstract;
            sp.Id_functor_app_actual = this.id_functor_app_actual;
            return sp;
        }

    }//class

}//namespace