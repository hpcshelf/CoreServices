﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Collections;

namespace org.hpcshelf.database
{
    [Serializable()]
    public class ComponentKey
    {

        private int id;
        private char id_type;
        private byte[] snk_contents;
        private byte[] pub_contents;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        public char IDType
        {
            get { return id_type; }
            set { id_type = value; }
        }


        public byte[] SNKContents
        {
            get { return snk_contents; }
            set { snk_contents = value; }
        }

        public byte[] PUBContents
        {
            get { return pub_contents; }
            set { pub_contents = value; }
        }

    }//class

}//namespace
