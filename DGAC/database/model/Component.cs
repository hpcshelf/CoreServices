﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.Collections.Concurrent;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class Component : HashComponent
    {
        private string library_path;
        private int id_concrete;
        private int id_concrete_supertype;
        private int id_functor_app;
        private int id_abstract;
        private string argument_calculator;
		private object argument_calculator_object;


		public string ContextArgumentCalculator
		{
            get { return argument_calculator == null || argument_calculator.Equals("") ? null : argument_calculator; }
            set { argument_calculator = value; }
		}

		public object ContextArgumentCalculatorObject
		{
			get { return argument_calculator_object; }
			set { argument_calculator_object = value; }
		}

		public string Library_path
        {
            get { return library_path; }
            set { library_path = value; }
        }

        public int Id_concrete
        {
            get { return id_concrete; }
            set { id_concrete = value; }
        }

        public int Id_concrete_supertype
        {
            get { return id_concrete_supertype; }
            set { id_concrete_supertype = value; }
        }

        public int Id_functor_app
        {
            get { return id_functor_app; }
            set { id_functor_app = value; }
        }

        public int GetId_abstract(IDbConnection dbcon)
        {
            //		return id_abstract;
            AbstractComponentFunctorApplication acfa = null;
            AbstractComponentFunctorApplication acfa_previous = null;
            bool found = false;
            Component cThis = this;
            while (!found)
            {
                acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(cThis.Id_functor_app, dbcon);
                acfa_previous = org.hpcshelf.DGAC.Backend.acfadao.retrieve_next(acfa.Id_functor_app, dbcon);
                if (acfa_previous == null)
                {
                    found = true;
                }
                else
                {
                    cThis = org.hpcshelf.DGAC.Backend.cdao.retrieveByFunctorApp(acfa_previous.Id_functor_app, dbcon);
                }

            }

            return acfa.Id_abstract;
        }



        public void SetId_abstract(int value)
        { id_abstract = value; }

        public string GetKind(IDbConnection dbcon)
        {
            //AbstractComponentFunctorApplicationDAO acfadao = new AbstractComponentFunctorApplicationDAO();
            // AbstractComponentFunctorDAO acfdao = new AbstractComponentFunctorDAO();
            AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(this.Id_functor_app, dbcon);
            return org.hpcshelf.DGAC.Backend.acfdao.retrieve(acfa.Id_abstract, dbcon).Kind;
        }

        private IDictionary<string, SupplyParameterComponent> component_contract = null;

        public void SetComponentContract(AbstractComponentFunctorApplication value, IDbConnection dbcon)
        {
            component_contract = new ConcurrentDictionary<string, SupplyParameterComponent>();

            foreach (SupplyParameter sp in Backend.spdao.list(value.Id_functor_app, dbcon))
                if (sp is SupplyParameterComponent)
                {
                    SupplyParameterComponent sp_new = (SupplyParameterComponent)sp.getCopy(this.id_functor_app, this.id_abstract);
                    component_contract[sp.Id_parameter] = sp_new;
                }
        }

        public IDictionary<string, AbstractComponentFunctorApplication> GetArguments0(IDbConnection dbcon)
        {
            IDictionary<string, AbstractComponentFunctorParameter> acfp_dict = null;

            //  Console.WriteLine("get_Parameter enter");
            IDictionary<string, AbstractComponentFunctorApplication> pars = new Dictionary<string, AbstractComponentFunctorApplication>();
            ICollection<SupplyParameter> sps = Backend.spdao.list(this.Id_functor_app, dbcon);
            foreach (SupplyParameter sp in sps)
            {
                //      Console.WriteLine("get_Parameter loop");
                if (sp is SupplyParameterComponent)
                {
                    SupplyParameterComponent sp_ = (SupplyParameterComponent)sp;
                    pars[sp_.Id_parameter] = Backend.acfadao.retrieve(sp_.Id_functor_app_actual, dbcon);
                }
                else // sp_ is SupplyParameterParameter
                {
                    SupplyParameterParameter sp_ = (SupplyParameterParameter)sp;
                    Console.Error.WriteLine("sp_.Id_parameter = " + sp_.Id_parameter);
                    Console.Error.WriteLine("sp_.Id_parameter_actual = " + sp_.Id_argument);
                    Console.Error.WriteLine("sp_.FreeVariable = " + sp_.FreeVariable);

                    if (acfp_dict == null)
                    {
                        IList<AbstractComponentFunctorParameter> acfp_list = Backend.acfpdao.list(this.id_abstract, dbcon);
                        acfp_dict = new Dictionary<string, AbstractComponentFunctorParameter>();
                        foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                            acfp_dict[acfp.Id_parameter] = acfp;
                    }

                    AbstractComponentFunctorParameter acfp0 = acfp_dict[sp_.Id_parameter];
                    pars[sp_.Id_parameter] = Backend.acfadao.retrieve(acfp0.Bounds_of, dbcon);
                }
            }
            return pars;
        }

        public IDictionary<string, AbstractComponentFunctorApplication> GetArguments(IDbConnection dbcon)
        {
            IDictionary<string, AbstractComponentFunctorParameter> acfp_dict = null;

            //	Console.WriteLine("get_Parameter enter");
            IDictionary<string, AbstractComponentFunctorApplication> pars = new Dictionary<string, AbstractComponentFunctorApplication>();
            ICollection<SupplyParameter> sps = org.hpcshelf.DGAC.Backend.spdao.list(this.Id_functor_app, dbcon);
            foreach (SupplyParameter sp0 in sps)
            {
                SupplyParameter sp = (component_contract.ContainsKey(sp0.Id_parameter)) ? component_contract[sp0.Id_parameter] : sp0;

                //		Console.WriteLine("get_Parameter loop");
                if (sp is SupplyParameterComponent)
                {
                    SupplyParameterComponent sp_ = (SupplyParameterComponent)sp;
                    pars[sp_.Id_parameter] = Backend.acfadao.retrieve(sp_.Id_functor_app_actual, dbcon);
                }
                else // sp_ is SupplyParameterParameter
                {
                    SupplyParameterParameter sp_ = (SupplyParameterParameter)sp;
                    Console.Error.WriteLine("sp_.Id_parameter = " + sp_.Id_parameter);
                    Console.Error.WriteLine("sp_.Id_parameter_actual = " + sp_.Id_argument);
                    Console.Error.WriteLine("sp_.FreeVariable = " + sp_.FreeVariable);

                    if (acfp_dict == null)
                    {
                        IList<AbstractComponentFunctorParameter> acfp_list = Backend.acfpdao.list(this.id_abstract, dbcon);
                        acfp_dict = new Dictionary<string, AbstractComponentFunctorParameter>();
                        foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                            acfp_dict[acfp.Id_parameter] = acfp;
                    }

                    AbstractComponentFunctorParameter acfp0 = acfp_dict[sp_.Id_parameter];
                    pars[sp_.Id_parameter] = Backend.acfadao.retrieve(acfp0.Bounds_of, dbcon);
                }
            }
            return pars;
        }


    }//class

}//namespace