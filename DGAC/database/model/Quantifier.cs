﻿using System;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class Quantifier
    {
        private int id_abstract;
        private decimal qualifier_value;
        private int id_abstract_quantifier;

        public decimal QuantifierValue
        {
            get { return qualifier_value; }
            set { qualifier_value = value; }
        }

        public int Id_abstract
        {
            get { return id_abstract; }
            set { id_abstract = value; }
        }

        public int Id_abstract_quantifier
		{
			get { return id_abstract_quantifier; }
			set { id_abstract_quantifier = value; }
		}


	}//class

}//namespace
