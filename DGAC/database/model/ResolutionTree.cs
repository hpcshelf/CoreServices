﻿using System;
using System.IO;
using System.Collections.Generic;

using System.Runtime.Serialization;

namespace org.hpcshelf.database
{
    [Serializable()]
    public class ResolutionTree
    {
        private int node_id;
        private int super_id;
       // private int? bound_node_id;
       // private int? bound_super_id;
        private int? id_abstract = null;
        private decimal? quantifier = null;
        private int level;

        public int? Id_abstract { get { return id_abstract; } set { id_abstract = value; } }
        public int NodeId { get { return node_id; } set { node_id = value; } }
        public int SuperId { get { return super_id; } set { super_id = value; } }
      //  public int? BoundNodeId { get { return bound_node_id; } set { bound_node_id = value; } }
       // public int? BoundSuperId { get { return bound_super_id; } set { bound_super_id = value; } }
        public decimal? Quantifier { get { return quantifier; } set { quantifier = value; } }
        public int Level { get { return level; } set { level = value; } }

    }//class

}//namespace


