﻿using System;
using System.Collections.Generic;
using System.IO;
using org.hpcshelf;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.ports;
using gov.cca;
using gov.cca.ports;
using SAFeSWL;
using org.hpcshelf.provenance;

namespace org.hpcshelf.kinds
{

    public interface IWorkflowKind : IActivateKind, GoPort
    {
        string SWLOrchestration { set; }
        int WorkflowHandle { set; }

       // void resolve(string component_id);
		//void deploy(string component_id);
		//void instantiate(string component_id);
		//void run(string component_id);
		//void release(string component_id);

	}

    public abstract class Workflow : Activate, IWorkflowKind
    {
        private SWLWorkflow<bool> w_tree = null;

        public string SWLOrchestration
        {
            set
            {
                SAFeSWL_OperationAnyType w = FileUtil.readXML<SAFeSWL_OperationAnyType>(value);
                Console.WriteLine("READING XML --- {0}", w == null);
                w_tree = WorkflowParser.convertoToSWLAbstractTree(w);
            }
        }

        private int workflow_handle;
        public int WorkflowHandle
        {
            set
            {
                this.workflow_handle = value;
            }
        }

        public ProvenancePort Core_provenance_port 
        { 
            get 
            {
                if (core_provenance_port == null)
					core_provenance_port = (ProvenancePort)Services.getPort(Constants.CORE_PROVENANCE_PORT_NAME);

				return core_provenance_port; 
            } 
            set 
            { 
                core_provenance_port = value; 
            } 
        }
        public ICoreServices Core_lifecycle_port 
        { 
            get 
            {
                if (core_lifecycle_port == null)
                {
                    core_lifecycle_port = (ICoreServices)Services.getPort(Constants.CORE_LIFECYCLE_PORT_NAME);
					Services.addProvidesPort(core_provenance_port, Constants.PROVENANCE_PORT_NAME, Constants.PROVENANCE_PORT_TYPE, new TypeMapImpl());
				}


				return core_lifecycle_port; 
            } 
            set 
            { 
                core_lifecycle_port = value; 
            } 
        }

        private ProvenancePort core_provenance_port = null;
        private ICoreServices core_lifecycle_port = null;

        public new void setServices(Services services)
        {
            base.setServices(services);

            services.registerUsesPort(Constants.CORE_PROVENANCE_PORT_NAME, Constants.PROVENANCE_PORT_TYPE, new TypeMapImpl());
            services.registerUsesPort(Constants.CORE_LIFECYCLE_PORT_NAME, Constants.LIFECYCLE_PORT_TYPE, new TypeMapImpl());
        }

        public override void main()
        {
            string system_ref = Core_provenance_port.SystemRef;

            string orchestration_trace_file = Path.Combine(Constants.PATH_CATALOG_FOLDER, system_ref, "orchestration.history");
            if (File.Exists(orchestration_trace_file))
            {
                ProvenanceTracer provenance_tracer = new ProvenanceTracer();
                provenance_tracer.load(orchestration_trace_file);

                IList<SWLWorkflow<bool>> action_list = new List<SWLWorkflow<bool>>();

                Tuple<string, string> next_action = provenance_tracer.next();
                while (next_action != null)
                {
                    string port_name = next_action.Item1;
                    string action_name = next_action.Item2;
                    action_list.Add(new SWLWorkflowInvoke<bool>(port_name, action_name));
                    next_action = provenance_tracer.next();
                }
                SWLWorkflow<bool>[] action_array = new SWLWorkflow<bool>[action_list.Count];
                action_list.CopyTo(action_array, 0);

                w_tree = new SWLWorkflowSequence<bool>(action_array);
            }
            else
            {
                string orchestration_file_location_path = Path.Combine(Constants.PATH_GAC, this.QualifiedComponentTypeName, "workflow.safeswl");
                this.SWLOrchestration = File.ReadAllText(orchestration_file_location_path);
            }

            Console.WriteLine("VISIT ORCHESTRATION !!! " + (w_tree == null));
            w_tree.accept(new SWLVisitorOrchestrate(this.Services, workflow_handle, Core_lifecycle_port, Core_provenance_port));
            Console.WriteLine("FINISH ORCHESTRATION !!! ");
        }

/*        public void resolve(string component_id)
        {
            this.Core_lifecycle_port.resolve(component_id);
        }

        public void deploy(string component_id)
        {
            this.Core_lifecycle_port.deploy(component_id);
        }

        public void instantiate(string component_id)
        {
            this.Core_lifecycle_port.instantiate(component_id);
        }

        public void run(string component_id)
        {
            this.Core_lifecycle_port.run(component_id);
        }

        public void release(string component_id)
        {
            this.Core_lifecycle_port.release(component_id);
        }
  */  }


}
