﻿using SAFeSWL;
using System;
using System.Collections.Generic;


namespace org.hpcshelf.kinds
{
    public class WorkflowParser
    {
        public static SWLWorkflow<bool> convertoToSWLAbstractTree(SAFeSWL_OperationAnyType workflow)
        {
            return traverse(workflow);
        }

        private static SWLWorkflow<bool> traverse(SAFeSWL_OperationAnyType workflow)
        {

            switch (workflow.ItemElementName)
            {
                case SAFeSWL.ItemChoiceType.skip: return traverse(SAFeSWL.ItemChoiceType.skip, (SAFeSWL_OperationPrimitiveType)workflow.Item);
                case SAFeSWL.ItemChoiceType.@break: return traverse(SAFeSWL.ItemChoiceType.@break, (SAFeSWL_OperationPrimitiveType)workflow.Item);
                case SAFeSWL.ItemChoiceType.@continue: return traverse(SAFeSWL.ItemChoiceType.@continue, (SAFeSWL_OperationPrimitiveType)workflow.Item);
                case SAFeSWL.ItemChoiceType.start: return traverse(SAFeSWL.ItemChoiceType.start, (SAFeSWL_OperationPrimitiveInvokeActionAsyncType)workflow.Item);
                case SAFeSWL.ItemChoiceType.wait: return traverse(SAFeSWL.ItemChoiceType.wait, (SAFeSWL_OperationPrimitiveInvokeActionAsyncType)workflow.Item);
                case SAFeSWL.ItemChoiceType.invoke: return traverse(SAFeSWL.ItemChoiceType.invoke, (SAFeSWL_OperationPrimitiveInvokeActionType)workflow.Item);
                case SAFeSWL.ItemChoiceType.cancel: return traverse(SAFeSWL.ItemChoiceType.cancel, (SAFeSWL_OperationPrimitiveInvokeActionAsyncType)workflow.Item);
                case SAFeSWL.ItemChoiceType.sequence: return traverse(SAFeSWL.ItemChoiceType.sequence, (SAFeSWL_OperationManyType)workflow.Item);
                case SAFeSWL.ItemChoiceType.parallel: return traverse(SAFeSWL.ItemChoiceType.parallel, (SAFeSWL_OperationManyType)workflow.Item);
                case SAFeSWL.ItemChoiceType.choice: return traverse(SAFeSWL.ItemChoiceType.choice, (SAFeSWL_OperationChoiceType)workflow.Item);
                case SAFeSWL.ItemChoiceType.iterate: return traverse(SAFeSWL.ItemChoiceType.iterate, (SAFeSWL_IterateType)workflow.Item);
                case SAFeSWL.ItemChoiceType.semaphore: return traverse(SAFeSWL.ItemChoiceType.semaphore, (SAFeSWL_CreateSemaphore) workflow.Item);
				case SAFeSWL.ItemChoiceType.p: return traverse(SAFeSWL.ItemChoiceType.p, (string)workflow.Item);
				case SAFeSWL.ItemChoiceType.v: return traverse(SAFeSWL.ItemChoiceType.v, (string)workflow.Item);
				default: return null;
            }



            /* if (workflow.Item is SAFeSWL_OperationPrimitiveType) 
			{
				return traverse (workflow.ItemElementName, (SAFeSWL_OperationPrimitiveType) workflow.Item);
			} 
			else if (workflow.Item is SAFeSWL_OperationPrimitiveInvokeActionAsyncType) 
			{
				return traverse (workflow.ItemElementName, (SAFeSWL_OperationPrimitiveInvokeActionAsyncType) workflow.Item);
			} 
			else if (workflow.Item is SAFeSWL_OperationPrimitiveInvokeActionType) 
			{
				return traverse (workflow.ItemElementName, (SAFeSWL_OperationPrimitiveInvokeActionType) workflow.Item);
			}
			else if (workflow.Item is SAFeSWL_OperationManyType) 
			{
				return traverse (workflow.ItemElementName, (SAFeSWL_OperationManyType) workflow.Item);
			} 
			else if (workflow.Item is SAFeSWL_OperationChoiceType) 
			{
				return traverse (workflow.ItemElementName, (SAFeSWL_OperationChoiceType) workflow.Item);
			} 
			else if (workflow.Item is SAFeSWL_OperationAnyType) 
			{
				return traverse (workflow.ItemElementName, (SAFeSWL_OperationAnyType) workflow.Item);
			} 
			else
				throw new Exception ("convert_to_abstract_syntax/traverse: UNRECOGNIZED SAFeSWL constructor.");
*/
        }

        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType semaphore, SAFeSWL_CreateSemaphore sem)
        {
            return new SWLWorkflowNewSemaphore<bool>(sem.id, sem.value);
		}

        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType semaphore_operation, string item)
        {
            switch(semaphore_operation)
            {
                case SAFeSWL.ItemChoiceType.p: return new SWLWorkflowPSemaphore<bool>(item); 
				case SAFeSWL.ItemChoiceType.v: return new SWLWorkflowVSemaphore<bool>(item);
                default: return null;
			}


        }

        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType element_name, SAFeSWL_OperationPrimitiveType workflow)
        {
            if (element_name == SAFeSWL.ItemChoiceType.skip)
            {
                return new SWLWorkflowSkip<bool>();
            }
            else if (element_name == SAFeSWL.ItemChoiceType.@break)
            {
                return new SWLWorkflowBreak<bool>();
            }
            else if (element_name == SAFeSWL.ItemChoiceType.@continue)
            {
                return new SWLWorkflowBreak<bool>();
            }
            else
                throw new Exception("convert_to_abstract_syntax/traverse: wait/break/continue expected !");
        }

        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType element_name, SAFeSWL_OperationPrimitiveInvokeActionAsyncType workflow)
        {
            SAFeSWL_OperationPrimitiveInvokeActionAsyncType action = (SAFeSWL_OperationPrimitiveInvokeActionAsyncType)workflow;

            if (element_name == SAFeSWL.ItemChoiceType.start)
            {
                string handle = action.handle_id;
                string port_name = action.port;
                string action_name = action.action;
                return new SWLWorkflowStart<bool>(handle, port_name, action_name);
            }
            else if (element_name == SAFeSWL.ItemChoiceType.wait)
            {
                string handle = action.handle_id;
                return new SWLWorkflowWait<bool>(handle);
            }
            else if (element_name == SAFeSWL.ItemChoiceType.cancel)
            {
                string handle = action.handle_id;
                return new SWLWorkflowCancel<bool>(handle);
            }
            else
                throw new Exception("convert_to_abstract_syntax/traverse: start/wait/cancel expected !");
        }

        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType element_name, SAFeSWL_OperationPrimitiveInvokeActionType workflow)
        {
            SAFeSWL_OperationPrimitiveInvokeActionType action = (SAFeSWL_OperationPrimitiveInvokeActionType)workflow;
            if (element_name == SAFeSWL.ItemChoiceType.invoke)
            {
                string port_name = action.port;
                string action_name = action.action;
                return new SWLWorkflowInvoke<bool>(port_name, action_name);
            }
            else
                throw new Exception("convert_to_abstract_syntax/traverse: invoke expected !");
        }

        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType element_name, SAFeSWL_OperationManyType workflow)
        {
            SWLWorkflow<bool>[] actions = traverse((SAFeSWL_OperationManyType)workflow);

            if (element_name == SAFeSWL.ItemChoiceType.sequence)
            {
                Console.WriteLine("AST SEQUENCE {0}", actions.Length);
                return new SWLWorkflowSequence<bool>(actions);
            }
            else if (element_name == SAFeSWL.ItemChoiceType.parallel)
            {
                Console.WriteLine("AST PARALLEL {0}", actions.Length);
                return new SWLWorkflowParallel<bool>(actions);
            }
            else
                throw new Exception("convert_to_abstract_syntax/traverse: sequence/parallel expected !");
        }


        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType element_name, SAFeSWL_OperationChoiceType workflow)
        {
            if (element_name == SAFeSWL.ItemChoiceType.choice)
            {
                SWLWorkflow<bool>[] action_list;
                string[] action_name;

                traverse(workflow.select, out action_name, out action_list);

                return new SWLWorkflowChoice<bool>(workflow.port, action_name, action_list);
            }
            else
                throw new Exception("convert_to_abstract_syntax/traverse: choice expected !");

        }

        private static void traverse(SAFeSWL_SelectionGuardType[] select_guard, out string[] action_name, out SWLWorkflow<bool>[] action_list)
        {
            action_list = new SWLWorkflow<bool>[select_guard.Length];
			action_name = new string[select_guard.Length];

			for (int i = 0; i < select_guard.Length;i++)
            {
                action_list[i] = traverse(select_guard[i]);
                action_name[i] = select_guard[i].action;
            }
        }


        private static SWLWorkflow<bool>[] traverse(SAFeSWL_OperationManyType workflow)
        {
            IList<SWLWorkflow<bool>> action_list = new List<SWLWorkflow<bool>>();

            SAFeSWL_OperationManyType action = (SAFeSWL_OperationManyType)workflow;

            for (int i = 0; i < workflow.ItemsElementName.Length; i++)
            {
                switch (workflow.ItemsElementName[i])
                {
                    case SAFeSWL.ItemsChoiceType.skip:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.skip, (SAFeSWL_OperationPrimitiveType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.@break:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.@break, (SAFeSWL_OperationPrimitiveType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.@continue:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.@continue, (SAFeSWL_OperationPrimitiveType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.start:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.start, (SAFeSWL_OperationPrimitiveInvokeActionAsyncType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.wait:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.wait, (SAFeSWL_OperationPrimitiveInvokeActionAsyncType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.invoke:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.invoke, (SAFeSWL_OperationPrimitiveInvokeActionType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.cancel:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.cancel, (SAFeSWL_OperationPrimitiveInvokeActionAsyncType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.sequence:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.sequence, (SAFeSWL_OperationManyType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.parallel:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.parallel, (SAFeSWL_OperationManyType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.choice:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.choice, (SAFeSWL_OperationChoiceType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.iterate:
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.iterate, (SAFeSWL_IterateType)workflow.Items[i]));
                        break;
                    case SAFeSWL.ItemsChoiceType.semaphore: 
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.semaphore, (SAFeSWL_CreateSemaphore)workflow.Items[i])); 
                        break;
                    case SAFeSWL.ItemsChoiceType.p: 
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.p, (string)workflow.Items[i])); 
                        break;
                    case SAFeSWL.ItemsChoiceType.v: 
                        action_list.Add(traverse(SAFeSWL.ItemChoiceType.v, (string)workflow.Items[i])); 
                        break;
				}
            }

            SWLWorkflow<bool>[] actions = new SWLWorkflow<bool>[action_list.Count];
            action_list.CopyTo(actions, 0);

            return actions;
        }

        private static SWLWorkflow<bool> traverse(SAFeSWL.ItemChoiceType element_name, SAFeSWL_IterateType workflow)
        {
            // SIMPLE ITERATE
            bool iterate_type_1 = ((workflow.port != null && workflow.loop != null && workflow.until != null) || (workflow.port == null && workflow.loop == null && workflow.until == null)) /*&& workflow.Item != null*/ && workflow.Items == null;

            // SELECT ITERATE
            bool iterate_type_2 = workflow.port != null && workflow.loop == null && workflow.until == null && workflow.Item == null && workflow.Items != null;

            // BRANCH ITERATE
            bool iterate_type_3 = workflow.port == null && workflow.loop == null && workflow.until == null && workflow.Item == null && workflow.Items != null;

            IDictionary<string, IList<Tuple<string, SWLWorkflow<bool>>>> branches = new Dictionary<string, IList<Tuple<string, SWLWorkflow<bool>>>>();
            if (iterate_type_1)
                traverse(workflow.port, workflow.loop, workflow.until, workflow, branches);
            else if (iterate_type_2)
                traverse(workflow.port, workflow.Items, branches);
            else if (iterate_type_3)
                for (int i = 0; i < workflow.Items.Length; i++)
                {
                    SAFeSWL.SAFeSWL_BranchType a = (SAFeSWL.SAFeSWL_BranchType)workflow.Items[i];
                    traverse(a, branches);
                }
            else
                throw new Exception("Unexpected Iterate Configuration ...");

            return new SWLWorkflowIterate<bool>(branches);
        }

        private static void traverse(string id_port, SAFeSWL_OperationAnyType[] items, IDictionary<string, IList<Tuple<string, SWLWorkflow<bool>>>> branches)
        {
            IList<Tuple<string, SWLWorkflow<bool>>> l = new List<Tuple<string, SWLWorkflow<bool>>>();
            branches[id_port] = l;
            foreach (SAFeSWL.SAFeSWL_SelectionGuardType select_action in items)
            {
                SWLWorkflow<bool> a = traverse((SAFeSWL_OperationAnyType)select_action);
                Tuple<string, SWLWorkflow<bool>> c = new Tuple<string, SWLWorkflow<bool>>(select_action.action, a);
                l.Add(c);
            }
        }

        private static void traverse(string id_port, string loop, string until, SAFeSWL_OperationAnyType item, IDictionary<string, IList<Tuple<string, SWLWorkflow<bool>>>> branches)
        {
            SWLWorkflow<bool> a = item != null ? traverse((SAFeSWL_OperationAnyType)item) : new SWLWorkflowSkip<bool>();

            IList<Tuple<string, SWLWorkflow<bool>>> l = new List<Tuple<string, SWLWorkflow<bool>>>();
            branches[id_port] = l;
            Tuple<string, SWLWorkflow<bool>> c1 = new Tuple<string, SWLWorkflow<bool>>(loop, a);
            Tuple<string, SWLWorkflow<bool>> c2 = new Tuple<string, SWLWorkflow<bool>>(until, new SWLWorkflowBreak<bool>());
            l.Add(c1);
            l.Add(c2);
        }

        private static void traverse(SAFeSWL_BranchType branch_action, IDictionary<string, IList<Tuple<string, SWLWorkflow<bool>>>> branches)
        {
            if (branch_action.port != null && branch_action.loop != null && branch_action.until != null && (branch_action.select == null || branch_action.select.Length == 0) && branch_action.Item != null)
                traverse(branch_action.port, branch_action.loop, branch_action.until, (SAFeSWL_OperationAnyType)branch_action.Item, branches);
            else if (branch_action.port != null && branch_action.loop == null && branch_action.until == null && branch_action.Item == null && (branch_action.select != null && branch_action.select.Length > 0))
                traverse(branch_action.port, branch_action.select, branches);
        }
    }
}

