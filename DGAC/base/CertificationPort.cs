﻿/*=============================================================
(c) all rights reserved
================================================================*/

using gov.cca;

namespace org.hpcshelf.ports
{
    public interface CertificationPort : ICertification, Port
    {
    }

    public interface ICertification
    {
        CertificationStatus Status { get; }
        string CertifiableID { get; set; }
    }

    public enum CertificationStatus { Success, Fail };
 }
