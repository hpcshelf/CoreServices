﻿
using System;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.ports;
using gov.cca;

namespace org.hpcshelf.kinds
{
    public interface ICertifierKind : CertificationPort, IWorkflowKind
	{
	}

    public abstract class Certifier : Workflow, ICertifierKind 
	{	 
        public abstract CertificationStatus Status { get; }

        private string certifiable_id;

        public string CertifiableID { get { return certifiable_id; } set { certifiable_id = value; } }

        public new void setServices(Services services)
        {
            base.setServices(services);

            CertificationPort certification_port = new CertificationPortImpl(this);
            services.addProvidesPort(certification_port, Constants.CERTIFICATION_PORT_NAME, Constants.CERTIFICATION_PORT_TYPE, new TypeMapImpl());
            services.registerUsesPort(Constants.FETCH_FILES_PORT_NAME, Constants.FETCH_FILES_PORT_TYPE, new TypeMapImpl());
		}

        private class CertificationPortImpl : CertificationPort
        {
            private CertificationPort unit = null;

            public CertificationPortImpl(CertificationPort unit) { this.unit = unit;}

            CertificationStatus ICertification.Status { get { return unit.Status; } }

            string ICertification.CertifiableID { get { return unit.CertifiableID; } set { unit.CertifiableID = value; }}
        }
    }
}
