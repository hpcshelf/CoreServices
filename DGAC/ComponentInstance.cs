﻿using System;
using System.Collections.Generic;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using gov.cca;
using gov.cca.ports;
using static org.hpcshelf.DGAC.Backend;

namespace DGAC
{
    public interface ISystem
    {
		string SessionID { get; }
		void newComponentInstance(string id, ResolveComponentHierarchy ctree);

        string[] ChannelID { get; set; }
        int[] ChannelFacetInstance { get; set; }
        string[] ChannelFacetAddress { get; set; }
    }

    public class ParallelComputingSystem : ISystem
    {
        private IDictionary<string, IComponentInstance> component_instances = new Dictionary<string, IComponentInstance>();
        private int facet_instance;
        private ISession session;
        private string session_id_string;

        internal ParallelComputingSystem(string session_id_string, int facet_instance, ISession session)
        {
            this.session_id_string = session_id_string;
            this.facet_instance = facet_instance;
            this.session = session;
        }

        public static IDictionary<object, ISystem> SystemInstance = new Dictionary<object, ISystem>();

        public string SessionID { get { return session_id_string; } }

        private string[] channel_id;
        public string[] ChannelID { get { return channel_id; } set { channel_id = value; }}

        private int[] channel_facet_instance;
        public int[] ChannelFacetInstance { get { return channel_facet_instance; } set { channel_facet_instance = value; }}

        private string[] channel_facet_address;
        public string[] ChannelFacetAddress { get { return channel_facet_address; } set { channel_facet_address = value; }}

        public static ISession newSystem(string session_id_string, int facet_instance)
        {
            object handle = new object();

            ISession session = startSession(session_id_string);

            ParallelComputingSystem system = new ParallelComputingSystem(session_id_string, facet_instance, session);

            SystemInstance[session] = system;

            return session;
        }

        public void newComponentInstance(string id, ResolveComponentHierarchy ctree)
        {
            component_instances[id] = new ComponentInstance(ctree, facet_instance, this);
        }


    }


    interface IComponentInstance
    {

    }

    class ComponentInstance : IComponentInstance
    {
        private ISystem system;

        internal ComponentInstance(ResolveComponentHierarchy ctree, int facet_instance, ISystem system)
        {
            this.system = system;

            ISession session = open_sessions[system.SessionID];
            Services frwServices = session.Services;

            BuilderService bsPort = (BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

            ComponentID inner_cid = createComponentInstance(ctree, facet_instance, system.ChannelID, system.ChannelFacetInstance, system.ChannelFacetAddress, bsPort);
            bindComponentInstance(ctree, new int[1] {0}, bsPort);
		}

    }
}
