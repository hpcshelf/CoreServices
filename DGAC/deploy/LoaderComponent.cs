﻿using System;
using System.Collections.Generic;
using System.Text;
using org.hpcshelf.database;
using System.Diagnostics;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using System.Globalization;
using System.Data;

namespace HPE_DGAC_LoadDB
{
    public abstract class LoaderComponent
    {

        protected IList<InnerComponentType> inner = null;
        protected IList<InnerComponentType> innerAll = null;
        protected IDictionary<string, InnerComponentType> innerAllByCRef = null;

		protected IList<ParameterType> parameter = null;
		protected IDictionary<string, ParameterType> parameterByVarName = null;
		protected IDictionary<string, ParameterType> parameterByParId = null;
		protected IDictionary<string,ParameterType> parameterByCRef = null;
		protected IList<ParameterSupplyType> parameterSupply = null;
        protected IDictionary<string,ParameterSupplyType> parameterSupplyByVar = null;
		protected IDictionary<string, ParameterSupplyType> parameterSupplyByCRef = null;
		protected IList<InnerRenamingType> innerRenaming = null;
        protected IDictionary<string,string> fusion = null;
        protected IList<SplitType> split = null;
        protected IList<RecursiveEntryType> recursiveEntry = null;
        protected IList<InterfaceType> anInterface = null;
        protected IList<UnitType> unit = null;
        protected string argument_calculator = null;
        //protected IList<EnumeratorType> enumerator = null;
		protected IList<ExternalLibraryType> externalLibrary = null;
		//protected IList<FusionsOfReplicatorsType> fusionOfReplicators = null;

        public HashComponent loadComponent(ComponentType c, byte[] snk_contents, ref IList<ExternalLibraryType>  externalLibrary, IDictionary<string,SupplyParameter> var_context, IDbConnection dbcon)
        {
            //ComponentHeaderType ch = c.header;
            LoadBodyItems(c.componentInfo);

            HashComponent cc = loadComponent_(c, snk_contents, var_context, dbcon);

			externalLibrary = this.externalLibrary;

            return cc;
        }

        protected abstract HashComponent loadComponent_(ComponentType c, byte[] snk_contents, IDictionary<string, SupplyParameter> var_context, IDbConnection dbcon);

        //public abstract bool componentExists(string library_path, out IList<Component> cRef);

        protected void LoadBodyItems(object[] cb)
        {
			Console.Write ("LoadBodyItems:" + cb==null);
			foreach (Object o in cb)
            {
				//Console.Write ("Object:");
                if (o is InnerComponentType)
                {
                   // Console.WriteLine("Object:InnerComponentType " + ((InnerComponentType)o).localRef);
                    if (inner == null)
                    {
                        inner = new List<InnerComponentType>();
                        innerAll = new List<InnerComponentType>();
                        innerAllByCRef = new Dictionary<string, InnerComponentType>();
                    }

                    inner.Add((InnerComponentType)o);
                    innerAll.Add((InnerComponentType)o);
                    innerAllByCRef[((InnerComponentType)o).localRef] = (InnerComponentType)o;
                }
                else if (o is ParameterType)
                {
					//Console.WriteLine ("Object:ParameterType");
                    if (parameter == null)
                    {
                        parameter = new List<ParameterType>();
                        parameterByVarName = new Dictionary<string, ParameterType>();
                        parameterByCRef = new Dictionary<string, ParameterType>();
						parameterByParId = new Dictionary<string, ParameterType>();
					}
                    parameter.Add((ParameterType)o);
                    parameterByCRef[((ParameterType)o).componentRef] = (ParameterType)o;
                    parameterByParId[((ParameterType)o).formFieldId] = (ParameterType)o;
                    parameterByVarName[((ParameterType)o).varName] = (ParameterType)o;
				}
                else if (o is ParameterSupplyType)
                {
					//Console.WriteLine ("Object:ParameterSupplyType");
                    if (parameterSupply == null)
                    {
                        parameterSupply = new List<ParameterSupplyType>();
                        parameterSupplyByVar = new Dictionary<string, ParameterSupplyType>();
                        parameterSupplyByCRef = new Dictionary<string, ParameterSupplyType>();
					}
					parameterSupply.Add((ParameterSupplyType)o);
                    parameterSupplyByVar[((ParameterSupplyType)o).varName] = parameterSupplyByCRef[((ParameterSupplyType)o).cRef] = (ParameterSupplyType)o;
				}
                else if (o is InnerRenamingType)
                {
					//Console.WriteLine ("Object:InnerRenamingType");
                    if (innerRenaming == null) innerRenaming = new List<InnerRenamingType>();
                    innerRenaming.Add((InnerRenamingType)o);
                }
                else if (o is FusionType)
                {
					//Console.WriteLine ("Object:FusionType");
					if (fusion == null) fusion = new Dictionary<string, string>();
					FusionType fo = (FusionType)o;
					fusion.Add(fo.pRef,fo.cRefs[0]);
                }
                else if (o is SplitType)
                {
					//Console.WriteLine ("Object:SplitType");
                    if (split == null) split = new List<SplitType>();
                    split.Add((SplitType)o);
                }
                else if (o is RecursiveEntryType)
                {
				    //Console.WriteLine ("Object:RecursiveEntryType");
                    if (recursiveEntry == null) recursiveEntry = new List<RecursiveEntryType>();
                    recursiveEntry.Add((RecursiveEntryType)o);
                }
                else if (o is InterfaceType)
                {
					//Console.WriteLine ("Object:InterfaceType");
                    if (anInterface == null) anInterface = new List<InterfaceType>();
                    anInterface.Add((InterfaceType)o);
                }
                else if (o is UnitType)
                {
					//Console.WriteLine ("Object:UnitType");
                    if (unit == null) unit = new List<UnitType>();
                    unit.Add((UnitType)o);
                }
                else if (o is ArgumentCalculatorType)
                {
					//Console.WriteLine("Object:ArgumentCalculatorType");
					argument_calculator = (string) ((ArgumentCalculatorType)o).contents;
                }
				else if (o is ExternalLibraryType)
				{
					//Console.WriteLine ("Object:ExternalLibraryType");
					if (externalLibrary == null) externalLibrary = new List<ExternalLibraryType>();
					externalLibrary.Add((ExternalLibraryType)o);
				}
                else
                {
                    // Tipos inesperado ....
                }
            }
        }

      //  protected AbstractComponentFunctorApplication newAbstractComponentFunctorApplication(ComponentInUseType c)
      //  {
      //      return newAbstractComponentFunctorApplication(c, new Dictionary<string, SupplyParameter>());
      //  }

		protected AbstractComponentFunctorApplication newAbstractComponentFunctorApplication(ComponentInUseType c, IDictionary<string, SupplyParameter> context, IDbConnection dbcon)
        {
            AbstractComponentFunctor a = lookForAbstractComponentFunctor(c, dbcon);
            if (a == null)
            {
                return null;
            }

            // CREATE AbstractComponentFunctorApplication

			Tuple<char, char> q = null;
            bool is_quantifier = ((q = Backend.isQuantifier(a.Id_abstract, dbcon)) != null);

            AbstractComponentFunctorApplication aAppNew = !is_quantifier ? new AbstractComponentFunctorApplication() : new AbstractComponentFunctorApplicationQuantifier();
            aAppNew.Id_functor_app = DBConnector.nextKey("id_functor_app", "abstractcomponentfunctorapplication", dbcon);
            aAppNew.Id_abstract = a.Id_abstract;
            if (c.quantifier_value != null)
            {
                decimal quantifier_value;
				NumberStyles style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign;
                if (c.quantifier_value.EndsWith("inf"))
                    ((AbstractComponentFunctorApplicationQuantifier)aAppNew).Quantifier = q.Item2 == '+' ? (q.Item1 == 'i' ? (decimal) int.MinValue: Decimal.MinValue) : (q.Item1 == 'i' ? (decimal) int.MaxValue : Decimal.MaxValue);
                else if (decimal.TryParse(c.quantifier_value, style, CultureInfo.InvariantCulture, out quantifier_value))
                    ((AbstractComponentFunctorApplicationQuantifier)aAppNew).Quantifier = quantifier_value;
            }

            Backend.acfadao.insert(aAppNew, dbcon);

            // REGISTER parameters (follow supply-of, configure formal-parameter)
            loadAbstractComponentFunctorApplicationParameters(c, aAppNew, aAppNew, context, dbcon);

            return aAppNew;
        }

        protected IList<AbstractComponentFunctorApplication> newAbstractComponentFunctorApplicationForImplements(ComponentInUseType c, IDictionary<string, SupplyParameter> var_context, IDbConnection dbcon)
        {

		    IList<AbstractComponentFunctor> ancestrals = new List<AbstractComponentFunctor>();	

			AbstractComponentFunctor a_current = lookForAbstractComponentFunctor(c, dbcon);
			if (a_current == null)
                return null;

			{
				AbstractComponentFunctor a = a_current;
				ancestrals.Add (a);				
				while (a.Id_functor_app_supertype > 0) {
					AbstractComponentFunctorApplication acfa_ancestral = org.hpcshelf.DGAC.Backend.acfadao.retrieve (a.Id_functor_app_supertype, dbcon);
					a = org.hpcshelf.DGAC.Backend.acfdao.retrieve (acfa_ancestral.Id_abstract, dbcon);
					ancestrals.Add (a);
				}
			}
			
			IList<AbstractComponentFunctorApplication> aAppNewList = new List<AbstractComponentFunctorApplication>();
			
			AbstractComponentFunctorApplication aAppNewOld = null;
			AbstractComponentFunctorApplication aAppNew = null;
			foreach (AbstractComponentFunctor a in ancestrals) 
			{
                // CREATE AbstractComponentFunctorApplication
				
                aAppNew = new AbstractComponentFunctorApplication();
                aAppNew.Id_abstract = a.Id_abstract;
				aAppNew.Id_functor_app = DBConnector.nextKey("id_functor_app", "abstractcomponentfunctorapplication", dbcon);
                Backend.acfadao.insert(aAppNew, dbcon);
                loadAbstractComponentFunctorApplicationParameters(c, aAppNew, aAppNewOld == null ? aAppNew : aAppNewOld, var_context, dbcon);
				aAppNewList.Add(aAppNew);
				if (aAppNewOld != null)
                    Backend.acfadao.updateIdFunctorAppNext(aAppNewOld, aAppNew.Id_functor_app, dbcon);
				aAppNewOld = aAppNew;				
			}

            return aAppNewList;
       //     }
        }
        
		protected void loadAbstractComponentFunctorApplicationParameters(ComponentInUseType c, 
		                                                                 AbstractComponentFunctorApplication aNew, 
		                                                                 AbstractComponentFunctorApplication aNew_context,
                                                                         IDictionary<string,SupplyParameter> context, IDbConnection dbcon)
        {
            IDictionary<string, SupplyParameter> pars = new Dictionary<string,SupplyParameter>();

            if (c.parameter != null)
            {
                int total = c.parameter.Length;

                int counter = 0;
                foreach (ParameterRenaming p in c.parameter)
                {
                    counter ++;

                    string formFieldId = p.formFieldId; 
                    string varName = p.varName;
                   // Console.WriteLine("L {0} -- {1} -- {2} -- {3}", varName, formFieldId, total, counter);

                    SupplyParameter p_ = context.ContainsKey(varName) ? context[varName] : null;

                    if (p_ == null)
                    {
                        ParameterType topParameter = lookForParameterByVarName(varName);

                        if (topParameter != null)
                        {
                            p_ = new SupplyParameterParameter();
                            ((SupplyParameterParameter)p_).Id_argument = topParameter.formFieldId;
                            ((SupplyParameterParameter)p_).FreeVariable = false;
                        }
                        else
                        {
                            ParameterSupplyType s = lookForSupplyForVarName(varName);
                            if (s != null)
                            {
                                String cRef = s.cRef;

                                p_ = new SupplyParameterComponent();
                                // Look for the inner component that supplies that parameter.
                                InnerComponentType inner_parameter = lookForInnerComponent(cRef);
                                AbstractComponentFunctorApplication cPar = newAbstractComponentFunctorApplication(inner_parameter, context, dbcon);
                                if (cPar == null)
                                {
                                    throw new Exception("DEPLOY ERROR: Unresolved Dependency for base component (context actual parameter) : " + inner_parameter.name);
                                }
                            ((SupplyParameterComponent)p_).Id_functor_app_actual = cPar.Id_functor_app;

                            }
                            else
                            {
                                p_ = new SupplyParameterParameter();
                                ((SupplyParameterParameter)p_).Id_argument = null;
                                ((SupplyParameterParameter)p_).FreeVariable = true;
                            }
                        }

						p_.Id_functor_app = aNew.Id_functor_app;
						p_.Id_abstract = aNew.Id_abstract;
						p_.Id_parameter = formFieldId;

					//Backend.spdao.insert(p_);

						context[varName] = p_;
                    }
                    else 
                    {
                       // Console.WriteLine("{0} found ! {1} {2}", varName, c.localRef, c.name);
                    }

                    pars[formFieldId] = p_;
                }
            }

            foreach (KeyValuePair<string,SupplyParameter> par in pars)
            {
                SupplyParameter par_copy = par.Value.getCopy(aNew.Id_functor_app, aNew.Id_abstract);
                par_copy.Id_parameter = par.Key;
                par_copy.Id_abstract = aNew.Id_abstract; // ?
                if (Backend.spdao.retrieve(par.Key, aNew.Id_functor_app, dbcon) == null)
                    Backend.spdao.insert(par_copy, dbcon);
                else
                    Console.WriteLine();
            }

            loadSuppliedParametersOfSupertype(c, aNew, aNew_context, dbcon);
        }

		void loadSuppliedParametersOfSupertype (ComponentInUseType c, AbstractComponentFunctorApplication aNew, AbstractComponentFunctorApplication aNew_context, IDbConnection dbcon)
		{

			int id_functor_app_supertype;
			if (aNew_context.Id_abstract != aNew.Id_abstract)				 
			{
			//	Console.WriteLine ("id_abstract_context != aNew.Id_abstract : " + aNew_context.Id_abstract  + " " + aNew.Id_abstract);
				AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve (aNew_context.Id_abstract, dbcon);
				id_functor_app_supertype = acf.Id_functor_app_supertype;
			} 
			else 
			{
			//	Console.WriteLine ("id_abstract_context == aNew.Id_abstract : " + aNew_context.Id_abstract  + " " + aNew.Id_abstract);
				AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve (aNew.Id_abstract, dbcon);
				id_functor_app_supertype = acf.Id_functor_app_supertype;
			}

			while (id_functor_app_supertype > 0) 
			{
				AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve (id_functor_app_supertype, dbcon);

				ICollection<SupplyParameter> sp_list = org.hpcshelf.DGAC.Backend.spdao.list (id_functor_app_supertype, dbcon);
				foreach (SupplyParameter sp in sp_list)
				{
					if (sp is SupplyParameterComponent) 
					{
						SupplyParameterComponent spc = (SupplyParameterComponent)sp;
						SupplyParameterComponent spc_new = new SupplyParameterComponent ();

						spc_new.Id_functor_app = aNew.Id_functor_app;
						spc_new.Id_abstract = aNew.Id_abstract;
						spc_new.Id_functor_app_actual = spc.Id_functor_app_actual;
						spc_new.Id_parameter = spc.Id_parameter;
                        if (org.hpcshelf.DGAC.Backend.spdao.retrieve(spc_new.Id_parameter,spc_new.Id_functor_app, dbcon) == null)
						    org.hpcshelf.DGAC.Backend.spdao.insert(spc_new, dbcon);
					} 
					else if (sp is SupplyParameterParameter) 
					{

					}

				}

				AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve (acfa.Id_abstract, dbcon);
				id_functor_app_supertype = acf.Id_functor_app_supertype;

				Trace.WriteLineIf (id_functor_app_supertype > 0, "LOOPING TO SUPERTYPE !!!");
			}

		}

        public ParameterType lookForParameterByCRef(string cRef)
        {
            if (parameter != null && parameterByCRef.ContainsKey(cRef))
                return parameterByCRef[cRef];
            return null;
        }

        public ParameterType lookForParameterByVarName(string varName)
        {
            if (parameter != null && parameterByVarName.ContainsKey(varName))
                return parameterByVarName[varName];
            return null;
        }


        protected AbstractComponentFunctor lookForAbstractComponentFunctor(string package, string name, IDbConnection dbcon)
        {
			string library_path = package + "." + name;
			AbstractComponentFunctor acfa = Backend.acfdao.retrieve_libraryPath (library_path, dbcon);   // .retrieveByUID(component_UID);
            return acfa;
        }

        protected AbstractComponentFunctor lookForAbstractComponentFunctor(ComponentInUseType c, IDbConnection dbcon)
		{
			string library_path = c.package + "." + c.name;

            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);   // .retrieveByUID(component_UID);

 //           if (acf.Kind.Equals(Constants.KIND_QUANTIFIER_NAME))
 //           {
 //               acf = checkQuantifier(library_path, c.quantifier_value);
 //           }

			return acf;
		}

        public static AbstractComponentFunctor checkQuantifier(string library_path, string quantifier_value_str, IDbConnection dbcon)
        {
            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);

            if (!quantifier_value_str.EndsWith("inf"))
            {
                NumberStyles style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign;

                decimal quantifier_value = decimal.Parse(quantifier_value_str, style, CultureInfo.InvariantCulture);

                Quantifier q = Backend.qdao.retrieve(acf.Id_abstract, quantifier_value, dbcon);
                if (q == null)
                {
                    IList<Quantifier> q_list = Backend.qdao.list(acf.Id_abstract, dbcon);

                    AbstractComponentFunctor acf_new = new AbstractComponentFunctor();
                    acf_new.Id_abstract = DBConnector.nextKey("id_abstract", "abstractcomponentfunctor", dbcon);

                    Quantifier last_q_ = null;
                    int id_abstract_super = -1;
                    int id_abstract_sub = -1;
                    foreach (Quantifier q_ in q_list)
                    {
                        if (quantifier_value < q_.QuantifierValue)
                        {
                            id_abstract_super = acf.Library_path.EndsWith("Up") ? (last_q_ != null ? last_q_.Id_abstract_quantifier : acf.Id_abstract) : q_.Id_abstract_quantifier;
                            id_abstract_sub = acf.Library_path.EndsWith("Up") ? q_.Id_abstract_quantifier : (last_q_ != null ? last_q_.Id_abstract_quantifier : -2);
                            break;
                        }
                        last_q_ = q_;
                    }

                    if (id_abstract_super < 0)
                    {
                        id_abstract_super = acf.Library_path.EndsWith("Up") ? (last_q_ != null ? last_q_.Id_abstract_quantifier : acf.Id_abstract) : acf.Id_abstract;
                        id_abstract_sub = acf.Library_path.EndsWith("Up") ? -2 : (last_q_ != null ? last_q_.Id_abstract_quantifier : -2);
                    }

                    if (id_abstract_sub != -2)
                    {
                        AbstractComponentFunctorApplication acfa_sub = new AbstractComponentFunctorApplication();
                        acfa_sub.Id_abstract = acf_new.Id_abstract;
                        acfa_sub.Id_functor_app_next = -1;
                        Backend.acfadao.insert(acfa_sub, dbcon);

                        Backend.acfdao.updateSuperType(id_abstract_sub, acfa_sub.Id_functor_app, dbcon);
                    }

                    AbstractComponentFunctorApplication acfa_super = new AbstractComponentFunctorApplication();
                    acfa_super.Id_abstract = id_abstract_super;
                    acfa_super.Id_functor_app_next = -1;
                    Backend.acfadao.insert(acfa_super, dbcon);

                    string library_path_q = acf.Library_path + quantifier_value.ToString(CultureInfo.InvariantCulture);
                    acf_new.Id_functor_app_supertype = acfa_super.Id_functor_app;
                    acf_new.Library_path = library_path_q;
                    acf_new.Kind = acf.Kind;
                    acf_new.Hash_component_UID = FileUtil.create_snk_file(library_path_q + ".snk", library_path_q + ".pub"); ;
                    Backend.acfdao.insert(acf_new, dbcon);

                    q = new Quantifier();
                    q.Id_abstract = acf.Id_abstract;
                    q.QuantifierValue = quantifier_value;
                    q.Id_abstract_quantifier = acf_new.Id_abstract;
                    Backend.qdao.insert(q, dbcon);

                    acf = acf_new;
                }
                else
                    acf = Backend.acfdao.retrieve(q.Id_abstract_quantifier, dbcon);
            }
			
            return acf;
		}

        protected ParameterSupplyType lookForSupplyForVarName(string varName)
        {
            if (parameterSupply != null && parameterSupplyByVar.ContainsKey(varName))
                return parameterSupplyByVar[varName];
            return null;
        }

        protected InnerComponentType lookForInnerComponent(string cRef)
        {
            return innerAllByCRef[cRef];
        }

        protected bool isNotInSupply(InnerComponentType c)
        {
            return parameterSupply == null || !parameterSupplyByCRef.ContainsKey(c.localRef);
        }


        protected string buildEnumeratorId(string[] originRef, string eRef)
        {
            string eId = "";
            if (originRef != null)
            {
                foreach (string cRef in originRef)
                {
                    eId += cRef + ".";
                }
            }
            eId += eRef;
            return eId;
        }

        protected InterfaceType lookForInterface(string iRef)
        {
            foreach (InterfaceType i in this.anInterface)
            {
                if (i.iRef.Equals(iRef))
                {
                    return i;
                }
            }
            return null;
        }

        protected void insertComponentKey(char v, int id, byte[] snk_contents, byte[] pub_contents, IDbConnection dbcon)
		{
			ComponentKey ck = new ComponentKey();
			ck.IDType = v;
			ck.ID = id;
			ck.SNKContents = snk_contents;
			ck.PUBContents = pub_contents;
			Backend.ckdao.insert(ck, dbcon);
		}

	}


}
