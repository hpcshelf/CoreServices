﻿// /home/jefferson/projetos/hashmodel_monodevelop/hashmodel_monodevelop/tree/GenerateTree.cs created with MonoDevelop
// User: jefferson at 09:51 22/5/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace org.hpcshelf.database
{
    public class FreeVariableException : Exception
	{
	}
	
	public class GenerateTree
	{
        private static IDictionary<String, String> mmm = null;


        public static TreeNode generate(IDictionary<string,int> actualParametersTop, AbstractComponentFunctorApplication acfaRef, IDbConnection dbcon)
        {
		//	foreach (KeyValuePair<string,int> parTop in actualParametersTop)
		//		Console.WriteLine ("actualParametersTop[" + parTop.Key + "] = " + parTop.Value);

		//	foreach (KeyValuePair<string,int> parTop in acfaRef.ParametersList)
		//		Console.WriteLine ("acfaRef.ParametersList[" + parTop.Key + "] = " + parTop.Value);

		    IDictionary<TreeNode, int> level_log = new Dictionary<TreeNode,int> ();

		//	Console.WriteLine("begin generate " + acfaRef.Id_functor_app);
            mmm = new Dictionary<String, String>();

            IDictionary<int, TreeNode> memory = new Dictionary<int, TreeNode>();

            IList<AbstractComponentFunctorApplication> lll = new List<AbstractComponentFunctorApplication>();
            lll.Add(acfaRef);

            TreeNode root = new TreeNode(lll, null);
            ArrayList queue = new ArrayList();
            queue.Add(root);

			level_log.Add(root,0);

            while (queue.Count != 0)
            {

                TreeNode nodeRef = (TreeNode)queue[0];

				int level = level_log[nodeRef];
			//	Console.WriteLine ("ENTERING LEVEL " + level);

                int id_abstract = nodeRef.Functor_app.Id_abstract;
                int id_functor_app_actual = nodeRef.Functor_app.Id_functor_app;
                int id_abstract_top = nodeRef.Functor_app_top.Id_abstract;

			//	Console.WriteLine("Dequeue LOOP " + id_functor_app_actual);

                queue.RemoveAt(0);

                ICollection<SupplyParameter> parameterList = org.hpcshelf.DGAC.Backend.spdao.list(id_functor_app_actual, dbcon);
                foreach (SupplyParameter sp in parameterList)
                {
					bool flag_par = false;
                   // Console.WriteLine("generate LOOP {0}, {1}, {2} IN {3} -- {4}/{5}", sp.Id_parameter, sp.Id_functor_app, sp.Id_abstract , level, id_abstract_top, id_abstract);
					bool freeVariable = false;
					
                    string parameter_id = sp.Id_parameter;

                    AbstractComponentFunctorApplication acfaTop = null;
                    if (id_abstract_top != id_abstract)
                    {
                        AbstractComponentFunctorParameter acfpTop = org.hpcshelf.DGAC.Backend.acfpdao.retrieve(id_abstract_top, parameter_id, dbcon);
                        acfaTop = acfpTop == null ? null : org.hpcshelf.DGAC.Backend.acfadao.retrieve(acfpTop.Bounds_of, dbcon);
                        if (acfaTop == null)
                        {
                            AbstractComponentFunctorParameter acfp = org.hpcshelf.DGAC.Backend.acfpdao.retrieve(id_abstract, parameter_id, dbcon);
                            acfaTop = acfp == null ? null : org.hpcshelf.DGAC.Backend.acfadao.retrieve(acfp.Bounds_of, dbcon);
                        }
                    }
                    else
                    {
                        AbstractComponentFunctorParameter acfp = org.hpcshelf.DGAC.Backend.acfpdao.retrieve(id_abstract, parameter_id, dbcon);
                        acfaTop = acfp == null ? null : org.hpcshelf.DGAC.Backend.acfadao.retrieve(acfp.Bounds_of, dbcon);
                    }

					//Console.WriteLine("acfaTop is null ? " + (acfaTop==null));
					
                    AbstractComponentFunctorApplication acfaActual = null;
                    if (sp is SupplyParameterComponent)
                    {
                        SupplyParameterComponent spc = (SupplyParameterComponent)sp;
					//	Console.WriteLine ("acfaActual 1 ??? " + spc.Id_functor_app_actual);
                        acfaActual = org.hpcshelf.DGAC.Backend.acfadao.retrieve(spc.Id_functor_app_actual, dbcon);	
						if (acfaActual == null)
							throw new FreeVariableException ();
							//flag_par = true;	
                    }
                    else if (sp is SupplyParameterParameter)
                    {
						SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                        int Id_functor_app_actual = 0;

					//	Console.WriteLine ("****** parameter_id#??? =" + parameter_id + "#" + sp.Id_functor_app);
						if (!(flag_par= acfaRef.ParametersList.TryGetValue(parameter_id + "#" + sp.Id_functor_app, out Id_functor_app_actual)))
								 acfaRef.ParametersList.TryGetValue(parameter_id, out Id_functor_app_actual);
						
					//	Console.WriteLine (" acfaRef.ParametersList.Count = " + acfaRef.ParametersList.Count);
					//	foreach (KeyValuePair<string,int> p in acfaRef.ParametersList)
					//		Console.WriteLine("acfaRef.ParametersList -- " + p.Key + " -> " + p.Value);
						
                        if (Id_functor_app_actual <= 0) // LOOK AT THE TOP PARAMETERS
                        {
                            bool found = actualParametersTop.TryGetValue(spp.Id_argument, out Id_functor_app_actual);
					//	    Console.WriteLine (" actualParametersTop.Count = " + actualParametersTop.Count);
					//	    foreach (KeyValuePair<string,int> p in actualParametersTop)
					//		     Console.WriteLine("actualParametersTop -- " + p.Key + " -> " + p.Value);
							if (!found)
							{	
								string key = spp.Id_argument + "#" + sp.Id_functor_app;
					//			Console.WriteLine ("key ??? " + key);
								acfaRef.ParametersList.TryGetValue(key, out Id_functor_app_actual);
							}
							
					//		Console.WriteLine ("spp.Id_parameter_actual ??? " + spp.Id_argument);
					//		Console.WriteLine ("found ??? " + found);
                        }
					//	Console.WriteLine ("acfaRef ??? " + acfaRef.Id_functor_app);
					//	Console.WriteLine ("parameter_id ??? " + parameter_id);
					//	Console.WriteLine ("acfaActual 2 ??? " + Id_functor_app_actual);
						if (Id_functor_app_actual <= 0) {
							// FREE VARIABLE !!! USE THE BOUND !!!
							//AbstractComponentFunctorApplication acfaParF = br.ufc.pargo.hpe.backend.DGAC.BackEnd.acfadao.retrieve(spp.Id_functor_app);
							//AbstractComponentFunctorParameter parF = br.ufc.pargo.hpe.backend.DGAC.BackEnd.acfpdao.retrieve(acfaParF.Id_abstract,parameter_id);
							//Id_functor_app_actual = parF.Bounds_of;
							//Console.WriteLine ("FREE VARIABLE !!! BOUND=" + Id_functor_app_actual);
							freeVariable = true;
						} 
                        acfaActual = org.hpcshelf.DGAC.Backend.acfadao.retrieve(Id_functor_app_actual, dbcon);
                    }
					
                 //   Console.WriteLine ("acfaActual ??? {0} {1}", acfaActual.Id_functor_app, acfaActual.Id_abstract);

					if (!freeVariable) 
					{					
	                    // LOOK FOR ACTUAL PARAMETER IDs for THE NEXT ITERATIONS !!!
					//	Console.WriteLine ("acfaTop.Id_functor_app = " + acfaTop.Id_functor_app);
	                    ICollection<SupplyParameter> sss = org.hpcshelf.DGAC.Backend.spdao.list(acfaTop.Id_functor_app, dbcon);
	                    foreach (SupplyParameter sssx in sss)
	                    {
	                        if (sssx is SupplyParameterComponent)
	                        {
	                            SupplyParameterComponent ssxc = (SupplyParameterComponent)sssx;
	                            if (!mmm.ContainsKey(acfaActual.Id_functor_app + "." + ssxc.Id_parameter))
	                                mmm.Add(acfaActual.Id_functor_app + "." + ssxc.Id_parameter, ssxc.Id_parameter);
	                        }
	                        else if (sssx is SupplyParameterParameter)
	                        {
	                            SupplyParameterParameter ssxp = (SupplyParameterParameter)sssx;
	                            if (!mmm.ContainsKey(acfaActual.Id_functor_app + "." + ssxp.Id_parameter))
	                                mmm.Add(acfaActual.Id_functor_app + "." + ssxp.Id_parameter, ssxp.Id_argument);
	                        }
	
	                    }

						int memory_key = acfaActual.Id_functor_app;
	                    string parameter_id_2;
	                    mmm.TryGetValue(id_functor_app_actual + "." + parameter_id, out parameter_id_2);
	                    if (parameter_id_2 == null)
	                        parameter_id_2 = parameter_id;
						else
						    acfaRef.ParametersList.TryGetValue (parameter_id_2, out memory_key);

						TreeNode node = null;
						bool node_found = memory.TryGetValue(memory_key, out node);

					//	foreach (KeyValuePair<string, string> ppp in mmm)
					//		Console.WriteLine ("mmm[" + ppp.Key + "]=" + ppp.Value);
					//	foreach (KeyValuePair<int, TreeNode> pair in memory)
					//		Console.WriteLine ("memory: key=" + pair.Key + ", value=" + pair.Value.Functor_app.Id_functor_app);

						if (!node_found)
	                    {
							IList<AbstractComponentFunctorApplication> generalizeSteps = buildGeneralizationSteps(acfaActual, acfaTop, dbcon);

				//			foreach (AbstractComponentFunctorApplication yyy in generalizeSteps) 
				//				Console.WriteLine("generate - " + yyy.Id_functor_app);

	                        node = new TreeNode(generalizeSteps, nodeRef);
							level_log.Add(node, level+1);
	                        node.addParameterIdSyn(parameter_id_2);
	                        node.Parameter_id = parameter_id_2;
	                        queue.Add(node);
				//			Console.WriteLine("BEGIN " + parameter_id + "," + parameter_id_2 + "," + flag_par + " *** "  + node.Functor_app.Id_functor_app);
							if (level == 0 || flag_par) 
								memory.Add (node.Functor_app.Id_functor_app/*parameter_id_2 + (flag_par ? sp.Id_functor_app.ToString () : "")*/, node);
				//			else 
				//				Console.WriteLine ("LEVEL " + level);
				//			Console.WriteLine("END" + parameter_id + "," + parameter_id_2);
	                    }
	                    else
	                    {
				//			Console.WriteLine ("parameter_id_2 = " + parameter_id_2 + (flag_par ? sp.Id_functor_app.ToString() : ""));
	                        node.addParameterIdSyn(parameter_id_2);
	                    }
						
                        nodeRef.addChild(node);
					}
					else 
					{
						
					}


                }
            }

			//Console.WriteLine("end generate " + root.Parameter_id + "," +  root.Functor_app.Id_functor_app);
            return root;
        }



        private static IList<AbstractComponentFunctorApplication> buildGeneralizationSteps(AbstractComponentFunctorApplication acfActual, AbstractComponentFunctorApplication acfaTop, IDbConnection dbcon)
        {
          //  Console.WriteLine("buildGeneralizationSteps - ENTER --- {0} {1}", acfaTop.Id_abstract, acfActual.Id_abstract);

            AbstractComponentFunctorApplication acfa_step = acfActual;
            AbstractComponentFunctorApplication acfa_top = acfaTop;
            IList<AbstractComponentFunctorApplication> gs = new List<AbstractComponentFunctorApplication>();
            AbstractComponentFunctor acf;	
            while (acfa_step.Id_abstract != acfa_top.Id_abstract)
            {
                gs.Add(acfa_step);
                acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve(acfa_step.Id_abstract, dbcon);
                acfa_step = org.hpcshelf.DGAC.Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
            }            

            gs.Add(acfa_step);

			//Console.WriteLine("buildGeneralizationSteps - EXIT");
			return gs;
        }
		
		

	}
}
