﻿// /home/jefferson/projetos/hashmodel_monodevelop/hashmodel_monodevelop/tree/Resolution.cs created with MonoDevelop
// User: jefferson at 14:24 2/6/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using System.Collections.Generic;
using org.hpcshelf.unit;
using System.Diagnostics;
using System.Data;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{
    public class Resolution
    {

        public static Component tryGeneralize(TreeNode CTop, TreeNode C, IDbConnection dbcon)
        {
            C.reset();
            do
            {
                Component CTopImpl = C.Next != null ? tryGeneralize(CTop, C.Next, dbcon) : findImplementation(CTop, dbcon);

                if (CTopImpl != null)
                    return CTopImpl;
                else
                    C.generalize();

            } while (!C.OnTop);

            return null;
        }

        public static Component[] tryGeneralizeAll(TreeNode CTop, TreeNode C, IDbConnection dbcon)
        {
            IDictionary<string, Component> cList = new Dictionary<string, Component>();

            C.reset();
            do
            {
                Component[] CTopImpl;
                if (C.Next != null)
                    CTopImpl = tryGeneralizeAll(CTop, C.Next, dbcon);
                else
                {
                    Component c = findImplementation(CTop, dbcon);
                    CTopImpl = c != null ? new Component[1] { c } : new Component[0];
                }

                foreach (Component c in CTopImpl)
                    if (!cList.ContainsKey(c.Library_path))
                        cList.Add(c.Library_path, c);

                C.generalize();

            } while (!C.OnTop);

            Component[] cArray = new Component[cList.Count];
            cList.Values.CopyTo(cArray, 0);

            return cArray;
        }

        private static string writeTreeNode(TreeNode t)
        {
            string result = t.Parameter_id + "@" + t.GetHashCode() + "=" + t.Functor_app.Id_abstract + (t.Children.Count > 0 ? "[" : "");
            // Trace.Write(result);
            foreach (TreeNode child in t.Children)
            {
                result += writeTreeNode(child);
                result += ",";
                //     Trace.Write(",");
            }
            string s = t.Children.Count > 0 ? "]" : "";
            result += s;
            // Trace.Write(s);
            return result;
        }

        private static string readTreeNode(TreeNode t)
        {
            string result = t.Functor_app.Id_abstract + (t.Children.Count > 0 ? "[" : "");
            foreach (TreeNode child in t.Children)
            {
                result += readTreeNode(child);
                result += ",";
            }
            result += t.Children.Count > 0 ? "]" : "";
            return result;
        }

        private static IDictionary<int, IList<AbstractComponentFunctorApplication>> cache2 = new Dictionary<int, IList<AbstractComponentFunctorApplication>>();

        // EFFICIENCY IS NOT YET A CONCERN ! NEED TO IMPROVE !
        private static Component findImplementation(TreeNode CTop, IDbConnection dbcon)
        {
            int id_abstract = CTop.Functor_app.Id_abstract;

            IList<AbstractComponentFunctorApplication> acfaList;

            if (!cache2.TryGetValue(id_abstract, out acfaList))
            {
                acfaList = Backend.acfadao.listByIdAbstract(id_abstract, dbcon);
                cache2[id_abstract] = acfaList;
            }

            foreach (AbstractComponentFunctorApplication acfa in acfaList)
            {
                if (recMatchParameters(CTop, acfa, dbcon))
                {
                    //  Trace.Write("IMPLEMENTATION FOUND !!!! ");
                    //	writeTreeNode (CTop);						

                    return Backend.cdao.retrieveByFunctorApp(acfa.Id_functor_app, dbcon);
                }
            }

            return null;
        }

        private static IDictionary<int, IDictionary<string, int>> ttt_memory = new Dictionary<int, IDictionary<string, int>>();

        private static bool recMatchParameters(TreeNode node, AbstractComponentFunctorApplication acfa, IDbConnection dbcon)
        {
            AbstractComponentFunctorApplication acfaSon = null;

            IDictionary<string, int> ttt;
            if (!ttt_memory.TryGetValue(acfa.Id_functor_app, out ttt))
            {
                ttt = new Dictionary<string, int>();
                IList<SupplyParameterComponent> spList = org.hpcshelf.DGAC.Backend.spcdao.list(acfa.Id_functor_app, dbcon);

                foreach (SupplyParameterComponent sp_ in spList)
                {
                    SupplyParameterComponent sp = (SupplyParameterComponent)sp_;
                    ttt[sp.Id_parameter] = sp.Id_functor_app_actual;
                }

                ttt_memory[acfa.Id_functor_app] = ttt;
            }

            foreach (TreeNode nodeSon in node.Children)
            {
                string parameter_id = nodeSon.Parameter_id;
                int id_functor_app_actual = -1;
                bool found = false;
                foreach (string parid in nodeSon.getParameterIdSyns())
                {
                    if (ttt.ContainsKey(parid))
                    {
                        found = true;
                        id_functor_app_actual = ttt[parid]; // TODO
                        int id_abstract;
                        if (!id_abstract_of_functor_app.TryGetValue(id_functor_app_actual, out id_abstract))
                        {
                            acfaSon = Backend.acfadao.retrieve(id_functor_app_actual, dbcon);
                            id_abstract = acfaSon.Id_abstract;
                            id_abstract_of_functor_app[id_functor_app_actual] = id_abstract;
                            acfa_of_functor_app[id_functor_app_actual] = acfaSon;
                        }

                        if (/*acfaSon.Id_abstract*/ id_abstract != nodeSon.Functor_app.Id_abstract)
                        {
                            // Console.WriteLine("FAIL 1! ");
                            return false;
                        }
                        //Console.WriteLine("MATCH! ");
                    }
                }

                if (found)
                {
                    acfaSon = acfa_of_functor_app[id_functor_app_actual];
                    if (!recMatchParameters(nodeSon, acfaSon, dbcon))
                        return false;
                }

            }

            return true;
        }

        private static IDictionary<int, int> id_abstract_of_functor_app = new Dictionary<int, int>();
        private static IDictionary<int, AbstractComponentFunctorApplication> acfa_of_functor_app = new Dictionary<int, AbstractComponentFunctorApplication>();

        public static void sort(TreeNode root)
        {
            lastMarked = null;
            while (!root.Closed)
                sort_(root, new List<TreeNode>());
        }


        private static TreeNode lastMarked = null;

        private static void sort_(TreeNode root, IList<TreeNode> ddd)
        {
            IList<TreeNode> parameters = root.Children;
            //has umarked parameters
            if (!root.Closed)
            {
                root.Closed = true;
                foreach (TreeNode parameter in parameters)
                {
                    if (parameter.Mark == false)
                    {
                        root.Closed = false;
                        parameter.Parent = root; // not really necessary.
                        if (ddd.Contains(parameter))
                        {
                            //	foreach (TreeNode ttt in ddd)
                            //		Console.WriteLine("CYCLE PATH: argument=" + ttt.Parameter_id + "; value=" + ttt.Functor_app.Id_functor_app);

                            throw new Exception("CYCLE DETECTED " + parameter.Parameter_id + ", " + parameter.Functor_app.Id_functor_app);
                        }
                        ddd.Add(parameter);
                        sort_(parameter, ddd);
                        ddd.Remove(parameter);
                        parameter.Parent = null;
                    }
                }
            }

            if (root.Closed)
            {
                root.Mark = true;
                root.Next = lastMarked;
                lastMarked = root;
            }
        }

        private static IDictionary<int, Component> cache = new Dictionary<int, Component>();


        public static Component findHashComponent(IDictionary<string, int> actualParametersTop, AbstractComponentFunctorApplication acfaRef, IDbConnection dbcon)
        {
            Component c = null;

            IList<AbstractComponentFunctorApplication> acfa_list = Backend.acfadao.listByIdAbstract(acfaRef.Id_abstract, dbcon);

            int proximity_value = int.MaxValue;
            foreach (AbstractComponentFunctorApplication acfa in acfa_list)
            {
                int proximity_value_new = subType(actualParametersTop, acfaRef, acfa, dbcon);
                if (proximity_value_new > 0 && proximity_value_new <= proximity_value)
                {
                    c = Backend.cdao.retrieveByFunctorApp(Backend.acfadao.retrieve_last(acfa, dbcon).Id_functor_app, dbcon);
                    if (c != null)
                        proximity_value = proximity_value_new;
                }
            }

            return c;
        }

        // acfa2 <: acfa1
        public static int subType(AbstractComponentFunctorApplication acfa2, AbstractComponentFunctorApplication acfa1, IDbConnection dbcon)
        {
            IDictionary<string, int> actualParametersTop = new Dictionary<string, int>();
            return subType(actualParametersTop, acfa2, acfa1, dbcon);
        }

        public static int subType(IDictionary<string, int> actualParametersTop, AbstractComponentFunctorApplication acfa2, AbstractComponentFunctorApplication acfa1, IDbConnection dbcon)
        {

            if (acfa2 is AbstractComponentFunctorApplicationQuantifier && acfa1 is AbstractComponentFunctorApplicationQuantifier)
            {
                AbstractComponentFunctorApplicationQuantifier acfa1q = (AbstractComponentFunctorApplicationQuantifier)acfa1;
                AbstractComponentFunctorApplicationQuantifier acfa2q = (AbstractComponentFunctorApplicationQuantifier)acfa2;

                AbstractComponentFunctor acf1q = Backend.acfdao.retrieve(acfa1q.Id_abstract, dbcon);
                AbstractComponentFunctor acf2q = Backend.acfdao.retrieve(acfa2q.Id_abstract, dbcon);

                bool up1 = acf1q.Library_path.EndsWith("Up");
                bool up2 = acf2q.Library_path.EndsWith("Up");

                if (!acfa2q.Quantifier.HasValue && acfa1q.Quantifier.HasValue)
                    return -1;

                if (acfa1q.Quantifier.HasValue && acfa2q.Quantifier.HasValue)
                {
                    if (up1 && up2 && acfa2q.Quantifier < acfa1q.Quantifier)         // UP
                        return -1;
                    else if (!up1 && !up2 && acfa2q.Quantifier > acfa1q.Quantifier)  // DOWN
                        return -1;
                    else if (up1 != up2)
                        return -1;
                }
            }
            else if (!(acfa2 is AbstractComponentFunctorApplicationQuantifier) && !(acfa1 is AbstractComponentFunctorApplicationQuantifier))
            {
                int id_abstract = acfa2.Id_abstract;
                AbstractComponentFunctor acf = Backend.acfdao.retrieve(id_abstract, dbcon);
                int id_functor_app_supertype = acf.Id_functor_app_supertype;
                while (id_abstract != acfa1.Id_abstract && id_functor_app_supertype > 0)
                {
                    AbstractComponentFunctorApplication acfa = Backend.acfadao.retrieve(id_functor_app_supertype, dbcon);
                    id_abstract = acfa.Id_abstract;
                    acf = Backend.acfdao.retrieve(id_abstract, dbcon);
                    id_functor_app_supertype = acf.Id_functor_app_supertype;
                }

                if (id_abstract != acfa1.Id_abstract)
                    return -1;
            }
            else
                return -1;

            IDictionary<string, VarianceType> variance = new Dictionary<string, VarianceType>();
            IList<AbstractComponentFunctorParameter> acfp_list = Backend.acfpdao.list(acfa2.Id_abstract, dbcon);
            foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                variance[acfp.Id_parameter] = acfp.Variance;
            acfp_list = Backend.acfpdao.list(acfa1.Id_abstract, dbcon);
            foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                if (!variance.ContainsKey(acfp.Id_parameter))
                    variance[acfp.Id_parameter] = acfp.Variance;

            ICollection<SupplyParameter> sp_list_2 = Backend.spdao.list(acfa2.Id_functor_app, dbcon);
            ICollection<SupplyParameter> sp_list_1 = Backend.spdao.list(acfa1.Id_functor_app, dbcon);

            foreach (SupplyParameter sp_2 in sp_list_2)
            {
                AbstractComponentFunctorApplication acfa_current_1 = null;
                AbstractComponentFunctorApplication acfa_current_2 = null;

                SupplyParameter sp_1 = Backend.spdao.retrieve(sp_2.Id_parameter, acfa1.Id_functor_app, dbcon);
                if (sp_1 != null)
                {
                    if (sp_1 is SupplyParameterComponent)
                    {
                        SupplyParameterComponent sp_1_c = (SupplyParameterComponent)sp_1;
                        acfa_current_1 = Backend.acfadao.retrieve(sp_1_c.Id_functor_app_actual, dbcon);
                    }
                    else if (sp_1 is SupplyParameterParameter)
                    {
                        SupplyParameterParameter sp_1_p = (SupplyParameterParameter)sp_1;
                        AbstractComponentFunctorApplication acfa_1_top = Backend.acfadao.retrieve_last(acfa1, dbcon);
                        AbstractComponentFunctorParameter acfp_1 = Backend.acfpdao.retrieve(acfa_1_top.Id_abstract, sp_2.Id_parameter, dbcon);
                        if (acfp_1 == null)
                        {
                            AbstractComponentFunctorApplication acfa_1_top_super = null;

                            do
                            {
                                int id_functor_app_next = acfa_1_top.Id_functor_app_next;
                                acfa_1_top = acfa_1_top_super != null ? acfa_1_top_super : acfa_1_top;
                                acfa_1_top_super = Backend.acfadao.retrieve(id_functor_app_next, dbcon);
                                acfp_1 = Backend.acfpdao.retrieve(acfa_1_top_super.Id_abstract, sp_2.Id_parameter, dbcon);
                            }
                            while (acfp_1 == null);

                            {
                                int id_functor_app_supertype = Backend.acfdao.retrieve(acfa_1_top.Id_abstract, dbcon).Id_functor_app_supertype;
                                SupplyParameterComponent spcdao = Backend.spcdao.retrieve(sp_2.Id_parameter, id_functor_app_supertype, dbcon);
                                acfa_current_1 = Backend.acfadao.retrieve(spcdao.Id_functor_app_actual, dbcon);
                            }
                        }
                        else
                            acfa_current_1 = Backend.acfadao.retrieve(acfp_1.Bounds_of, dbcon);
                    }


                    if (sp_2 is SupplyParameterComponent)
                    {
                        SupplyParameterComponent sp_2_c = (SupplyParameterComponent)sp_2;
                        acfa_current_2 = Backend.acfadao.retrieve(sp_2_c.Id_functor_app_actual, dbcon);
                    }
                    else if (sp_2 is SupplyParameterParameter)
                    {
                        SupplyParameterParameter sp_2_p = (SupplyParameterParameter)sp_2;
                        if (actualParametersTop.ContainsKey(sp_2_p.Id_argument))
                            acfa_current_2 = Backend.acfadao.retrieve(actualParametersTop[sp_2_p.Id_argument], dbcon);
                        else // if (sp_2_p.FreeVariable)
                            acfa_current_2 = acfa_current_1;
                        /*else
                        {
                            // take the bound.
                            AbstractComponentFunctorApplication acfa_2_top = Backend.acfadao.retrieve_last(acfa2);
                            AbstractComponentFunctorParameter acfp_2 = Backend.acfpdao.retrieve(acfa_2_top.Id_abstract, sp_2.Id_parameter);
                            if (acfp_2 == null)
                            {
                                AbstractComponentFunctorApplication acfa_2_top_super = null;

                                do
                                {
                                    int id_functor_app_next = acfa_2_top.Id_functor_app_next;
                                    acfa_2_top = acfa_2_top_super != null ? acfa_2_top_super : acfa_2_top;
                                    acfa_2_top_super = Backend.acfadao.retrieve(id_functor_app_next);
                                    acfp_2 = Backend.acfpdao.retrieve(acfa_2_top_super.Id_abstract, sp_2.Id_parameter);
                                }
                                while (acfp_2 == null);

                                {
                                    int id_functor_app_supertype = Backend.acfdao.retrieve(acfa_2_top.Id_abstract).Id_functor_app_supertype;
                                    SupplyParameterComponent spcdao = Backend.spcdao.retrieve(sp_2.Id_parameter, id_functor_app_supertype);
                                    acfa_current_2 = Backend.acfadao.retrieve(spcdao.Id_functor_app_actual);
                                }
                            }
                            else
                                acfa_current_2 = Backend.acfadao.retrieve(acfp_2.Bounds_of);
                        }*/
                    }

                    int result = 0;

                    if (variance[sp_2.Id_parameter].Equals(VarianceType.covariant))
                        result = subType(actualParametersTop, acfa_current_2, acfa_current_1, dbcon);
                    else if (variance[sp_2.Id_parameter].Equals(VarianceType.contravariant))
                        result = subType(actualParametersTop, acfa_current_1, acfa_current_2, dbcon);
                    else if (variance[sp_2.Id_parameter].Equals(VarianceType.invariant))
                        result = subType(actualParametersTop, acfa_current_2, acfa_current_1, dbcon) * subType(actualParametersTop, acfa_current_1, acfa_current_2, dbcon);

                    if (result < 0)
                        return result;
                }
            }


            return 1;
        }

        public static Component findHashComponent2(IDictionary<string, int> actualParametersTop, AbstractComponentFunctorApplication acfaRef, IDbConnection dbcon)
        {
            Console.WriteLine("FIND HASH COMPONENT: " + acfaRef.Id_functor_app + " === " + acfaRef.Id_abstract);

            Component c;

            if (acfaRef.ParametersList.Count == 0)
            {
                if (cache.TryGetValue(acfaRef.Id_functor_app, out c))
                {
                    return c;
                }
            }

            TreeNode root = GenerateTree.generate(actualParametersTop, acfaRef, dbcon);

            Resolution.sort(root);

            TreeNode tn = root;
            while (tn != null)
            {
                tn = tn.Next;
            }

            writeTreeNode(root);

            //Console.WriteLine ("AFTER Resolution.sort(root)");

            c = Resolution.tryGeneralize(root, root, dbcon);

            //Console.WriteLine ("AFTER Resolution.tryGeneralize(root, root) --- " + (c == null));

            if (acfaRef.ParametersList.Count == 0)
            {
                cache.Add(acfaRef.Id_functor_app, c);
            }

            Console.WriteLine("FINISH - FIND HASH COMPONENT -- " + (c == null ? "NOT FOUND" : "FOUND"));

            return Backend.cdao.retrieve_libraryPath(c.Library_path, dbcon); ; // if c is null, there is not an implementation ....			
        }

        public static Component[] findHashComponentAll(AbstractComponentFunctorApplication acfaRef, IDbConnection dbcon)
        {

            Console.WriteLine("FIND HASH COMPONENT ALL: " + acfaRef.Id_functor_app);

            Component[] cAll;

            TreeNode root = GenerateTree.generate(new Dictionary<string, int>(), acfaRef, dbcon);

            Resolution.sort(root);

            TreeNode tn = root;
            while (tn != null)
            {
                //	Trace.Write (tn.Functor_app.Id_abstract + "@" + tn.GetHashCode() + "->");
                tn = tn.Next;
            }

            writeTreeNode(root);

            //	Console.WriteLine ("AFTER Resolution.sort(root) - ALL");

            cAll = Resolution.tryGeneralizeAll(root, root, dbcon);

            //	Console.WriteLine ("AFTER Resolution.tryGeneralize(root, root) - ALL");

            return cAll; // if c is null, there is not an implementation ....
        }

        public static void addToResolutionTree(Component c, IDbConnection dbcon)
        {
            if (Backend.rtdao.componentIsInResolutionTree(c.Id_concrete, dbcon))
                return;

            IDictionary<string, SupplyParameter> arguments = Backend.spdao.listBy(c.Id_functor_app, dbcon);

            int id_functor_app_current = c.Id_functor_app;

            do
            {
                AbstractComponentFunctorApplication acfaRef = Backend.acfadao.retrieve(id_functor_app_current, dbcon);

                List<AbstractComponentFunctorParameter> acfp_list = (List<AbstractComponentFunctorParameter>)Backend.acfpdao.listStaticParameters(acfaRef.Id_abstract, dbcon);
                acfp_list.Sort(delegate (AbstractComponentFunctorParameter x, AbstractComponentFunctorParameter y) { return x.Order - y.Order; });

                ResolutionTree rt0 = Backend.rtdao.retrieve_root(acfaRef.Id_abstract, dbcon);
                if (rt0 == null)
                {
                    rt0 = new ResolutionTree();
                    rt0.Id_abstract = acfaRef.Id_abstract;
                    rt0.Level = -2;
                    Backend.rtdao.insert(rt0, dbcon);
                }

                // enterLevel(c, 0, rt0, acfp_list, arguments);

                ResolutionTree rt = rt0;
                int node_id = rt.NodeId;

                int level = 0;
                foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                {
                    int? id_functor_app = arguments.ContainsKey(acfp.Id_parameter) && arguments[acfp.Id_parameter] is SupplyParameterComponent
                                                  ? (int?)((SupplyParameterComponent)arguments[acfp.Id_parameter]).Id_functor_app_actual : null /*acfp.Bounds_of*/;

                    AbstractComponentFunctor acf = Backend.acfdao.retrieve(Backend.acfadao.retrieve(acfp.Bounds_of, dbcon).Id_abstract, dbcon);

                    Tuple<char, char> q = Backend.isQuantifier(acf.Id_abstract, dbcon);
                    if (q != null)
                    {
                        if (id_functor_app.HasValue)
                        {
                            AbstractComponentFunctorApplication acfa_argument = Backend.acfadao.retrieve(id_functor_app.GetValueOrDefault(), dbcon);
                            AbstractComponentFunctorApplicationQuantifier acfa_argument_quantifier = (AbstractComponentFunctorApplicationQuantifier)acfa_argument;

                            rt = Backend.rtdao.retrieve(node_id, acfa_argument_quantifier.Quantifier, level, dbcon);
                            if (rt == null)
                            {
                                rt = new ResolutionTree();
                                rt.SuperId = node_id;
                                rt.Level = level;
                                rt.Id_abstract = acf.Id_abstract;
                                rt.Quantifier = acfa_argument_quantifier.Quantifier;
                                Backend.rtdao.insert(rt, dbcon);
                            }
                        }
                        else
                        {
                            rt = Backend.rtdao.retrieve(node_id, null, level, dbcon);
                            if (rt == null)
                            {
                                rt = new ResolutionTree();
                                rt.SuperId = node_id;
                                rt.Level = level;
                                rt.Id_abstract = acf.Id_abstract;
                                Backend.rtdao.insert(rt, dbcon);
                            }
                        }
                    }
                    else
                    {
                        if (id_functor_app.HasValue)
                        {
                            AbstractComponentFunctorApplication acfa_argument = Backend.acfadao.retrieve(id_functor_app.GetValueOrDefault(), dbcon);
                            int id_abstract_current = acfa_argument.Id_abstract;

                            // if (id_abstract_current == 111)
                            //     Console.WriteLine();

                            rt = Backend.rtdao.retrieve(node_id, id_abstract_current, level, dbcon);
                            if (rt == null)
                            {
                                rt = new ResolutionTree();
                                rt.SuperId = node_id;
                                rt.Level = level;
                                rt.Id_abstract = id_abstract_current;
                                Backend.rtdao.insert(rt, dbcon);
                            }

                        }
                        else
                        {
                            rt = Backend.rtdao.retrieve(node_id, level, dbcon);
                            if (rt == null)
                            {
                                rt = new ResolutionTree();
                                rt.SuperId = node_id;
                                rt.Level = level;
                                Backend.rtdao.insert(rt, dbcon);
                            }
                        }
                    }
                    node_id = rt.NodeId;
                    level++;
                }

                ResolutionTree rt_candidate = new ResolutionTree();
                rt_candidate.Id_abstract = c.Id_concrete;
                rt_candidate.Level = -1;
                rt_candidate.SuperId = node_id;
                Backend.rtdao.insert(rt_candidate, dbcon);
                Console.WriteLine("final node {0} - {1}", node_id, c.Library_path);

                id_functor_app_current = acfaRef.Id_functor_app_next;
            }
            while (id_functor_app_current > 0);

            return;
        }

        private static void enterLevel(Component c, int level, ResolutionTree rt0, List<AbstractComponentFunctorParameter> acfp_list, IDictionary<string, SupplyParameter> arguments, IDbConnection dbcon)
        {
            int node_id = rt0.NodeId;

            if (level >= acfp_list.Count)
            {
                ResolutionTree rt_candidate = new ResolutionTree();
                rt_candidate.Id_abstract = c.Id_concrete;
                rt_candidate.Level = -1;
                rt_candidate.SuperId = node_id;
                Backend.rtdao.insert(rt_candidate, dbcon);
                Console.WriteLine("final node {0} - {1}", node_id, c.Library_path);
                return;
            }

            AbstractComponentFunctorParameter acfp = acfp_list[level];

            int? id_functor_app = arguments.ContainsKey(acfp.Id_parameter)
                                          ? (int?)((SupplyParameterComponent)arguments[acfp.Id_parameter]).Id_functor_app_actual : null /*acfp.Bounds_of*/;

            AbstractComponentFunctor acf = Backend.acfdao.retrieve(Backend.acfadao.retrieve(acfp.Bounds_of, dbcon).Id_abstract, dbcon);

            ResolutionTree rt = null;
            Tuple<char, char> q = Backend.isQuantifier(acf.Id_abstract, dbcon);
            if (q != null)
            {
                if (id_functor_app.HasValue)
                {
                    AbstractComponentFunctorApplication acfa_argument = Backend.acfadao.retrieve(id_functor_app.GetValueOrDefault(), dbcon);
                    AbstractComponentFunctorApplicationQuantifier acfa_argument_quantifier = (AbstractComponentFunctorApplicationQuantifier)acfa_argument;

                    rt = Backend.rtdao.retrieve(node_id, acfa_argument_quantifier.Quantifier, level, dbcon);
                    if (rt == null)
                    {
                        rt = new ResolutionTree();
                        rt.SuperId = node_id;
                        rt.Level = level;
                        rt.Id_abstract = acf.Id_abstract;
                        rt.Quantifier = acfa_argument_quantifier.Quantifier;
                        Backend.rtdao.insert(rt, dbcon);
                    }
                }
                else
                {
                    rt = Backend.rtdao.retrieve(node_id, null, level, dbcon);
                    if (rt == null)
                    {
                        rt = new ResolutionTree();
                        rt.SuperId = node_id;
                        rt.Level = level;
                        rt.Id_abstract = acf.Id_abstract;
                        Backend.rtdao.insert(rt, dbcon);
                    }
                }
            }
            else
            {
                if (id_functor_app.HasValue)
                {
                    AbstractComponentFunctorApplication acfa_argument = Backend.acfadao.retrieve(id_functor_app.GetValueOrDefault(), dbcon);
                    int id_abstract_current = acfa_argument.Id_abstract;

                    // if (id_abstract_current == 111)
                    //     Console.WriteLine();

                    rt = Backend.rtdao.retrieve(node_id, id_abstract_current, level, dbcon);
                    if (rt == null)
                    {
                        rt = new ResolutionTree();
                        rt.SuperId = node_id;
                        rt.Level = level;
                        rt.Id_abstract = id_abstract_current;
                        Backend.rtdao.insert(rt, dbcon);
                    }

                }
                else
                {
                    rt = Backend.rtdao.retrieve(node_id, level, dbcon);
                    if (rt == null)
                    {
                        rt = new ResolutionTree();
                        rt.SuperId = node_id;
                        rt.Level = level;
                        Backend.rtdao.insert(rt, dbcon);
                    }
                }
            }

            enterLevel(c, level + 1, rt, acfp_list, arguments, dbcon);
        }

        public static Component[] findHashComponent3(AbstractComponentFunctorApplication acfaRef, IDbConnection dbcon)
        {
            ResolutionTree rt0 = Backend.rtdao.retrieve_root(acfaRef.Id_abstract, dbcon);
            if (rt0 == null) return new Component[0];

            IList<AbstractComponentFunctorParameter> acfp_list = (List<AbstractComponentFunctorParameter>)Backend.acfpdao.listStaticParameters(acfaRef.Id_abstract, dbcon);

            IDictionary<string, SupplyParameter> arguments = Backend.spdao.listBy(acfaRef.Id_functor_app, dbcon);

            // IList<int> node_id = new List<int>() { rt0.NodeId };
            string node_id = string.Format("({0})", rt0.NodeId);

           // var sw1 = new Stopwatch();
           // var sw2 = new Stopwatch();
           // var sw3 = new Stopwatch();

            IDbCommand dbcmd = dbcon.CreateCommand();

           // int c1=0, c2=0, c3=0;
            foreach (AbstractComponentFunctorParameter acfp in acfp_list)
            {
                int id_functor_app = arguments.ContainsKey(acfp.Id_parameter) && arguments[acfp.Id_parameter] is SupplyParameterComponent
                                              ? ((SupplyParameterComponent)arguments[acfp.Id_parameter]).Id_functor_app_actual : acfp.Bounds_of;

                if (id_functor_app == acfp.Bounds_of)
                {
                  //  c1++;
                    node_id = Backend.rtdao.retrieve3(node_id, dbcmd/*, sw1, sw2, sw3*/);
                }
                else
                {
                    AbstractComponentFunctorApplication acfa_argument = Backend.acfadao.retrieve(id_functor_app, dbcon);

                    if (acfa_argument is AbstractComponentFunctorApplicationQuantifier)
                    {
                    //    c2++;
                        AbstractComponentFunctorApplicationQuantifier acfa_argument_quantifier = (AbstractComponentFunctorApplicationQuantifier)acfa_argument;
                        AbstractComponentFunctor acf = Backend.acfdao.retrieve(Backend.acfadao.retrieve(acfp.Bounds_of, dbcon).Id_abstract, dbcon);
                        char subtyping_direction = acf.Name.Equals("IntUp") || acf.Name.Equals("DecimalUp") ? '+' : '-';
                        node_id = Backend.rtdao.retrieve(node_id, acfa_argument_quantifier.Quantifier/*, level*/, acfp.Variance, subtyping_direction/*, bounds_flag, out bound_node_id*/, dbcon);
                    }
                    else
                    {
                      //  c3++;
                        node_id = Backend.rtdao.retrieve(node_id, acfa_argument.Id_abstract, /*level,*/ acfp.Variance/*, bounds_flag, out bound_node_id*/, dbcon);
                    }

                }

                if (node_id.Equals("()"))
                    break;

            }

            dbcmd.Dispose();

          //  Console.WriteLine("COUNTERS: {0} -- {1} -- {2}", c1, c2, c3);

          //  Console.WriteLine("TIME !!!!!!!!!!!!! {0}ms {1}ms {2}ms", sw1.ElapsedMilliseconds, sw2.ElapsedMilliseconds, sw3.ElapsedMilliseconds);

            Component[] candidates_list = null;
            if (!node_id.Equals("()"))
                candidates_list = Backend.rtdao.listBySuperIds_(node_id, dbcon);
            else
                candidates_list = new Component[0];

            return candidates_list;
        }

    /*   private static void update_bound_super_id(IList<int> last_bound_id_list, IList<int> node_id_list)
        {
            IDictionary<int, IList<int>> vovo = new Dictionary<int, IList<int>>();
            foreach (int node_id in node_id_list)
            {
                int super_id = Backend.rtdao.retrieve(node_id).SuperId;
                int super_super_id = Math.Abs(Backend.rtdao.retrieve(Math.Abs(super_id)).SuperId);
                if (!vovo.ContainsKey(super_super_id))
                    vovo[super_super_id] = new List<int>();
                vovo[super_super_id].Add(node_id);
            }

            foreach (int last_bound_id in last_bound_id_list)
            {
                int super_id = -Backend.rtdao.retrieve(last_bound_id).SuperId;
                if (!vovo.ContainsKey(super_id))
                {
                    Console.WriteLine();
                }

                foreach (int node_id in vovo[super_id])
                {
                    Backend.rtdao.updateBoundSuperId(node_id, last_bound_id);
                }
            }
        }

        private static bool check_bound_id(int level, IList<int> node_id_list, out IList<int> current_bound_id)
        {
            bool flag = false;
            current_bound_id = new List<int>();
            foreach (int node_id in node_id_list)
            {
                ResolutionTree node = Backend.rtdao.retrieve(node_id);
                if (!node.BoundNodeId.HasValue)
                {
                    flag = true;
                    ResolutionTree rt = new ResolutionTree();
                    rt.SuperId = -node_id;
                    rt.Level = level;
                    Backend.rtdao.updateBoundNodeId(node_id, Backend.rtdao.insert(rt));
                    current_bound_id.Add(rt.NodeId);
                }
            }

            if (current_bound_id.Count == 0)
                current_bound_id = null;

            return flag;
        }*/
    }
}
